/**
 *  @file treepart_app.cpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Documentation for treepart.cpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#include "treepart_app.hpp"

/**
 * @brief Main entry point for the TreePart application (standalone CLI)
 *
 * @param nargs
 * @param args
 * @return
 */
int main(int nargs, char *args[]) {
  MPI_Init(&nargs, &args);
  rank_t myrank;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  {
    if (myrank == 0) {
      std::cout << C_GitVersionString;
      std::cout << "Message: TreePart compiled with integer sizes\n";
      {
        std::cout << "Message: id_size_t = " << sizeof(id_size_t) * 8
                  << " bits ";
        auto my_signed_string =
            std::is_signed<id_size_t>::value ? "(signed)\n" : "(unsigned)\n";
        std::cout << my_signed_string;
      }
      {
        std::cout << "Message: my_size_t = " << sizeof(my_size_t) * 8
                  << " bits ";
        auto my_signed_string =
            std::is_signed<my_size_t>::value ? "(signed)\n" : "(unsigned)\n";
        std::cout << my_signed_string;
      }
      {
        std::cout << "Message: ZOLTAN_ID_TYPE = " << sizeof(ZOLTAN_ID_TYPE) * 8
                  << " bits ";
        auto my_signed_string = std::is_signed<ZOLTAN_ID_TYPE>::value
                                    ? "(signed)\n"
                                    : "(unsigned)\n";
        std::cout << my_signed_string;
      }
      // {
      //   std::cout << "Message: ParMETIS idx_t = " << sizeof(idx_t) * 8
      //             << " bits ";
      //   auto my_signed_string =
      //       std::is_signed<idx_t>::value ? "(signed)\n" : "(unsigned)\n";
      //   std::cout << my_signed_string;
      // }
      // std::cout << "Message: ParMETIS real_t = " << sizeof(real_t) * 8
      //           << " bits\n";
    }
    TreePartApp app(nargs, args);
  }
  MPI_Finalize();
  return EXIT_SUCCESS;
}
