/**
 *  @file connectivity.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 18 June 2019
 *  @brief Documentation for connectivity.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "distributed_hash.hpp"
#include <algorithm>
#include <iostream>
#include <sstream>

namespace taru {

/**
 * @brief Function extracts unique set of nodes in element @ref connectivity
 *        and assumes a round-robin distribution of these nodes in an assumed
 *        partitioning and provides the MPI send information of the unique
 *        nodes to the distribution (used to extract the processor shared
 *        nodes)
 * @tparam T
 * @param connectivity
 * @param comm
 * @param unique_nodes
 * @param send_proc_rank
 * @param send_proc_displ
 */
template <typename T>
my_size_t MakeUnique(const T &connectivity,
                     CommunicatorConcept &comm,
                     std::vector<id_size_t> &unique_nodes,
                     std::vector<rank_t> &send_proc_rank,
                     std::vector<std::size_t> &send_proc_displ) {
  int threshold, quotient, remainder;
  my_size_t returnval = 0;

  if (comm.IsValid()) {
    std::set<id_size_t> unique;
    /**
     *  Get local set of unique global node ids
     */
    for (const auto &item : connectivity)
      for (const auto &node : item.nodes)
        if (node > 0)
          unique.insert(node);
    std::copy(unique.begin(), unique.end(), std::back_inserter(unique_nodes));
    /**
     *  Sort local list of nodes by processor
     *  distribution (round robin distribution)
     */
    auto num_procs = comm.size();
    auto num_nodes =
        *(std::max_element(unique_nodes.begin(), unique_nodes.end()));
    comm.AllReduceMax(&num_nodes, 1);
    quotient = num_nodes / num_procs;
    remainder = num_nodes % num_procs;
    threshold = remainder * (quotient + 1);
    std::sort(unique_nodes.begin(),
              unique_nodes.end(),
              [threshold, quotient, remainder](const id_size_t &i,
                                               const id_size_t &j) {
                auto i_proc =
                    PROC_ID(int64_t(i), threshold, quotient, remainder);
                auto j_proc =
                    PROC_ID(int64_t(j), threshold, quotient, remainder);
                return i_proc < j_proc;
              });

    send_proc_rank.push_back(
        PROC_ID(int64_t(unique_nodes[0]), threshold, quotient, remainder));
    send_proc_displ.push_back(0);
    for (my_size_t i = 1; i < unique_nodes.size(); ++i) {
      auto my_proc_id =
          PROC_ID(int64_t(unique_nodes[i]), threshold, quotient, remainder);
      if (send_proc_rank.back() != my_proc_id) {
        send_proc_rank.push_back(my_proc_id);
        send_proc_displ.push_back(i);
      }
    }
    send_proc_displ.push_back(unique_nodes.size());
    returnval = num_nodes;
  }
  return returnval;
}

/**
 * TODO: Add this to @refitem MakeUnique and avoid repetition
 * @param comm
 * @param unique_nodes
 * @param num_nodes
 * @param send_proc_rank
 * @param send_proc_displ
 */
inline void InferAssumedSchedule(CommunicatorConcept &comm,
                                 std::vector<id_size_t> &unique_nodes,
                                 const id_size_t num_nodes,
                                 std::vector<rank_t> &send_proc_rank,
                                 std::vector<std::size_t> &send_proc_displ) {
  send_proc_rank.clear();
  send_proc_displ.clear();
  int64_t num_procs = comm.size();
  int64_t quotient = num_nodes / num_procs;
  int64_t remainder = num_nodes % num_procs;
  int64_t threshold = remainder * (quotient + 1);
  std::sort(
      unique_nodes.begin(),
      unique_nodes.end(),
      [threshold, quotient, remainder](const id_size_t &i, const id_size_t &j) {
        auto i_proc = PROC_ID(int64_t(i), threshold, quotient, remainder);
        auto j_proc = PROC_ID(int64_t(j), threshold, quotient, remainder);
        return i_proc < j_proc;
      });

  send_proc_rank.push_back(
      PROC_ID(int64_t(unique_nodes[0]), threshold, quotient, remainder));
  send_proc_displ.push_back(0);
  for (my_size_t i = 1; i < unique_nodes.size(); ++i) {
    auto my_proc_id =
        PROC_ID(int64_t(unique_nodes[i]), threshold, quotient, remainder);
    if (send_proc_rank.back() != my_proc_id) {
      send_proc_rank.push_back(my_proc_id);
      send_proc_displ.push_back(i);
    }
  }
  send_proc_displ.push_back(unique_nodes.size());
}

/**
 * @brief Function gives the communication schedule
 * @tparam T_Payload
 * @param tree
 * @param schedule
 */
template <typename T_Payload>
void GetCommunicationSchedule(
    CommunicatorConcept &comm,
    const T_Payload &payload,
    std::vector<rank_t> &schedule,
    const DistributedHashTable<id_size_t, Point3d> *node_dht_ptr = nullptr) {
  std::vector<id_size_t> unique_nodes;
  std::vector<rank_t> send_proc_rank;
  std::vector<std::size_t> send_proc_displ;
  MakeUnique(payload.connectivity,
             comm,
             unique_nodes,
             send_proc_rank,
             send_proc_displ);
  comm.Barrier();
  /**
   *  Infer the number of receives
   */
  auto num_recvs = comm.InferNumReceive(send_proc_rank);
  assert(num_recvs != 0);
  std::vector<MPI_Request> send_req(send_proc_rank.size());
  std::vector<MPI_Status> send_stat(send_proc_rank.size());
  rank_t my_rank = comm.Rank();
  /**
   *  Send data to dictionary rank
   */
  for (std::size_t i = 0; i < send_proc_rank.size(); ++i) {
    auto send_count = send_proc_displ[i + 1] - send_proc_displ[i];
    auto *send_buf = &unique_nodes[send_proc_displ[i]];
    auto send_rank = send_proc_rank[i];
    MPI_Isend(send_buf,
              send_count,
              GetMpiType<id_size_t>(),
              send_rank,
              my_rank,
              comm.RawHandle(),
              &send_req[i]);
  }
  /**
   *  Probe in recv end for alloc and blocking recv
   */
  std::vector<id_size_t> temp_buffer;
  std::map<id_size_t, std::vector<rank_t>> node_proc_map;
  MPI_Status recv_stat;
  for (int i = 0; i < num_recvs; ++i) {
    int recv_size;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
    MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
    temp_buffer.resize(recv_size);
    MPI_Recv(&temp_buffer[0],
             recv_size,
             GetMpiType<id_size_t>(),
             recv_stat.MPI_SOURCE,
             recv_stat.MPI_TAG,
             comm.RawHandle(),
             &recv_stat);
    for (const auto &item : temp_buffer)
      node_proc_map[item].push_back(recv_stat.MPI_SOURCE);
  }
  temp_buffer.clear();
  /**
   *  Wait till all send is completed
   */
  MPI_Waitall(send_proc_rank.size(), send_req.data(), send_stat.data());
  /**
   *  Remove items that are not shared nodes
   */
  for (auto item = node_proc_map.cbegin(); item != node_proc_map.cend();) {
    if (item->second.size() == 1)
      item = node_proc_map.erase(item);
    else
      ++item;
  }
  if (node_dht_ptr != nullptr) {
    auto &node_dht = *node_dht_ptr;
    std::vector<id_size_t> keys;
    for (const auto &item : node_proc_map)
      keys.push_back(item.first - 1);
    std::vector<Point3d> shared_nodes(keys.size());
    node_dht.GetDataFromKeys(keys, shared_nodes);
    std::stringstream cat;
    cat << "rank_data_" << my_rank << ".dat";
    std::ofstream fout(cat.str());
    fout << R"(VARIABLES="N","V","X","Y","Z")"
         << "\n";
    for (std::size_t i = 0; i < shared_nodes.size(); ++i) {
      auto &item = shared_nodes[i];
      fout << keys[i] << "  " << node_proc_map[keys[i] + 1].size() << "  "
           << item.x << "  " << item.y << "  " << item.z << "\n";
    }
  }
  /**
   *  Now send shared node data to respective processor
   */
  std::map<rank_t, std::set<rank_t>> temp_rank_to_rank;
  for (const auto &item : node_proc_map)
    for (const auto &rank : item.second)
      temp_rank_to_rank[rank].insert(item.second.begin(), item.second.end());
  std::map<rank_t, std::vector<rank_t>> rank_to_rank;
  for (const auto &item : temp_rank_to_rank) {
    auto cur_rank = item.first;
    std::copy_if(item.second.begin(),
                 item.second.end(),
                 std::back_inserter(rank_to_rank[item.first]),
                 [&cur_rank](rank_t i) { return (i != cur_rank); });
  }
  /**
   *  Clear all send ranks
   */
  send_proc_rank.clear();
  send_proc_rank.reserve(rank_to_rank.size());
  for (const auto &item : rank_to_rank)
    send_proc_rank.push_back(item.first);
  num_recvs = comm.InferNumReceive(send_proc_rank);
  assert(num_recvs != 0);
  /**
   *  Send data to dictionary rank
   */
  auto counter = 0;
  std::vector<MPI_Request> new_send_req(send_proc_rank.size());
  std::vector<MPI_Status> new_send_stat(send_proc_rank.size());
  for (const auto &item : rank_to_rank) {
    auto send_count = item.second.size();
    auto *send_buf = &(item.second[0]);
    auto send_rank = item.first;
    MPI_Isend(send_buf,
              send_count,
              GetMpiType<rank_t>(),
              send_rank,
              my_rank,
              comm.RawHandle(),
              &new_send_req[counter++]);
  }
  /**
   *  Probe in recv end for alloc and blocking recv
   */
  std::vector<rank_t> temp_rank_buffer;
  std::set<rank_t> schedule_set;
  for (int i = 0; i < num_recvs; ++i) {
    int recv_size;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
    MPI_Get_count(&recv_stat, GetMpiType<rank_t>(), &recv_size);
    temp_rank_buffer.resize(recv_size);
    MPI_Recv(&temp_rank_buffer[0],
             recv_size,
             GetMpiType<rank_t>(),
             recv_stat.MPI_SOURCE,
             recv_stat.MPI_TAG,
             comm.RawHandle(),
             &recv_stat);
    schedule_set.insert(temp_rank_buffer.begin(), temp_rank_buffer.end());
  }
  /**
   *  Wait till all send is completed
   */
  MPI_Waitall(send_proc_rank.size(), new_send_req.data(), new_send_stat.data());
  /**
   *  Copy to schedule
   */
  std::copy_if(schedule_set.begin(),
               schedule_set.end(),
               std::back_inserter(schedule),
               [&my_rank](rank_t i) { return i != my_rank; });
}

} // namespace taru
