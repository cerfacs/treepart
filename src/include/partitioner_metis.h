#pragma once

#include "treepart_config.h"

#ifdef TARU_USE_METIS

#include "parmetis.h"
#include "metis.h"

#define partition_metis(args...) ParMETIS_V3_PartGeomKway(args)
#define partition_metis_serial(args...) METIS_PartMeshDual(args)

#else
/**
 * @brief
 *
 * @param vtxdist
 * @param xadj
 * @param adjncy
 * @param vtxwgt
 * @param adjwgt
 * @param wgtflag
 * @param numflag
 * @param ndims
 * @param xyz
 * @param ncon
 * @param nparts
 * @param tpwgts
 * @param ubvec
 * @param options
 * @param edgecut
 * @param part
 * @param comm
 * @return int
 */
inline int partition_metis(taru::id_size_t *vtxdist,
                           taru::id_size_t *xadj,
                           taru::id_size_t *adjncy,
                           taru::id_size_t *vtxwgt,
                           taru::id_size_t *adjwgt,
                           taru::id_size_t *wgtflag,
                           taru::id_size_t *numflag,
                           taru::id_size_t *ndims,
                           double *xyz,
                           taru::id_size_t *ncon,
                           taru::id_size_t *nparts,
                           double *tpwgts,
                           double *ubvec,
                           taru::id_size_t *options,
                           taru::id_size_t *edgecut,
                           taru::id_size_t *part,
                           MPI_Comm *comm) {
  throw std::runtime_error("METIS partitioner interface is unavailable.");
  return 0;
}

int partition_metis_serial(taru::id_size_t *ne,
                           taru::id_size_t *nn,
                           taru::id_size_t *eptr,
                           taru::id_size_t *eind,
                           taru::id_size_t *vwgt,
                           taru::id_size_t *vsize,
                           taru::id_size_t *ncommon,
                           taru::id_size_t *nparts,
                           taru::id_size_t *tpwgts,
                           taru::id_size_t *options,
                           taru::id_size_t *objval,
                           taru::id_size_t *epart,
                           taru::id_size_t *npart) {
  throw std::runtime_error("METIS partitioner interface is unavailable.");
  return 0;
}

#endif
