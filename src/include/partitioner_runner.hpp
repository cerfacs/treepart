/** @file partitioner_runner.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 06 May 2019
 *  @brief Documentation for partitioner_runner.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "communicator_taru.hpp"
#include "global_timer.hpp"
#include "mesh_common.hpp"
#include <zoltan.h>

#define ZOLTAN_LB_DEBUG_LEVEL "0"

namespace taru {

/**
 * @brief This is the partition runner class
 *        responsible for configuring, managing
 *        and running the load-balancing algorithms
 *
 * @tparam T_Strategy
 * @tparam T_Payload
 */
template <typename T_Payload> class PartitionRunner {
public:
  typedef std::pair<rank_t, id_size_t> part_perm_t;

  /**
   * @brief Run the partitioning algorithm
   *
   * @tparam T_BootstrapFunctor
   * @param input_tree
   * @param bootstrap_functor
   */
  template <typename T_BootstrapFunctor>
  void Run(TreeCommunicator<T_Payload> &input_tree,
           T_BootstrapFunctor &bootstrap_functor);

  /**
   * @brief Data migration function to migrate payload data given the
   *        parts array (from external partitioner). Assumes the payload
   *        data is distributed between the ranks of comm.
   *
   * @param comm
   * @param payload
   * @param parts
   */
  void MigrateUsingParts(CommunicatorConcept &comm,
                         T_Payload &payload,
                         std::vector<rank_t> &parts) {

#if 0
    std::stringstream cat;
    cat << "before_mig" << comm.Rank() << ".dat";
    std::ofstream fout_before(cat.str());
    cat.str("");
    cat << "after_mig" << comm.Rank() << ".dat";
    std::ofstream fout_after(cat.str());

    // Data before permutation and send/recv occured
    for( id_size_t i=0; i< payload.global_ids.size(); ++i ) {
      fout_before << payload.global_ids[i] << " [";
      fout_before << payload.coordinates[i].x << " ";
      fout_before << payload.coordinates[i].y << " ";
      fout_before << payload.coordinates[i].z << "] ";
      fout_before << "[ ";
      for( const auto &item : payload.connectivity[i].nodes)
        fout_before << item << " ";
      fout_before << "] ";
      if( !payload.weights.empty() )
        fout_before << payload.weights[i] << "\n";
    }
#endif

    // Sort the local list of nodes based on the processor id and get that
    // permutation
    std::vector<part_perm_t> part_perm(parts.size());
    for (std::size_t i = 0; i < parts.size(); ++i)
      part_perm[i] = std::make_pair(parts[i], i);
    parts.clear();
    std::sort(part_perm.begin(),
              part_perm.end(),
              [](const part_perm_t &a, const part_perm_t &b) {
                return a.first < b.first;
              });
    // form offset array and copy perm to vector
    std::vector<rank_t> exportRanks;
    std::vector<my_size_t> offset;
    rank_t current_rank = part_perm.front().first;
    my_size_t count = 0;
    offset.push_back(count);
    exportRanks.push_back(current_rank);
    for (const auto &item : part_perm) {
      if (item.first != current_rank) {
        offset.push_back(count);
        exportRanks.push_back(item.first);
        current_rank = item.first;
      }
      count++;
    }
    offset.push_back(part_perm.size());
    auto num_recvs = comm.InferNumReceive(exportRanks, true);

    std::vector<id_size_t> perm;
    perm.reserve(part_perm.size());
    for (const auto &item : part_perm)
      perm.push_back(item.second);
    // Use the permutation and re-arrange the data
    InplacePermutation(perm.size(),
                       &perm[0],
                       &(payload.coordinates[0]),
                       &(payload.global_ids[0]),
                       &(payload.connectivity[0]),
                       &(payload.weights[0]));
    perm.clear();
    // Send/Recv the data using offset and stride
    comm.SendRecvVector(num_recvs, exportRanks, offset, payload.global_ids);
    comm.SendRecvVector(num_recvs, exportRanks, offset, payload.coordinates);
    comm.SendRecvVector(num_recvs, exportRanks, offset, payload.connectivity);
    comm.SendRecvVector(num_recvs, exportRanks, offset, payload.weights);
#if 0
    // Data after permutation and send/recv occured
    for( id_size_t i=0; i< payload.global_ids.size(); ++i ) {
      fout_after << payload.global_ids[i] << " [";
      fout_after << payload.coordinates[i].x << " ";
      fout_after << payload.coordinates[i].y << " ";
      fout_after << payload.coordinates[i].z << "] ";
      fout_after << "[ ";
      for( const auto &item : payload.connectivity[i].nodes)
        fout_after << item << " ";
      fout_after << "] ";
      if( !payload.weights.empty() )
        fout_after << payload.weights[i] << "\n";
    }
#endif
  }

  /**
   * @brief Partition the local payload (distributed over ranks) using
   *        graph partitioning
   *
   * @param comm
   * @param payload
   */
  rank_t PartitionGraphParallel(CommunicatorConcept &comm, T_Payload &payload) {
    std::vector<rank_t> parts;
    auto edge_cuts = payload.GetElementParts(comm, parts);
    MigrateUsingParts(comm, payload, parts);
    return edge_cuts;
  }

  /**
   * @brief Partition the local payload using serial graph partitioning
   *
   * @param num_parts
   * @param payload
   * @param offset
   * @todo Investigate problems with large mesh decomposition (hierarchical)
   */
  int PartitionGraphSerial(CommunicatorConcept &comm,
                           T_Payload &payload,
                           rank_t debug_rank = 0) {
    int return_edge_cuts = 0;
    if (comm.size() > 1) {
      std::vector<my_size_t> offset;
      if (comm.IsMaster()) {
        auto epart = payload.GetElementPartsSerial(comm, return_edge_cuts);
        id_size_t ne = epart.size();
        /**
         * Sort permutation pair using partition id as key
         */
        std::vector<part_perm_t> part_perm(ne);
        for (decltype(ne) i = 0; i < ne; ++i) {
          part_perm[i].first = epart[i];
          part_perm[i].second = i;
        }
        epart.clear();
        epart.shrink_to_fit();
        std::sort(part_perm.begin(),
                  part_perm.end(),
                  [](const part_perm_t &a, const part_perm_t &b) {
                    return a.first < b.first;
                  });

        rank_t current_rank = part_perm.front().first;
        offset.clear();
        my_size_t count = 0;
        offset.push_back(count);
        for (const auto &item : part_perm) {
          if (item.first != current_rank) {
            offset.push_back(count);
            current_rank = item.first;
          }
          ++count;
        }
        offset.push_back(part_perm.size());
        std::vector<id_size_t> perm;
        perm.reserve(part_perm.size());
        for (const auto &item : part_perm)
          perm.push_back(item.second);
        /**
         * Permute the data as per the partition information from Metis
         * and create offset information to cascade to lower levels
         */
        InplacePermutation(perm.size(),
                           &perm[0],
                           &(payload.coordinates[0]),
                           &(payload.global_ids[0]),
                           &(payload.connectivity[0]),
                           &(payload.weights[0]));
      }
      comm.CascadeMasterToAll(payload.global_ids, offset);
      comm.CascadeMasterToAll(payload.coordinates, offset);
      comm.CascadeMasterToAll(payload.weights, offset);
      comm.CascadeMasterToAll(payload.connectivity, offset);
    }
    return return_edge_cuts;
  }

  /**
   * @brief Sets the defaults of the Zoltan load balancing parameters
   *
   * @param zz
   * @param lb_method
   */
  static void SetLoadBalanceParameters(Zoltan_Struct *zz,
                                       const char *lb_method) {
    Zoltan_Set_Param(zz, "DEBUG_LEVEL", ZOLTAN_LB_DEBUG_LEVEL);
    Zoltan_Set_Param(zz, "LB_METHOD", lb_method);
    Zoltan_Set_Param(zz, "NUM_GID_ENTRIES", "1");
    Zoltan_Set_Param(zz, "NUM_LID_ENTRIES", "1");
    Zoltan_Set_Param(zz, "RETURN_LISTS", "ALL");
    Zoltan_Set_Param(zz, "KEEP_CUTS", "0");
    Zoltan_Set_Param(zz, "OBJ_WEIGHT_DIM", "1");
    if (std::string(lb_method) == "RCB") {
      Zoltan_Set_Param(zz, "RCB_OUTPUT_LEVEL", "0");
      Zoltan_Set_Param(zz, "REDUCE_DIMENSIONS", "0");
      Zoltan_Set_Param(zz, "RCB_RECTILINEAR_BLOCKS", "1");
      Zoltan_Set_Param(zz, "RCB_RECOMPUTE_BOX", "1");
      Zoltan_Set_Param(zz, "RCB_MAX_ASPECT_RATIO", "1.0");
    }
  }

  /**
   * @brief Partitions the payload given the communicator at any level
   *
   * @param communicator
   * @param payload
   */
  static void PartitionPayload(CommunicatorConcept &communicator,
                               T_Payload &payload) {
    // Allocate Zoltan load balance structure
    Zoltan_Struct *zz = Zoltan_Create(communicator.RawHandle());
    // Set partitioning parameters
    auto part_string = get_partition_name(payload.GetPartitionMethod());
    SetLoadBalanceParameters(zz, part_string.c_str());
    // Set migration and load-balance query callback functions
    payload.RegisterCallback(zz);
    Time(global_timer::TIME_PARTITIONING_CORE, global_timer::C_TimerStart);
    // Perform the load balancing and migrate
    LoadBalanceAndMigrate(zz);
    Time(global_timer::TIME_PARTITIONING_CORE, global_timer::C_TimerStop);
    // Cleanup Zoltan load balance structure
    Zoltan_Destroy(&zz);
  }

  /**
   * @brief Performs the load balancing and migration
   *        of the payload after partitioning
   *
   * @param zz
   */
  static void LoadBalanceAndMigrate(struct Zoltan_Struct *zz) {
    int changes, numGidEntries, numLidEntries, numImport, numExport;
    ZOLTAN_ID_PTR importGlobalGids, importLocalGids, exportGlobalGids,
        exportLocalGids;
    int *importProcs, *importToPart, *exportProcs, *exportToPart;

    int rc = Zoltan_LB_Partition(
        zz,                /* input (all remaining fields are output) */
        &changes,          /* 1 if partitioning was changed, 0 otherwise */
        &numGidEntries,    /* Number of integers used for a global ID */
        &numLidEntries,    /* Number of integers used for a local ID */
        &numImport,        /* Number of vertices to be sent to me */
        &importGlobalGids, /* Global IDs of vertices to be sent to me */
        &importLocalGids,  /* Local IDs of vertices to be sent to me */
        &importProcs,      /* Process rank for source of each incoming vertex*/
        &importToPart,     /* New partition for each incoming vertex */
        &numExport, /* Number of vertices I must send to other processes*/
        &exportGlobalGids, /* Global IDs of the vertices I must send */
        &exportLocalGids,  /* Local IDs of the vertices I must send */
        &exportProcs,      /* Process to which I send each of the vertices */
        &exportToPart);    /* Partition to which each vertex will belong */

    if (rc != ZOLTAN_OK) {
      Zoltan_Destroy(&zz);
      abort();
    }

    rc = Zoltan_Migrate(zz,
                        numImport,
                        importGlobalGids,
                        importLocalGids,
                        importProcs,
                        importToPart,
                        numExport,
                        exportGlobalGids,
                        exportLocalGids,
                        exportProcs,
                        exportToPart);

    if (rc != ZOLTAN_OK) {
      Zoltan_Destroy(&zz);
      abort();
    }

    Zoltan_LB_Free_Part(
        &importGlobalGids, &importLocalGids, &importProcs, &importToPart);
    Zoltan_LB_Free_Part(
        &exportGlobalGids, &exportLocalGids, &exportProcs, &exportToPart);
  }
};

} // End of namespace taru
