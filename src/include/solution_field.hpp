/**
 * @file solution_field.hpp
 * @author your name (you@domain.com)
 * @brief
 * @date 2021-01-25
 *
 * @copyright Copyright (c) 2021
 *
 */
#pragma once

#include "distributed_hash.hpp"
#include <cstring>

namespace taru {

struct SolutionField {

  typedef taru::DistributedHashTable<id_size_t, double> ScalarDHT;

  const unsigned int MAX_NAME = 1024;

  SolutionField() {}

  SolutionField(const std::string &solution_file,
                const std::vector<std::string> &solution_group_ignore,
                const std::string &metric_file,
                const std::string &metric_dset,
                taru::CommunicatorConcept &comm) {
    DefaultConstructor(
        solution_file, solution_group_ignore, metric_file, metric_dset, comm);
  }

  void DefaultConstructor(const std::string &solution_file,
                          const std::vector<std::string> &solution_group_ignore,
                          const std::string &metric_file,
                          const std::string &metric_dset,
                          taru::CommunicatorConcept &comm) {
    if (comm.IsMaster() && !solution_file.empty()) {
      /* Open HDF5 file in serial mode and read sizes */
      auto fhandle =
          H5Fopen(solution_file.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
      // Loop over all groups
      hsize_t nobj;
      auto status = H5Gget_num_objs(fhandle, &nobj);
      if (status < 0)
        throw std::runtime_error("Error reading group info");
      for (hsize_t i = 0; i < nobj; i++) {
        char memb_name[MAX_NAME];
        auto len =
            H5Gget_objname_by_idx(fhandle, i, memb_name, (size_t)MAX_NAME);
        if (len < 0)
          std::runtime_error("Negative length in group");
        auto otype = H5Gget_objtype_by_idx(fhandle, (size_t)i);
        switch (otype) {
        // Search all group and only add the ones that are not in ignore list
        case H5G_GROUP:
          std::cout << "/Message: Treating Groups:\n";
          if (std::find(solution_group_ignore.begin(),
                        solution_group_ignore.end(),
                        std::string(memb_name)) ==
              std::end(solution_group_ignore)) {
            std::cout << "         "
                      << "/" << memb_name << "\n";
            auto grpid = H5Gopen(fhandle, memb_name, H5P_DEFAULT);
            m_solution_groups.push_back(memb_name);
            H5Gclose(grpid);
          }
          break;

        default:
          break;

        } // end of switch

      } // end of group loop

      // Read all dataset names in group list
      for (auto &mygroup : m_solution_groups) {
        auto gid = H5Gopen(fhandle, mygroup.c_str(), H5P_DEFAULT);
        // hsize_t nobj;
        auto status = H5Gget_num_objs(gid, &nobj);
        if (status < 0)
          throw std::runtime_error("Error reading group info");
        for (hsize_t i = 0; i < nobj; i++) {
          char memb_name[MAX_NAME];
          auto len = H5Gget_objname_by_idx(gid, i, memb_name, (size_t)MAX_NAME);
          if (len < 0)
            std::runtime_error("Negative length in group");
          auto otype = H5Gget_objtype_by_idx(gid, (size_t)i);
          switch (otype) {
          // Search all goup and only add the ones that are not in ignore list
          case H5G_DATASET:
            // std::cout << "/" << mygroup <<  "/" << memb_name << "\n";
            m_solution_dset.push_back("/" + mygroup + "/" + memb_name);
            break;

          default:
            break;

          } // end of switch

        } // end of dataset loop
        H5Gclose(gid);
      } // end of permissible groups
      H5Fclose(fhandle);
    } // Master rank
    hsize_t num_dset = m_solution_dset.size();
    comm.BroadcastMaster(&num_dset, 1);
    m_solution_dset.resize(num_dset);
    if (comm.IsMaster()) {
      if (num_dset > 0)
        std::cout << "Message: Found " << num_dset << " variables\n";
      else
        std::cout << "Message: No solution variables running a pure adaptation "
                     "case\n";
    }
    for (hsize_t i = 0; i < num_dset; ++i) {
      auto str_length = m_solution_dset[i].size();
      comm.BroadcastMaster(&str_length, 1);
      m_solution_dset[i].resize(str_length);
      auto ctemp_ptr = std::unique_ptr<char>(new char[str_length + 1]);
      auto ctemp = &(*ctemp_ptr);
      std::fill(ctemp, ctemp + str_length + 1, '\0');
      if (comm.IsMaster()) {
        std::strcpy(ctemp, m_solution_dset[i].c_str());
        // std::cout << "Message: " << i + 1 << " - " << ctemp << "\n";
      }
      comm.BroadcastMaster(ctemp, str_length);
      if (!comm.IsMaster())
        m_solution_dset[i].assign(ctemp);
      comm.Barrier();
    }
    m_solution_dht.resize(num_dset, ScalarDHT(comm));
    for (size_t i = 0; i < num_dset; ++i) {
      if (comm.IsMaster())
        std::cout << "Message: Populate solution " << solution_file << " -- "
                  << m_solution_dset[i] << "\n";
      m_solution_dht[i].Populate<double>(solution_file, m_solution_dset[i]);
    }
    // By default allocate one empty dict for metric
    // but do not populate it with data
    m_metric_dht.resize(1, ScalarDHT(comm));
    if (!metric_file.empty())
      m_metric_dht[0].Populate<double>(metric_file, metric_dset);
  }

  /**
   *
   * @return
   */
  const ScalarDHT &MetricHashTable() { return m_metric_dht[0]; }

  /**
   *
   * @return
   */
  ScalarDHT &GetMetricHashTable() { return m_metric_dht[0]; }

  template <typename T_Int, typename T_Map, typename T_Vector>
  void UpdateMetric(T_Int &num_nodes,
                    T_Map &unique,
                    T_Vector &keys,
                    const std::vector<double> &metric_value) {
    GetMetricHashTable().clear();
    GetMetricHashTable().SetSize(num_nodes);
    std::vector<double> temp_metric_value;
    temp_metric_value.reserve(keys.size());
    for (auto &item : unique)
      temp_metric_value.push_back(metric_value[item.first]);
    GetMetricHashTable().Populate(keys, temp_metric_value);
  }

  /**
   *
   * @param file_name
   */
  void CreateFieldFile(const std::string &file_name,
                       CommunicatorConcept &comm) {
    if (comm.IsMaster()) {
      auto file =
          H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
      for (auto &field_group : m_solution_groups)
        hdf5::CreateGroup(file, field_group.c_str());
      H5Fclose(file);
    }
    comm.Barrier();
  }

  /**
   *
   * @tparam T_Real
   * @param file_name
   * @param group_name
   * @param dset_name
   * @param metric
   * @param owned_map
   * @param comm
   * @param create_file
   */
  template <typename T_Real>
  static void
  WriteScalarToFile(const std::string &file_name,
                    const std::string &dset_name,
                    const std::vector<T_Real> &metric,
                    const std::map<id_size_t, id_size_t> &inv_owned_map,
                    taru::CommunicatorConcept &comm) {
    hdf5::Hyperslab<hsize_t, 1> m, f;
    auto min_vertex_gid = inv_owned_map.begin()->first;
    auto max_vertex_gid = std::prev(inv_owned_map.end())->first;
    m.dims[0] = max_vertex_gid - min_vertex_gid + 1;
    m.count[0] = f.count[0] = max_vertex_gid - min_vertex_gid + 1;
    m.stride[0] = f.stride[0] = 1;
    f.dims[0] = max_vertex_gid;
    f.offset[0] = min_vertex_gid - 1;
    MPI_Bcast(f.dims,
              1,
              taru::GetMpiType<hsize_t>(),
              comm.size() - 1,
              comm.RawHandle());

    /* Create plist object for I/O */
    auto plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, comm.RawHandle(), MPI_INFO_NULL);
    /* Open file collectively */
    auto file = H5Fopen(file_name.c_str(), H5F_ACC_RDWR, plist_id);
    H5Pclose(plist_id);
    std::vector<double> temp_metric;
    temp_metric.reserve(inv_owned_map.size());
    for (const auto &item : inv_owned_map)
      temp_metric.push_back(metric[item.second]);
    hdf5::WriteCollective(file, dset_name, f, m, temp_metric);
    H5Fclose(file);
  }

  /**
   *
   * @tparam T_Real
   * @param file_name
   * @param group_name
   * @param dset_name
   * @param metric
   * @param owned_map
   * @param comm
   * @param create_file
   */
  template <typename T_Real>
  static void
  WriteScalarToFile(const std::string &file_name,
                    const std::string &group_name,
                    const std::string &dset_name,
                    const std::vector<T_Real> &metric,
                    const std::map<id_size_t, id_size_t> &inv_owned_map,
                    taru::CommunicatorConcept &comm,
                    bool create_file) {
    hdf5::Hyperslab<hsize_t, 1> m, f;
    // Inverse permutation keys are in sorted order ^_^
    auto min_vertex_gid = inv_owned_map.begin()->first;
    auto max_vertex_gid = std::prev(inv_owned_map.end())->first;
    m.dims[0] = max_vertex_gid - min_vertex_gid + 1;
    m.count[0] = f.count[0] = max_vertex_gid - min_vertex_gid + 1;
    m.stride[0] = f.stride[0] = 1;
    f.dims[0] = max_vertex_gid;
    f.offset[0] = min_vertex_gid - 1;
    MPI_Bcast(f.dims,
              1,
              taru::GetMpiType<hsize_t>(),
              comm.size() - 1,
              comm.RawHandle());

    /* Create plist object for I/O */
    auto plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, comm.RawHandle(), MPI_INFO_NULL);
    /* Open file collectively */
    hid_t file;
    if (create_file)
      file = H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    else
      file = H5Fopen(file_name.c_str(), H5F_ACC_RDWR, plist_id);
    H5Pclose(plist_id);
    std::vector<double> temp_metric;
    temp_metric.reserve(inv_owned_map.size());
    for (const auto &item : inv_owned_map)
      temp_metric.push_back(metric[item.second]);
    if (create_file)
      hdf5::CreateGroup(file, group_name.c_str());
    hdf5::WriteCollective(
        file, group_name + "/" + dset_name, f, m, temp_metric);
    H5Fclose(file);
  }

  void SetPrefix(const std::string &prefix) { m_prefix = prefix; }

  const std::string &GetPrefix() { return m_prefix; }

  // data access
  std::string m_prefix = "untitled";
  std::vector<std::string> m_solution_dset;
  std::vector<std::string> m_solution_groups;
  std::vector<ScalarDHT> m_solution_dht;
  std::vector<ScalarDHT> m_metric_dht; //!< Allow multiple metrics
};

struct SolutionFieldInterface {

public:
  int m_offset = 0;
  int m_stride = 0;
  double *m_data_ptr = nullptr;

  /**
   *
   * @tparam T_Int
   * @param offset
   * @param stride
   * @param data_ptr
   */
  template <typename T_Int>
  SolutionFieldInterface(const T_Int offset,
                         const T_Int stride,
                         double *data_ptr)
      : m_offset(offset),
        m_stride(stride),
        m_data_ptr(data_ptr) {}
};

} // namespace taru
