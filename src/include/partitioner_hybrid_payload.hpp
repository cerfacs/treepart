/**
 *  @file partitioner_hybrid_payload.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 16 May 2019
 *  @brief Documentation for partitioner_hybrid_payload.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */
#pragma once

#include "hybrid_payload.hpp"
#include "partitioner_runner.hpp"
#include "partition_method.hpp"

namespace taru {

/**
 * @brief Runs the partitioning process on the input tree using the specified
 * bootstrap functor. Template specialisation of @ref PartitionRunner for @ref
 * HybridPayload types.
 *
 * This function performs the partitioning process on the input tree using the
 * specified bootstrap functor. It starts by bootstrapping and partitioning at
 * the bootstrap level, then iterates over the levels of the tree and performs
 * the cascade and partitioning operations on the leafs. It also swaps payloads
 * between levels and clears the data in the current level to prevent memory
 * overhead.
 *
 * @tparam T_BootstrapFunctor The type of the bootstrap functor.
 * @param input_tree The input tree to be partitioned.
 * @param bootstrap_functor The bootstrap functor used for partitioning.
 */
template <>
template <typename T_BootstrapFunctor>
void PartitionRunner<HybridPayload>::Run(
    TreeCommunicator<HybridPayload> &input_tree,
    T_BootstrapFunctor &bootstrap_functor) {
  Time(global_timer::TIME_PARTITIONING, global_timer::C_TimerStart);
  int begin_from = int(input_tree.size()) - 1;
  int end_at = 0;
  /**
   *  Checks to prevent sending undefined index values for levels
   */
  assert(begin_from >= end_at);
  assert(begin_from < int(input_tree.size()));
  /**
   *  Bootstrap and partition at the bootstrap level
   */
  bootstrap_functor(input_tree, begin_from);
  {
    auto &current_branch = input_tree[begin_from].Branch();
    auto &current_payload = input_tree[begin_from].Payload();
    if (current_branch.IsValid()) {
      if (is_graph_partitioner(current_payload.GetPartitionMethod())) {
        auto start_time = MPI_Wtime();
        auto edge_cuts =
            PartitionGraphParallel(current_branch, current_payload);
        auto end_time = MPI_Wtime();
        if (current_branch.IsMaster())
          std::cout << "Message: Parallel graph partitioning ("
                    << get_partition_name(current_payload.GetPartitionMethod())
                    << ") at bootstrap level completed in "
                    << end_time - start_time << " (s) with " << edge_cuts
                    << " edge cuts\n";
      } else {
        auto start_time = MPI_Wtime();
        PartitionPayload(current_branch, current_payload);
        auto end_time = MPI_Wtime();
        if (current_branch.IsMaster())
          std::cout << "Message: Parallel geometric partitioning ("
                    << get_partition_name(current_payload.GetPartitionMethod())
                    << ") at bootstrap level completed in "
                    << end_time - start_time << " (s)\n";
      }
    }
  }
  /**
   *  Loop over level but only for valid communicators at root level
   */
  for (int level_id = begin_from; level_id >= end_at; --level_id) {
    auto &current_payload = input_tree[level_id].Payload();
    auto &current_branch = input_tree[level_id].Branch();
    auto &current_leaf = input_tree[level_id].Leaf();
    /**
     *  Cascade and partition leaf
     */
    if (current_leaf.IsValid()) {
      if (is_graph_partitioner(current_payload.GetPartitionMethod())) {
        if (current_branch.IsValid())
          if (current_branch.IsMaster())
            std::cout << "Message: Serial graph partitioning of leafs at level "
                      << level_id << "\n";
        auto start_time = MPI_Wtime();
        auto edge_cuts = PartitionGraphSerial(
            current_leaf, current_payload, input_tree.Root().Rank());
        auto end_time = MPI_Wtime();
        if (current_branch.IsValid())
          if (current_branch.IsMaster())
            std::cout << "Message: Serial graph partitioning edge_cuts = "
                      << edge_cuts << " time " << end_time - start_time
                      << " (s)\n";
      } else {
        if (current_branch.IsValid())
          if (current_branch.IsMaster())
            std::cout << "Message: Cascading to leafs of level " << level_id
                      << "\n";
        current_leaf.CascadeMasterToAll(current_payload.global_ids);
        current_leaf.CascadeMasterToAll(current_payload.coordinates);
        current_leaf.CascadeMasterToAll(current_payload.weights);
        current_leaf.CascadeMasterToAll(current_payload.connectivity);
        auto start_time = MPI_Wtime();
        PartitionPayload(current_leaf, current_payload);
        auto end_time = MPI_Wtime();
        if (current_branch.IsValid())
          if (current_branch.IsMaster())
            std::cout << "Message: Parallel "
                      << get_partition_name(
                             current_payload.GetPartitionMethod())
                      << " to leafs of level " << level_id << " time "
                      << end_time - start_time << " (s)\n";
      }
    }
    /**
     *  Swap payloads between levels if not at the last level
     *  @note Next level is ``(level_id - 1)`` because the communicator is
     * bottom-up
     */
    if (level_id != end_at) {
      current_payload.coordinates.swap(
          input_tree[level_id - 1].Payload().coordinates);
      current_payload.global_ids.swap(
          input_tree[level_id - 1].Payload().global_ids);
      current_payload.weights.swap(input_tree[level_id - 1].Payload().weights);
      current_payload.connectivity.swap(
          input_tree[level_id - 1].Payload().connectivity);
      // Make sure we clear the data in the current level (swapped)
      // and deallocate to prevent carry-over of extra memory overhead
      current_payload.clear();
      current_payload.shrink_to_fit();
    }
    /**
     *  Block till all participating ranks finish the partitioning
     */
    if (input_tree.IsValidTreeRoot()) {
      input_tree.Root().Barrier();
      if (input_tree.IsMasterTreeRoot()) {
        std::cout << "Message: Partitioned at level " << level_id << " using \""
                  << get_partition_name(current_payload.GetPartitionMethod())
                  << "\" method on " << current_branch.size()
                  << " branches and " << current_leaf.size() << " leafs\n";
      }
    }
  }

  Time(global_timer::TIME_PARTITIONING, global_timer::C_TimerStop);
}

} // namespace taru
