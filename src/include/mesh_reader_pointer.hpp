/**
 *  @file mesh_reader_pointer.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 01 January 2021
 *  @brief Pointer mesh dummy reader for in-memory mesh adaptation
 *        Make sure local payload is populated with local-mesh data.
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 *
 */
#pragma once

#include "distributed_hash.hpp"
#include "mesh_reader_hip.hpp"

namespace taru {

template <typename T_Int, typename T_Comm> struct PointerMeshReader {

  explicit PointerMeshReader(T_Comm &comm)
      : m_comm(comm) {}

  /**
   * @brief Closes the reader interface (do nothing for now)
   */
  void close() {}

  /**
   * @brief Return the total number of cells (sum of local rank cell sizes)
   * @tparam T (int, long, long long) depending on the size of the mesh
   * @param element_sizes (output) array of element sizes for each element type
   */
  template <typename T> void GetElementSizes(T *element_sizes) {
    for (unsigned ielm = 0; ielm < C_NumElementTypes; ++ielm)
      element_sizes[ielm] = 0;
    element_sizes[TRI_ID] = m_ntrielm;
    element_sizes[TET_ID] = m_ntetelm;
    m_comm.AllReduceSum(element_sizes, C_NumElementTypes);
  }

  /**
   * @brief Return of the mesh is 2D
   * @return bool (true if 2D, false if 3D)
   */
  bool Is2D() { return m_is2d; }

  /**
   * @brief Return the total number of nodes (sum of local rank node sizes)
   * @tparam T (int, long, long long) depending on the size of the mesh
   * @param node_size (output) total number of nodes
   */
  template <typename T> void GetNodeSize(T &node_size) {
    node_size = m_mynode;
    m_comm.AllReduceSum(&node_size, 1);
  }

  /**
   * @brief Read all the node coordinates from file and
   *        create the hash table
   * @param file (input) mesh file name to read
   * @param node_dht (output) hash table of node coordinates (id, Point3d)
   */
  void ReadNodeCoordinates(my_size_t global_size,
                           DistributedHashTable<id_size_t, Point3d> &node_dht) {
    // Note that m_mynode is the unique list of nodes so it is sufficient
    // we only include them while forming the hash table of mesh nodes
    node_dht.SetSize(global_size);
    std::vector<id_size_t> keys(m_mynode);
    // Zero indexing for keys in the hash table
    for (decltype(m_mynode) i = 0; i < m_mynode; ++i)
      keys[i] = m_nodelist[i] - 1;
    std::vector<Point3d> data(m_mynode);
    int stride = m_is2d ? 2 : 3;
    for (decltype(m_mynode) i = 0; i < m_mynode; ++i) {
      data[i].x = m_node_x[stride * i];
      data[i].y = m_node_x[stride * i + 1];
      if (!m_is2d)
        data[i].z = m_node_x[stride * i + 2];
    }
    node_dht.Populate(keys, data);
  }

  /**
   * @brief Reads the Element node connectivity from file
   *        and returns the cell centroids
   * @param node_dht (input) hash table of node coordinates (id, Point3d)
   * @param dht (output) hash table of element connectivity (id,
   * ElementConnectivity)
   * @param centroid_dht (output) hash table of element centroids (id, Point3d)
   * @return std::pair<Point3d, Point3d> (min, max) bounding box of the mesh
   */
  std::pair<Point3d, Point3d>
  ReadElements(my_size_t global_size,
               DistributedHashTable<id_size_t, Point3d> &node_dht,
               DistributedHashTable<id_size_t, ElementConnectivity> &dht,
               DistributedHashTable<id_size_t, Point3d> &centroid_dht) {
    // Total elements are sum of triangles and tets (no hybrid suport yet)
    id_size_t num_elems = m_ntrielm + m_ntetelm;
    std::vector<ElementConnectivity> buffer(num_elems);
    std::vector<id_size_t> elm_keys(num_elems);
    id_size_t global_num_elems = num_elems;
    dht.CommunicatorHandle().AllReduceSum(&global_num_elems, 1);
    dht.SetSize(global_num_elems);
    centroid_dht.SetSize(global_num_elems);
    id_size_t elm_begin = 0;
    // Obtain the offsets in each processor to form the element keys
    MPI_Exscan(&num_elems,
               &elm_begin,
               1,
               GetMpiType<id_size_t>(),
               MPI_SUM,
               dht.CommunicatorHandle().RawHandle());
    for (id_size_t i = 0; i < num_elems; ++i)
      elm_keys[i] = i + elm_begin;
    // Calculate the centroid and commit to hash table
    std::vector<Point3d> centroid(num_elems);
    int num_nodes_element = (m_is2d) ? 3 : 4;
    T_Int *element_nodes = (m_is2d) ? m_trielm : m_tetelm;
    int node_stride = m_is2d ? 2 : 3;
    for (id_size_t i_element = 0; i_element < num_elems; ++i_element) {
      for (int iv = 0; iv < num_nodes_element; ++iv) {
        // Get local index of vertex id forming i_element
        auto node_index = element_nodes[num_nodes_element * i_element + iv] - 1;
        // // Convert to global index and add to buffer to populate connectivity
        // // hash table
        buffer[i_element].nodes[iv] = m_nodelist[node_index];
        centroid[i_element].x += m_node_x[node_stride * node_index + 0];
        centroid[i_element].y += m_node_x[node_stride * node_index + 1];
        if (!m_is2d)
          centroid[i_element].z += m_node_x[node_stride * node_index + 2];
      }
      centroid[i_element].x /= num_nodes_element;
      centroid[i_element].y /= num_nodes_element;
      if (!m_is2d)
        centroid[i_element].z /= num_nodes_element;
    }
    centroid_dht.Populate(elm_keys, centroid);
    dht.Populate(elm_keys, buffer);

    // Calculate bounding box
    std::pair<Point3d, Point3d> ret_point =
        std::make_pair(Point3d{std::numeric_limits<double>::max()},
                       Point3d{std::numeric_limits<double>::min()});
    for (T_Int i = 0; i < m_nnode; ++i) {
      const auto &item_x = m_node_x[node_stride * i + 0];
      const auto &item_y = m_node_x[node_stride * i + 1];
      // Min
      if (ret_point.first.x > item_x)
        ret_point.first.x = item_x;
      if (ret_point.first.y > item_y)
        ret_point.first.y = item_y;
      // Max
      if (ret_point.second.x < item_x)
        ret_point.second.x = item_x;
      if (ret_point.second.y < item_y)
        ret_point.second.y = item_y;
      // Z-coordinate for 3D
      if (!m_is2d) {
        const auto &item_z = m_node_x[node_stride * i + 2];
        if (ret_point.first.z > item_z)
          ret_point.first.z = item_z;
        if (ret_point.second.z < item_z)
          ret_point.second.z = item_z;
      }
    }
    return ret_point;
  }

  /**
   * @brief Read the boundary nodes in the master rank given offset and chunk
   * @param num_nodes (input) total number of nodes
   * @param binfo (output) boundary information for each node
   * @return int (number of patches)
   */
  int PopulateBoundaryInfo(my_size_t num_nodes, binfo_t &binfo) {
    if (m_comm.IsValid()) {
      std::map<int, std::vector<id_size_t>> proc_bnodes;
      // Get the node dictionary distribution sizes
      my_size_t local_begin, local_size;
      m_comm.RankDistribution(num_nodes, local_begin, local_size);
      auto num_procs = m_comm.size();
      auto my_rank = m_comm.Rank();
      int64_t quotient = num_nodes / num_procs;
      int64_t remainder = num_nodes % num_procs;
      int64_t threshold = remainder * (quotient + 1);
      for (int ipatch = 0; ipatch < m_npatch; ++ipatch) {
        auto offset = m_bnode_begin[ipatch] - 1;
        auto length = m_bnode_size[ipatch];
        for (decltype(offset) i = offset; i < offset + length; ++i) {
          // Identify the owner processor this element is attached
          const auto global_node_id = m_bnode_to_node[i];
          const auto i_proc =
              PROC_ID(global_node_id, threshold, quotient, remainder);
          proc_bnodes[i_proc].push_back(global_node_id); // Node id
          proc_bnodes[i_proc].push_back(ipatch + 1);     // patch id
        }
      }
      // Copy the local data before infering the send/recv
      binfo.resize(local_size);
      if (!proc_bnodes[my_rank].empty()) {
        for (id_size_t i = 0; i < id_size_t(proc_bnodes[my_rank].size() / 2);
             ++i) {
          auto my_loc = proc_bnodes[my_rank][2 * i] - local_begin - 1;
          auto my_pid = proc_bnodes[my_rank][2 * i + 1]; // patch id
          auto found =
              std::find(binfo[my_loc].begin(), binfo[my_loc].end(), my_pid);
          if (found == std::end(binfo[my_loc]))
            binfo[my_loc].push_back(my_pid);
        }
        proc_bnodes.erase(my_rank);
      }
      // Infer the send/recv and add the neighbour data to my_proc_nodes
      auto num_recv = m_comm.InferNumReceive(proc_bnodes);
      std::vector<MPI_Request> send_req(proc_bnodes.size());
      std::vector<MPI_Status> send_stat(proc_bnodes.size());
      MPI_Status recv_stat;
      // Non-blocking send of data
      int count = 0;
      for (const auto &item : proc_bnodes)
        MPI_Isend(item.second.data(),
                  item.second.size(),
                  GetMpiType<id_size_t>(),
                  item.first,
                  my_rank,
                  m_comm.RawHandle(),
                  &send_req[count++]);
      // Blocking receive of data
      for (int irecv = 0; irecv < num_recv; ++irecv) {
        int recv_size;
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, m_comm.RawHandle(), &recv_stat);
        MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
        std::vector<id_size_t> temp_buffer(recv_size);
        MPI_Recv(temp_buffer.data(),
                 recv_size,
                 GetMpiType<id_size_t>(),
                 recv_stat.MPI_SOURCE,
                 recv_stat.MPI_TAG,
                 m_comm.RawHandle(),
                 &recv_stat);
        for (decltype(recv_size) i = 0; i < recv_size / 2; ++i) {
          auto my_loc = temp_buffer[2 * i] - local_begin - 1;
          auto my_pid = temp_buffer[2 * i + 1]; // patch id
          auto found =
              std::find(binfo[my_loc].begin(), binfo[my_loc].end(), my_pid);
          if (found == std::end(binfo[my_loc]))
            binfo[my_loc].push_back(my_pid);
        }
      }
      // Wait for the send to complete
      MPI_Waitall(proc_bnodes.size(), send_req.data(), send_stat.data());
    }
    return m_npatch;
  }

#if 0 // This is the old boundary read from file (deprecated)
  /**
   *
   * @param num_nodes
   * @param binfo
   * @return
   */
  int PopulateBoundaryInfo(my_size_t num_nodes, binfo_t &binfo) {
    int npatch = 0;
    if (m_comm.IsValid()) {
      hid_t fhandle = 0;
      if (m_comm.IsMaster())
        fhandle = H5Fopen(m_mesh_filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
      my_size_t local_begin, local_size;
      std::vector<my_size_t> bnode_offset;
      std::vector<id_size_t> bnodes(1000);
      m_comm.RankDistribution(num_nodes, local_begin, local_size);
      binfo.resize(local_size);
      GetDsetOffsetMaster(fhandle, C_HipBoundaryNodeOffsetDataSetName,
                          bnode_offset);
      m_comm.BroadcastMaster(&bnode_offset[0], bnode_offset.size());
      npatch = int(bnode_offset.size()) - 1;
      for (unsigned ipatch = 0; ipatch < bnode_offset.size() - 1; ++ipatch) {
        int num_bnodes =
            int(bnode_offset[ipatch + 1]) - int(bnode_offset[ipatch]);
        int num_batch = num_bnodes / 1000;
        int num_spill_over = num_bnodes - num_batch * 1000;
        int current_offset = bnode_offset[ipatch];
        if (m_comm.IsMaster())
          std::cout << "Message: Reading boundary node chunks "
                    << " of patch " << ipatch + 1 << "\n";
        for (int ibatch = 0; ibatch < num_batch + 1; ++ibatch) {
          int num_batch_nodes = (ibatch != num_batch) ? 1000 : num_spill_over;
          GetBnodesMaster(fhandle, num_batch_nodes, current_offset, bnodes);
          m_comm.BroadcastMaster(&bnodes[0], num_batch_nodes);
          for (int i = 0; i < num_batch_nodes; ++i)
            if (bnodes[i] >= local_begin + 1 &&
                bnodes[i] <= local_begin + local_size)
              binfo[bnodes[i] - local_begin - 1].push_back(ipatch + 1);
          current_offset += num_batch_nodes;
        } // Batch loop ends
      }   // Patch loop ends
      // close HDF5 file handle if open
      if (fhandle != 0)
        H5Fclose(fhandle);
    } // For valid communicator ranks
    return npatch;
  }
#endif

  /**
   * @brief Read the boundary nodes in the master rank given offset and chunk
   *        size
   *
   * @param num_bnodes number of boundary nodes to read
   * @param bnode_offset offset of boundary nodes in the file
   * @param bnodes boundary nodes data
   * @param write_debug write debug information if true
   */
  void GetBnodesMaster(hid_t &fhandle,
                       int num_batch_nodes,
                       int current_offset,
                       std::vector<id_size_t> &bnodes,
                       bool write_debug = false) {
    // Read the chunk in master and broadcast
    if (m_comm.IsMaster()) {
      hdf5::Hyperslab<hsize_t, 1> file_slab, memory_slab;

      file_slab.count[0] = num_batch_nodes;
      file_slab.stride[0] = 1;
      file_slab.offset[0] = current_offset;

      memory_slab.dims[0] = num_batch_nodes;
      memory_slab.count[0] = num_batch_nodes;
      memory_slab.stride[0] = 1;
      memory_slab.offset[0] = 0;

      hdf5::Read(fhandle,
                 C_HipBoundaryNodeDataSetName,
                 file_slab,
                 memory_slab,
                 &bnodes[0]);
      if (write_debug)
        std::cout << "Message: Reading boundary nodes size : "
                  << num_batch_nodes << " offset " << current_offset << "\n";
    }
  }

  /**
   * @brief Populates the data from the dictionary
   *
   * @param num_elms number of elements in the mesh
   * @param bfaces_info boundary faces information
   * @return int number of patches in the mesh
   */
  int PopulatePatchFaces(my_size_t num_elms, bface_t &bfaces_info) {
    // Convert the local boundary information into a global
    // assumed partitioning of boundary faces in bface_info
    if (m_comm.IsValid()) {
      my_size_t local_begin, local_size;
      // Maps the owner mpi rank to the list of boundary faces
      std::map<int, std::vector<id_size_t>> proc_bfaces;
      // Note that we have all the local boundary faces of all elements
      // from external solver (since it already had created a partition)
      m_comm.RankDistribution(num_elms, local_begin, local_size);
      // Obtain the element offset for assigning
      id_size_t local_num_elems = m_ntrielm + m_ntetelm;
      id_size_t elm_offset = 0;
      // Obtain the offsets in each processor to form the element keys
      MPI_Exscan(&local_num_elems,
                 &elm_offset,
                 1,
                 GetMpiType<id_size_t>(),
                 MPI_SUM,
                 m_comm.RawHandle());
      // Loop over local boundary faces and assign the data
      // to its respective owner rank in the map
      auto num_procs = m_comm.size();
      int64_t quotient = num_elms / num_procs;
      int64_t remainder = num_elms % num_procs;
      int64_t threshold = remainder * (quotient + 1);
      for (int ipatch = 0; ipatch < m_npatch; ++ipatch) {
        for (T_Int iface = 0; iface < m_nbfaces[ipatch]; ++iface) {
          int64_t global_elm_id = m_bface_elm[ipatch][iface] + elm_offset;
          // Identify the owner processor this element is attached
          auto i_proc = PROC_ID(global_elm_id, threshold, quotient, remainder);
          proc_bfaces[i_proc].push_back(global_elm_id); // Element id
          proc_bfaces[i_proc].push_back(ipatch + 1);    // patch id
          proc_bfaces[i_proc].push_back(m_bface_no[ipatch][iface]); // Face no
        }
      }
      // Copy the local data before infering the send/recv
      bfaces_info.resize(local_size);
      auto my_rank = m_comm.Rank();
      if (!proc_bfaces[my_rank].empty()) {
        for (id_size_t i = 0; i < id_size_t(proc_bfaces[my_rank].size() / 3);
             ++i) {
          bfaces_info[proc_bfaces[my_rank][3 * i] - local_begin - 1].push_back(
              BoundaryFace{
                  int(proc_bfaces[my_rank][3 * i + 1]), // patch id
                  int(proc_bfaces[my_rank][3 * i + 2])  // face no
              });
        }
        proc_bfaces.erase(my_rank);
      }
      // Infer the send/recv and add the neighbour data to my_proc_bfaces
      auto num_recv = m_comm.InferNumReceive(proc_bfaces);
      std::vector<MPI_Request> send_req(proc_bfaces.size());
      std::vector<MPI_Status> send_stat(proc_bfaces.size());
      MPI_Status recv_stat;
      // Non-blocking send of data
      int count = 0;
      for (const auto &item : proc_bfaces)
        MPI_Isend(item.second.data(),
                  item.second.size(),
                  GetMpiType<id_size_t>(),
                  item.first,
                  my_rank,
                  m_comm.RawHandle(),
                  &send_req[count++]);
      // Blocking receive of data
      for (int irecv = 0; irecv < num_recv; ++irecv) {
        int recv_size;
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, m_comm.RawHandle(), &recv_stat);
        MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
        std::vector<id_size_t> temp_buffer(recv_size);
        MPI_Recv(temp_buffer.data(),
                 recv_size,
                 GetMpiType<id_size_t>(),
                 recv_stat.MPI_SOURCE,
                 recv_stat.MPI_TAG,
                 m_comm.RawHandle(),
                 &recv_stat);
        for (decltype(recv_size) i = 0; i < recv_size / 3; ++i) {
          bfaces_info[temp_buffer[3 * i] - local_begin - 1].push_back(
              BoundaryFace{
                  int(temp_buffer[3 * i + 1]), // patch id
                  int(temp_buffer[3 * i + 2])  // face no
              });
        }
      }
      // Wait for the send to complete
      MPI_Waitall(proc_bfaces.size(), send_req.data(), send_stat.data());
    }
    return m_npatch;
  }

  /**
   * @brief Empty for now and not populated
   * @param npatches number of patches
   * @param str string data for patch labels
   */
  void ReadPatchLabels(const int npatches, std::vector<char> &str) {}

  /**
   * @brief Empty for now as periodic patch is unsupported in AVBP adapt
   * @tparam T (int, long, long long) depending on the size of the mesh
   * @param patch_pairs periodic patch pairs
   * @param num_patch_pairs number of periodic patch pairs
   */
  template <typename T>
  void GetPeriodicPatch(std::vector<T> &patch_pairs, T &num_patch_pairs) {}

  /**
   * @brief Empty for now as periodic patch is unsupported in AVBP adapt
   *
   * @tparam T (int, long, long long) depending on the size of the mesh
   * @param patch_nodes periodic patch nodes
   */
  template <typename T> void GetPeriodicNodes(std::vector<T> &patch_nodes) {}

  /**
   * @brief Read the boundary nodes offset information from HDF5 mesh in the
   *        master MPI rank
   *
   * @param bnode_offset boundary node offset information from HDF5
   */
  void GetDsetOffsetMaster(hid_t &fhandle,
                           const char *dset_name,
                           std::vector<my_size_t> &bnode_offset) {
    hsize_t file_dims[2];
    if (m_comm.IsMaster()) {
      // Read the number of boundary nodes from file
      // int rank = hdf5::GetDimensions(fhandle, dset_name, file_dims);
      // std::cout << "Rank of bnode-offset = " << rank
      //          << " and dim:0 = " << file_dims[0] << "\n";
    }
    m_comm.BroadcastMaster(file_dims, 1);
    bnode_offset.resize(file_dims[0] + 1);
    if (m_comm.IsMaster()) {
      // std::cout << "Message: Size of bnode offset = " << file_dims[0] + 1
      //          << "\n";
      hdf5::Hyperslab<hsize_t, 1> file_slab, memory_slab;

      file_slab.dims[0] = file_dims[0];
      file_slab.count[0] = file_dims[0];
      file_slab.stride[0] = 1;
      file_slab.offset[0] = 0;

      memory_slab.dims[0] = file_dims[0] + 1;
      memory_slab.count[0] = file_dims[0];
      memory_slab.stride[0] = 1;
      memory_slab.offset[0] = 1;

      hdf5::Read(fhandle, dset_name, file_slab, memory_slab, &bnode_offset[0]);
    }
  }

  /**
   * Member data
   */
  bool m_is2d = false;
  T_Int m_nnode = 0;
  T_Int m_mynode = 0;

  T_Int *m_nodelist = 0;
  double *m_node_x = nullptr;

  T_Int m_ntrielm = 0;
  T_Int *m_trielm = nullptr;

  T_Int m_ntetelm = 0;
  T_Int *m_tetelm = nullptr;

  T_Int m_npatch = 0;
  T_Int m_nbnodes = 0;
  T_Int *m_bnode_begin = nullptr;
  T_Int *m_bnode_size = nullptr;
  T_Int *m_bnode_to_node = nullptr;

  std::vector<T_Int> m_nbfaces;
  std::vector<T_Int *> m_bface_elm;
  std::vector<T_Int *> m_bface_no;

  std::string m_mesh_filename;
  T_Comm &m_comm;
  hid_t m_hfile = 0;
};
} // namespace taru
