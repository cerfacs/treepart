/**
 *  @file mesh_common.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Common data structures and functions for mesh operations
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */
#pragma once

#include "treepart_config.h"
#include <array>
#include <cmath>
#include <iostream>
#include <limits>
#include <map>
#include <set>
#include <tuple>
#include <vector>

namespace taru {

[[maybe_unused]] const int C_BiElemFaceIndex[3][2] = {
    {1, 2},
    {2, 0},
    {0, 1}}; //!< Hip Face convention
[[maybe_unused]] const int C_TriElemFaceIndex[4][3] = {
    {1, 3, 2},
    {0, 2, 3},
    {0, 3, 1},
    {0, 1, 2}}; //!< Hip Triangle element face convention

/**
 * @brief Returns a static array of arrays representing the nodes of the edges
 * of a tetrahedron.
 *
 * The array contains 6 sub-arrays, each representing an edge of the
 * tetrahedron. Each sub-array contains 2 integers, representing the indices of
 * the nodes that form the edge.
 *
 * @return A static array of arrays representing the nodes of the edges of a
 * tetrahedron.
 */
static const std::array<std::array<int, 2>, 6> TetEdgeNodes() {
  // List of hip edges (two nodes of the edge in local index)};
  return {{{0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}}};
}

/**
 * @brief Returns a list of hip edges in a triangle mesh.
 *
 * The function returns a 2D array where each row represents an edge in the
 * triangle mesh. Each edge is represented by two nodes, specified by their
 * local indices.
 *
 * @return A 2D array of hip edges in the triangle mesh.
 */
static const std::array<std::array<int, 2>, 3> TriEdgeNodes() {
  // List of hip edges (two nodes of the edge in local index)};
  return {{{0, 1}, {1, 2}, {2, 0}}};
}

/**
 * @brief Calculates the determinant of a 4x4 matrix.
 *
 * This function calculates the determinant of a 4x4 matrix using the input
 * array `src`. The matrix is represented as a flat array of size 16, where the
 * elements are stored in column-major order. The determinant is computed using
 * the formula:
 *
 * det = src[0] * dst[0] + src[1] * dst[1] + src[2] * dst[2] + src[3] * dst[3]
 *
 * where dst is the adjoint matrix of the input matrix.
 *
 * @tparam T The type of the elements in the matrix.
 * @param src The input matrix represented as a flat array of size 16.
 * @return The determinant of the matrix.
 */
template <typename T> static inline T det4x4(const T src[]) {
  T dst[4];
  /* Compute adjoint: */
  dst[0] = +src[5] * src[10] * src[15] - src[5] * src[11] * src[14] -
           src[9] * src[6] * src[15] + src[9] * src[7] * src[14] +
           src[13] * src[6] * src[11] - src[13] * src[7] * src[10];
  dst[1] = -src[4] * src[10] * src[15] + src[4] * src[11] * src[14] +
           src[8] * src[6] * src[15] - src[8] * src[7] * src[14] -
           src[12] * src[6] * src[11] + src[12] * src[7] * src[10];
  dst[2] = +src[4] * src[9] * src[15] - src[4] * src[11] * src[13] -
           src[8] * src[5] * src[15] + src[8] * src[7] * src[13] +
           src[12] * src[5] * src[11] - src[12] * src[7] * src[9];
  dst[3] = -src[4] * src[9] * src[14] + src[4] * src[10] * src[13] +
           src[8] * src[5] * src[14] - src[8] * src[6] * src[13] -
           src[12] * src[5] * src[10] + src[12] * src[6] * src[9];
  /* Compute determinant: */
  return (+src[0] * dst[0] + src[1] * dst[1] + src[2] * dst[2] +
          src[3] * dst[3]);
}

/**
 * @brief Calculates the determinant of a 3x3 matrix.
 *
 * @tparam T The type of the matrix elements.
 * @param src The input matrix in row-major order.
 *
 * [a11 a12 a13
 *  a21 a22 a23
 *  a31 a32 a33]
 *
 * @return The determinant of the matrix.
 */
template <typename T> static inline T det3x3(const T src[]) {
  T dst[3];
  /* Compute adjoint: */
  dst[0] = +src[4] * src[8] - src[5] * src[7];
  dst[1] = -src[3] * src[8] + src[5] * src[6];
  dst[2] = +src[3] * src[7] - src[4] * src[6];
  /* Compute determinant: */
  return (src[0] * dst[0] + src[1] * dst[1] + src[2] * dst[2]);
}

static const unsigned C_ElementNumNodes[] =
    {3, 4, 4, 5, 6, 8}; //!< Number of nodes forming an element

const auto C_BigInt = std::numeric_limits<int>::max();

const auto C_BigIntBy2 = std::numeric_limits<int>::max() / 2;

const unsigned C_NumElementEdgeOrder[] = {
    6,
    12,
    9,
    8}; //!< Constant storing the element edge order size for each element type

static const unsigned C_NumElementTypes =
    6; //!< Constant that gives the total number of supported element types

// static const char *C_XdmfElementTypeString[] = {
//     "Triangle", "Quadrilateral", "Tetrahedron", "Pyramid",
//     "Wedge",    "Hexahedron"}; //!< Constant names of XDMF element types

// static const char *C_XdmfCoordinateString[] = {
//     "x", "y", "z"}; //!< Coordinate name as strings in XDMF file

// static const std::string C_XdmfHeader(
//     "<?xml version=\"1.0\" ?>\n"
//     "<Xdmf Version=\"2.0\" xmlns:xi=\"http://www.w3.org/2001/XInclude\">\n"
//     "  <Domain>\n"); //!< XDMF header string

// static const std::string
//     C_XdmfFooter("  </Domain>\n"
//                  "</Xdmf>\n"); //!< XDMF closing footer string

// static const unsigned C_VtkElementTypes[] = {5,  9,  10,
//                                              14, 13, 12}; //!< VTK element
//                                              types

const unsigned C_NumElementFaces[C_NumElementTypes] = {3, 4, 4, 5, 5, 6}; //!<

const unsigned C_NumElementFaceNodes[C_NumElementTypes][6] = {
    {2, 2, 2, 0, 0, 0}, // Triangle
    {2, 2, 2, 2, 0, 0}, // Quad
    {3, 3, 3, 3, 0, 0}, // Tet
    {4, 3, 3, 3, 3, 0}, // Pyramid
    {4, 4, 4, 3, 3, 0}, // Prism
    {4, 4, 4, 4, 4, 4}  // Hex
}; //!<

const unsigned C_ElementFaceOrder[C_NumElementTypes][6][4] = {
    /**
     *  Triangle
     */
    {{2, 3, 0, 0},
     {3, 1, 0, 0},
     {1, 2, 0, 0},
     /**
      *  Empty after
      */
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0}},
    /**
     *  Quadrilateral
     */
    {{1, 2, 0, 0},
     {2, 3, 0, 0},
     {3, 4, 0, 0},
     {4, 1, 0, 0},
     /**
      *  Empty after
      */
     {0, 0, 0, 0},
     {0, 0, 0, 0}},
    /**
     *  Tetrahedron
     */
    {{2, 4, 3, 0},
     {1, 3, 4, 0},
     {1, 4, 2, 0},
     {1, 2, 3, 0},
     /**
      *  Empty after
      */
     {0, 0, 0, 0},
     {0, 0, 0, 0}},
    /**
     *  Pyramid
     */
    {{1, 4, 3, 2},
     {1, 2, 5, 0},
     {2, 3, 5, 0},
     {3, 4, 5, 0},
     {4, 1, 5, 0},
     /**
      *  Empty after
      */
     {0, 0, 0, 0}},
    /**
     *  Prismatic
     */
    {{3, 4, 6, 5},
     {1, 2, 5, 6},
     {1, 4, 3, 2},
     {4, 1, 6, 0},
     {2, 3, 5, 0},
     /**
      *  Empty after
      */
     {0, 0, 0, 0}},
    /**
     *  Hexahedron
     */
    {{1, 2, 6, 5},
     {6, 2, 3, 7},
     {8, 7, 3, 4},
     {1, 5, 8, 4},
     {1, 4, 3, 2},
     {5, 6, 7, 8}}}; //!<

const unsigned C_ElementEdgeOrder[C_NumElementTypes][12][2] = {
    /**
     * Triangle
     */
    {{1, 2},
     {2, 3},
     {3, 1},
     /**
      * Empty after
      */
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0}},
    // Quadrilateral
    {{1, 2},
     {2, 3},
     {3, 4},
     {4, 1},
     /**
      * Empty after
      */
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0}},
    // Tetra
    {{1, 2},
     {2, 3},
     {3, 1},
     {1, 4},
     {2, 4},
     {3, 4},
     /**
      * Empty after
      */
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0}},
    // Pyramid
    {{1, 2},
     {2, 3},
     {3, 4},
     {4, 1},
     {4, 5},
     {1, 5},
     {2, 5},
     {3, 5},
     /**
      * Empty after
      */
     {0, 0},
     {0, 0},
     {0, 0},
     {0, 0}},
    // Prism
    {{1, 2},
     {2, 3},
     {3, 1},
     {4, 5},
     {5, 6},
     {6, 4},
     {1, 4},
     {3, 6},
     {2, 5},
     /**
      * Empty after
      */
     {0, 0},
     {0, 0},
     {0, 0}},
    // Hexa
    {{1, 2},
     {2, 3},
     {3, 4},
     {4, 1},
     {5, 6},
     {6, 7},
     {7, 8},
     {8, 5},
     {4, 8},
     {1, 5},
     {2, 6},
     {3,
      7}}}; //!< Constant storing the element edge order for each element type

/**
 * @brief Enumerations to access element types in meshes
 *
 */
enum ElementIndex {
  FIRST_ELEMENT_ID = 0, //!< Access first element
  TRI_ID = 0,           //!< Triangle access
  QUA_ID = 1,           //!< Quadrilateral access
  TET_ID = 2,           //!< Tetrahedral access
  PYR_ID = 3,           //!< Pyramid access
  PRI_ID = 4,           //!< Prism access
  HEX_ID = 5,           //!< Hexahedron access
  LAST_ELEMENT_ID = 5   //!< Access last element
};

/**
 * @brief Enumerations to access X, Y, Z coordinates in an array of coordinates
 *
 */
enum CoordinateIndex {
  X_ID = 0, //!< X coordinate access
  Y_ID = 1, //!< Y coordinate access
  Z_ID = 2  //!< Z coordinate access
};

/**
 * @brief 3D point coordinate data structure
 *
 * 3D coordinates are stored in this AoS format for the dictionaries
 * this enables the use of a POD type to infer sizes but fails if
 * padding is added to this struct i.e., sizeof(point3d_t) != sizeof(double) * 3
 */
struct Point3d {
  double x; //!< X coordinate
  double y; //!< Y coordinate
  double z; //!< Z coordinate

  /**
   * @brief
   *
   * @param out
   * @param p
   * @return std::ostream&
   */
  friend std::ostream &operator<<(std::ostream &out, const Point3d &p);

  /**
   * @brief Overloaded equality operator for comparing two Point3d objects.
   *
   * @param rhs The Point3d object to compare with.
   * @return true if the two Point3d objects are equal within a tolerance
   * of 1.0e-8, false otherwise.
   */
  bool operator==(const Point3d &rhs) {
    // return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
    return ((std::abs(x - rhs.x) <= 1.0e-8) &&
            (std::abs(y - rhs.y) <= 1.0e-8) && (std::abs(z - rhs.z) <= 1.0e-8));
  }

  /**
   * @brief Calculates the L2 distance between two 3D points.
   *
   * This function calculates the Euclidean distance between two 3D points using
   * the L2 norm.
   *
   * @param a The first 3D point.
   * @param b The second 3D point.
   * @return The L2 distance between the two points.
   */
  static double distance_l2(const Point3d &a, const Point3d &b) {
    Point3d dx = a;
    dx -= b;
    return norm2(dx);
  }

  /**
   * @brief Calculates the squared distance between two 3D points.
   *
   * This function calculates the squared distance between two 3D points using
   * the Euclidean distance formula. The squared distance is returned as a
   * double value.
   *
   * @param a The first 3D point.
   * @param b The second 3D point.
   * @return The squared distance between the two points.
   */
  static double distance_sq(const Point3d &a, const Point3d &b) {
    Point3d dx = a;
    dx -= b;
    return normSq(dx);
  }

  /**
   * @brief Subtract a Point3d from the current Point3d.
   *
   * This operator subtracts the coordinates of the given Point3d from the
   * current Point3d.
   *
   * @param rhs The Point3d to subtract.
   */
  void operator-=(const Point3d &rhs) {
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
  }

  /**
   * @brief Overloaded subtraction operator for Point3d objects.
   *
   * This operator subtracts the coordinates of the given Point3d object from
   * the current Point3d object.
   *
   * @param p The Point3d object to subtract.
   * @return The resulting Point3d object after subtraction.
   */
  Point3d operator-(const Point3d p) {
    Point3d temp;
    temp.x = x - p.x;
    temp.y = y - p.y;
    temp.z = z - p.z;
    return temp;
  }

  /**
   * @brief Calculates the squared Euclidean norm of a 3D point.
   *
   * @param p The 3D point.
   * @return The squared Euclidean norm of the point.
   */
  static double norm2(const Point3d &p) {
    return std::sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
  }

  /**
   * @brief Calculates the square of the Euclidean norm of a 3D point.
   *
   * @param p The 3D point for which to calculate the norm.
   * @return The square of the Euclidean norm of the point.
   */
  static double normSq(const Point3d &p) {
    return p.x * p.x + p.y * p.y + p.z * p.z;
  }
};

/**
 * @brief Data structure to hold node id of an element
 *
 * An array of size 8 encapsulated into a structure
 * to store the node ids forming an element
 */
struct ElementConnectivity {
  id_size_t
      nodes[8]; //!< Eight nodes of the element (for non-hex pad with zeros)

  /**
   * @brief
   *
   * @param out
   * @param e
   * @return std::ostream&
   */
  friend std::ostream &operator<<(std::ostream &out,
                                  const ElementConnectivity &e);

  /**
   * @brief
   *
   * @param rhs
   * @return true
   * @return false
   */
  bool operator==(const ElementConnectivity &rhs) {
    return (nodes[0] == rhs.nodes[0]) && (nodes[1] == rhs.nodes[1]) &&
           (nodes[2] == rhs.nodes[2]) && (nodes[3] == rhs.nodes[3]) &&
           (nodes[4] == rhs.nodes[4]) && (nodes[5] == rhs.nodes[5]) &&
           (nodes[6] == rhs.nodes[6]) && (nodes[7] == rhs.nodes[7]);
  }
};

/**
 * @brief Overloaded stream insertion operator for Point3d objects.
 *
 * This function allows the Point3d object to be printed to an output stream in
 * the format (x, y, z).
 *
 * @param out The output stream to write to.
 * @param p The Point3d object to be printed.
 * @return The output stream after writing the Point3d object.
 */
inline std::ostream &operator<<(std::ostream &out, const Point3d &p) {
  out << "(" << p.x << ", " << p.y << ", " << p.z << ")";
  return out;
}

/**
 * @brief Overloaded stream insertion operator for ElementConnectivity.
 *
 * This function allows the ElementConnectivity object to be printed to an
 * output stream. It prints the nodes of the ElementConnectivity object in a
 * formatted manner.
 *
 * @param out The output stream to write to.
 * @param e The ElementConnectivity object to be printed.
 * @return The output stream after writing the ElementConnectivity object.
 */
inline std::ostream &operator<<(std::ostream &out,
                                const ElementConnectivity &e) {
  out << "(";
  for (const auto &item : e.nodes)
    out << item << " ";
  out << ")";
  return out;
}

/** Boundary info type **/
typedef std::vector<std::vector<int>> binfo_t;

typedef std::pair<id_size_t, id_size_t> id_tuple2_t;

typedef std::tuple<id_size_t, id_size_t, id_size_t> id_tuple3_t;

typedef std::tuple<id_size_t, id_size_t, id_size_t> id_tuple4_t;

/**
 * @brief Boundary Face information for an element
 *
 */
struct BoundaryFace {
  int patch_id;
  int face_id;
};

/** Repeating Boundary Face Map for Elements **/
typedef std::vector<std::vector<BoundaryFace>> bface_t;

/**
 * @brief Converts the global node indices in the element buffer to local node
 * indices.
 *
 * This function takes a vector of ElementConnectivity objects and a vector of
 * node keys. It converts the global node indices in the element buffer to local
 * node indices based on the node keys. The node keys are updated to contain
 * only the unique local node indices.
 *
 * @param element_buffer The vector of ElementConnectivity objects representing
 * the element buffer.
 * @param node_keys The vector of node keys representing the global node
 * indices.
 * @param invert_connectivity Flag indicating whether to invert the connectivity
 * by mapping the global node indices to local node indices.
 */
static void
ConvertToLocalIndex(std::vector<ElementConnectivity> &element_buffer,
                    std::vector<id_size_t> &node_keys,
                    const bool invert_connectivity = true) {
  std::map<id_size_t, id_size_t> node_key_map;
  {
    std::set<id_size_t> node_keys_set;
    for (auto &element_items : element_buffer)
      for (auto &node_index : element_items.nodes)
        if (node_index > 0)
          node_keys_set.insert(node_index);
    node_keys.clear();
    std::copy(node_keys_set.begin(),
              node_keys_set.end(),
              std::back_inserter(node_keys));
    id_size_t counter = 0;
    for (auto &item : node_keys_set)
      node_key_map[item] = ++counter;
  }
  if (invert_connectivity) {
    // Inverse Map nodes
    for (auto &element_items : element_buffer)
      for (auto &node_index : element_items.nodes)
        if (node_index > 0)
          node_index = node_key_map[node_index];
  }
}

/**
 * @brief InplacePermutation performs an in-place permutation of the given data
 * arrays based on the provided permutation array.
 *
 * This function rearranges the elements in the data arrays (data1, data2,
 * data3, data4) according to the permutation array (perm). The size parameter
 * specifies the number of elements in the arrays.
 *
 * @tparam T1 The type of elements in data1 array.
 * @tparam T2 The type of elements in data2 array.
 * @tparam T3 The type of elements in data3 array.
 * @tparam T4 The type of elements in data4 array.
 * @tparam uintT The type of elements in the permutation array.
 *
 * @param size The number of elements in the arrays.
 * @param perm The permutation array specifying the new order of elements.
 * @param data1 The array to be permuted.
 * @param data2 The optional array to be permuted.
 * @param data3 The optional array to be permuted.
 * @param data4 The optional array to be permuted.
 *
 * @note The permutation array (perm) should contain values in the range [0,
 * size-1].
 *
 * @note The function modifies the input arrays in-place.
 */
template <typename T1, typename T2, typename T3, typename T4, typename uintT>
void InplacePermutation(const std::size_t size,
                        uintT *perm,
                        T1 *data1,
                        T2 *data2 = nullptr,
                        T3 *data3 = nullptr,
                        T4 *data4 = nullptr) {
  T1 temp1;
  T2 temp2;
  T3 temp3;
  T4 temp4;
  uintT j, k;
  for (uintT i = 0; i < uintT(size); ++i) {
    if (i != perm[i]) {
      temp1 = data1[i];
      if (data2)
        temp2 = data2[i];
      if (data3)
        temp3 = data3[i];
      if (data4)
        temp4 = data4[i];
      j = i;
      while (i != perm[j]) {
        k = perm[j];
        data1[j] = data1[k];
        if (data2)
          data2[j] = data2[k];
        if (data3)
          data3[j] = data3[k];
        if (data4)
          data4[j] = data4[k];
        perm[j] = j;
        j = k;
      }
      data1[j] = temp1;
      if (data2)
        data2[j] = temp2;
      if (data3)
        data3[j] = temp3;
      if (data4)
        data4[j] = temp4;
      perm[j] = j;
    }
  }
}

/**
 * @brief Calculates the element offsets and global positions based on the given
 * global offsets by element type
 *
 * This function takes the global offsets, my begin and end indices, and
 * calculates the local offsets and global positions for each element type. The
 * element sizes are determined using the global offsets information.
 *
 * @tparam T_Int The type of the integer used for offsets and positions.
 * @param global_offsets The array of global offsets for each element type.
 * @param my_begin The starting index for the local range.
 * @param my_end The ending index for the local range.
 * @param local_offsets The array to store the local offsets for each element
 * type.
 * @param global_position The array to store the global positions for each
 * element type.
 */
template <typename T_Int>
static void GetElementOffsets(const T_Int global_offsets[C_NumElementTypes + 1],
                              const T_Int my_begin,
                              const T_Int my_end,
                              T_Int local_offsets[C_NumElementTypes + 1],
                              T_Int global_position[C_NumElementTypes]) {
  bool set_global_pos[C_NumElementTypes] = {false};
  /* Nullify the entries in local_offsets */
  std::fill(local_offsets, local_offsets + C_NumElementTypes + 1, 0);
  std::fill(global_position, global_position + C_NumElementTypes, 0);
  /* Get element sizes using global local_offsets info */
  for (auto my_key = my_begin; my_key < my_end; ++my_key) {
    for (unsigned element_type = 0; element_type < C_NumElementTypes;
         ++element_type) {
      if (my_key >= global_offsets[element_type] &&
          my_key < global_offsets[element_type + 1]) {
        if (!set_global_pos[element_type]) {
          global_position[element_type] = my_key;
          set_global_pos[element_type] = true;
        }
        local_offsets[element_type + 1]++;
      }
    }
  }
  /* Form local_offsets table */
  for (T_Int i = 0; i < C_NumElementTypes; ++i) {
    local_offsets[i + 1] += local_offsets[i];
    if (global_position[i] > 0)
      global_position[i] -= global_offsets[i];
  }
}

/**
 * @brief Removes shared information from the given vector of sets.
 *
 * This function iterates over each set in the vector and removes any negative
 * values from it. Negative values are considered as shared information and are
 * removed from the set.
 *
 * @param adapt_binfo The vector of sets from which shared information needs to
 * be removed.
 */
inline void RemoveSharedInfo(std::vector<std::set<int>> &adapt_binfo) {
  for (auto &item : adapt_binfo) {
    auto shared_node_it = item.begin();
    while (shared_node_it != std::end(item)) {
      if (*shared_node_it < 0)
        shared_node_it = item.erase(shared_node_it);
      else
        ++shared_node_it;
    }
  }
}

/**
 * @brief Get the local edge list for tetrahedral elements.
 *
 * This function takes the number of nodes, element connectivity information,
 * and an empty vector to store the edge list. It forms a unique list of edges
 * from the element connectivity information and stores it in the provided
 * vector.
 *
 * @tparam T_Int The type of the indices used for the edge list.
 * @param num_nodes The number of nodes in the mesh.
 * @param elem3d The element connectivity information.
 * @param edge_list The vector to store the edge list.
 */
template <typename T_Int>
static void
GetLocalEdgeListTetra(const id_size_t num_nodes,
                      const std::vector<ElementConnectivity> &elem3d,
                      std::vector<std::pair<T_Int, T_Int>> &edge_list) {
  // First form the uniqe list of edges
  std::set<std::pair<T_Int, T_Int>> edge_set;
  const auto tet_nodes = TetEdgeNodes();
  for (const auto &my_elem : elem3d) {
    const auto &elem_nodes = my_elem.nodes;
    // Loop over the edge list of the tetra
    for (auto &edge_node : tet_nodes) {
      // Note using C indexing
      auto i_left =
          std::min(elem_nodes[edge_node[0]], elem_nodes[edge_node[1]]);
      auto i_right =
          std::max(elem_nodes[edge_node[0]], elem_nodes[edge_node[1]]);
      edge_set.insert(std::make_pair(i_left, i_right));
    }
  }
  std::copy(edge_set.begin(), edge_set.end(), std::back_inserter(edge_list));
  edge_set.clear();
}

/**
 * @brief Generates a vertex-edge graph based on the given edge list.
 *
 * This function takes a number of nodes, an edge list, and a vector to store
 * the vertex-edge graph. It clears the vector, resizes it to accommodate the
 * given number of nodes, and populates it with the edges based on the given
 * edge list. Each vertex in the graph is associated with the indices of the
 * edges it is connected to.
 *
 * @tparam T_Int The type of the indices.
 * @tparam T_Int1 The type of the elements in the edge list.
 * @param num_nodes The number of nodes in the graph.
 * @param edge_list The list of edges, represented as pairs of indices.
 * @param vertex_edges The vector to store the vertex-edge graph.
 */
template <typename T_Int, typename T_Int1>
static void
GetVertexEdgeGraph(const id_size_t num_nodes,
                   const std::vector<std::pair<T_Int1, T_Int1>> &edge_list,
                   std::vector<std::vector<T_Int>> &vertex_edges) {
  vertex_edges.clear();
  vertex_edges.shrink_to_fit();
  vertex_edges.resize(num_nodes);
  for (T_Int i = 0; i < T_Int(edge_list.size()); ++i) {
    const auto &iedge = edge_list[i].first;
    const auto &jedge = edge_list[i].second;
    vertex_edges[iedge].push_back(i);
    vertex_edges[jedge].push_back(i);
  }
}

/**
 * @brief Get the local edge list for a triangular mesh.
 *
 * This function takes the number of nodes, element connectivity, and an empty
 * edge list as input. It forms a unique list of edges from the given element
 * connectivity and stores it in the edge list.
 *
 * @tparam T_Int The type of the indices.
 * @param num_nodes The number of nodes in the mesh.
 * @param elem3d The element connectivity of the mesh.
 * @param edge_list The output edge list.
 */
template <typename T_Int>
static void
GetLocalEdgeListTria(const id_size_t num_nodes,
                     const std::vector<ElementConnectivity> &elem3d,
                     std::vector<std::pair<T_Int, T_Int>> &edge_list) {
  // First form the uniqe list of edges
  std::set<std::pair<T_Int, T_Int>> edge_set;
  const auto tri_nodes = TriEdgeNodes();
  for (const auto &my_elem : elem3d) {
    const auto &elem_nodes = my_elem.nodes;
    // Loop over the edge list of the triangle
    for (auto &edge_node : tri_nodes) {
      // Note using C indexing
      auto i_left =
          std::min(elem_nodes[edge_node[0]], elem_nodes[edge_node[1]]);
      auto i_right =
          std::max(elem_nodes[edge_node[0]], elem_nodes[edge_node[1]]);
      edge_set.insert(std::make_pair(i_left, i_right));
    }
  }
  std::copy(edge_set.begin(), edge_set.end(), std::back_inserter(edge_list));
  edge_set.clear();
}

/**
 * @brief Renumbers the nodes in the edge list based on the given node map.
 *
 * This function takes a node map and an edge list and renumbers the nodes in
 * the edge list based on the mapping provided in the node map. If both nodes of
 * an edge are found in the node map, the function updates the edge with the
 * renumbered node IDs. If either of the nodes is not found in the node map, the
 * function throws a std::runtime_error.
 *
 * @tparam T_Int The type of the node IDs in the edge list.
 * @param nmap The node map containing the mapping of old node IDs to new node
 * IDs.
 * @param edge_list The edge list to be renumbered.
 * @throws std::runtime_error if either of the nodes in an edge is not found in
 * the node map.
 */
template <typename T_Int>
static void RenumberEdgeList(const std::map<id_size_t, id_size_t> &nmap,
                             std::vector<std::pair<T_Int, T_Int>> &edge_list) {
  for (auto &edge : edge_list) {
    auto left_found = nmap.find(edge.first);
    auto right_found = nmap.find(edge.second);
    if (left_found != std::end(nmap) && right_found != std::end(nmap)) {
      edge.first = left_found->second;
      edge.second = right_found->second;
    } else
      throw std::runtime_error("Unable to find edge node in local map");
  }
}

/**
 * @brief Get the element type based on the number of element nodes and
 * dimensionality (linear elements).
 *
 * This function determines the element type based on the number of element
 * nodes and dimensionality. It returns an integer value representing the
 * element type.
 *
 * @tparam T_Int The type of the number of element nodes.
 * @param n_elm_node The number of element nodes.
 * @param is_2d A boolean value indicating whether the mesh is 2D or not.
 * @return An integer value representing the element type.
 *         -1: Unassigned or Unknown
 *          0: Triangle
 *          1: Quad (2D) or Tet (3D)
 *          2: Quad (2D)
 *          3: Pyramid (3D)
 *          4: Prism (3D)
 *          5: Hexa (3D)
 */
template <typename T_Int>
int GetElementType(const T_Int n_elm_node, const bool is_2d) {
  int elm_type = -1; // Unassigned or Unknown

  switch (n_elm_node) {
  case 3: // Triangle
    elm_type = 0;
    break;

  case 4: // Quad (2D) or Tet (3D)
    if (!is_2d)
      elm_type = 2;
    else
      elm_type = 1;
    break;

  case 5: // Pyramid (3D)
    elm_type = 3;
    break;

  case 6: // Prism (3D)
    elm_type = 4;
    break;

  case 8: // Hexa (3D)
    elm_type = 5;
    break;
  }
  return elm_type;
}

/**
 * @brief Calculates the number of edges at each node in a graph.
 *
 * This function takes a list of edges and calculates the number of edges
 * connected to each node in the graph. The result is stored in the
 * `num_edges_at_node` vector.
 *
 * @tparam T_Int The type of the node indices.
 * @param num_nodes The total number of nodes in the graph.
 * @param edge_list The list of edges in the graph.
 * @param num_edges_at_node The vector to store the number of edges at each
 * node.
 */
template <typename T_Int>
static void
GetNumEdgesAtNode(const id_size_t num_nodes,
                  const std::vector<std::pair<T_Int, T_Int>> &edge_list,
                  std::vector<int> &num_edges_at_node) {
  num_edges_at_node.clear();
  num_edges_at_node.shrink_to_fit();
  num_edges_at_node.resize(num_nodes);
  for (const auto &edge : edge_list) {
    num_edges_at_node[edge.first] += 1;
    num_edges_at_node[edge.second] += 1;
  }
}

/**
 * @brief Get the Average Edge Size object
 *
 * @tparam T_Real
 * @param num_nodes
 * @param xyz
 * @param edge_list
 * @param num_edges_at_node
 * @param avg_edge_size
 */
template <typename T_Real, typename T_Int>
static void
GetAverageEdgeSize(const id_size_t num_nodes,
                   const std::vector<Point3d> &xyz,
                   const std::vector<std::pair<T_Int, T_Int>> &edge_list,
                   const std::vector<int> &num_edges_at_node,
                   std::vector<T_Real> &avg_edge_size) {
  avg_edge_size.clear();
  avg_edge_size.shrink_to_fit();
  avg_edge_size.resize(num_nodes);
  // // Do the sum of edge distance at a node
  for (const auto &edge : edge_list) {
    auto edist = Point3d::distance_l2(xyz[edge.first], xyz[edge.second]);
    // Add to both left and right nodes
    avg_edge_size[edge.first] += edist;
    avg_edge_size[edge.second] += edist;
  }

  for (const auto &item : num_edges_at_node)
    if (item == 0)
      throw std::runtime_error("Node has no edges");

  // Do the average (divide by total edges at a node)
  for (std::size_t i = 0; i < xyz.size(); ++i)
    avg_edge_size[i] /= num_edges_at_node[i];
}

/**
 * @brief
 *
 * @tparam T
 * @param p1
 * @param p2
 * @param p3
 * @return
 */
inline double
TriangleArea(const Point3d &p1, const Point3d &p2, const Point3d &p3) {
  const double matrix[9] = {p1.x, p2.x, p3.x, p1.y, p2.y, p3.y, 1.0, 1.0, 1.0};
  return 0.5 * det3x3(matrix);
}

/**
 * @brief Calculates the volume of a tetrahedron defined by four points.
 *
 * This function calculates the volume of a tetrahedron using the given four
 * points. The points are represented by the Point3d structure, which contains
 * the x, y, and z coordinates.
 *
 * @param p1 The first point of the tetrahedron.
 * @param p2 The second point of the tetrahedron.
 * @param p3 The third point of the tetrahedron.
 * @param p4 The fourth point of the tetrahedron.
 * @return The volume of the tetrahedron.
 */
inline double TetraVolume(const Point3d &p1,
                          const Point3d &p2,
                          const Point3d &p3,
                          const Point3d &p4) {
  const double matrix[16] = {p1.x,
                             p2.x,
                             p3.x,
                             p4.x,
                             p1.y,
                             p2.y,
                             p3.y,
                             p4.y,
                             p1.z,
                             p2.z,
                             p3.z,
                             p4.z,
                             1.0,
                             1.0,
                             1.0,
                             1.0};
  return 1.0 / 6.0 * det4x4(matrix);
}

/**
 * @brief Calculates the circumradius of a tetrahedron.
 *
 * This function calculates the circumradius of a tetrahedron given its four
 * vertices. The circumradius is the radius of the circumsphere that passes
 * through all four vertices.
 *
 * @param v0 The first vertex of the tetrahedron.
 * @param v1 The second vertex of the tetrahedron.
 * @param v2 The third vertex of the tetrahedron.
 * @param v3 The fourth vertex of the tetrahedron.
 * @return The circumradius of the tetrahedron.
 */
inline double CircumRadiusTetra(const Point3d &v0,
                                const Point3d &v1,
                                const Point3d &v2,
                                const Point3d &v3) {
  // Create the rows of our "unrolled" 3x3 matrix
  auto Row1 = const_cast<Point3d &>(v1) - v0;
  auto sqLength1 = Point3d::normSq(Row1);
  auto Row2 = const_cast<Point3d &>(v2) - v0;
  auto sqLength2 = Point3d::normSq(Row2);
  auto Row3 = const_cast<Point3d &>(v3) - v0;
  auto sqLength3 = Point3d::normSq(Row3);

  // Compute the determinant of said matrix
  auto determinant = Row1.x * (Row2.y * Row3.z - Row3.y * Row2.z) -
                     Row2.x * (Row1.y * Row3.z - Row3.y * Row1.z) +
                     Row3.x * (Row1.y * Row2.z - Row2.y * Row1.z);

  // Compute the volume of the tetrahedron, and precompute a scalar quantity for
  // re-use in the formula
  const auto volume = determinant / 6.0;
  const auto iTwelveVolume = 1.0 / (volume * 12.0);

  Point3d delta{
      iTwelveVolume * ((Row2.y * Row3.z - Row3.y * Row2.z) * sqLength1 -
                       (Row1.y * Row3.z - Row3.y * Row1.z) * sqLength2 +
                       (Row1.y * Row2.z - Row2.y * Row1.z) * sqLength3),
      iTwelveVolume * (-(Row2.x * Row3.z - Row3.x * Row2.z) * sqLength1 +
                       (Row1.x * Row3.z - Row3.x * Row1.z) * sqLength2 -
                       (Row1.x * Row2.z - Row2.x * Row1.z) * sqLength3),
      iTwelveVolume * ((Row2.x * Row3.y - Row3.x * Row2.y) * sqLength1 -
                       (Row1.x * Row3.y - Row3.x * Row1.y) * sqLength2 +
                       (Row1.x * Row2.y - Row2.x * Row1.y) * sqLength3)};

  // Once we know the delta from center its norms is the distance (radius)
  // and the center of the circumsphere is v0 + delta
  return Point3d::norm2(delta);
}

/**
 * @brief Calculates the area of a regular triangle.
 *
 * This function calculates the area of a regular triangle given the coordinates
 * of its three vertices. The formula used to calculate the area is based on
 * Heron's formula and the circumcircle radius.
 *
 * @param p1 The first vertex of the triangle.
 * @param p2 The second vertex of the triangle.
 * @param p3 The third vertex of the triangle.
 * @return The area of the regular triangle.
 */
inline double
AreaRegularTriangle(const Point3d &p1, const Point3d &p2, const Point3d &p3) {
  // obtain the lengths of the three sides of the triangle
  auto a = Point3d::distance_l2(p1, p2);
  auto b = Point3d::distance_l2(p2, p3);
  auto c = Point3d::distance_l2(p3, p1);
  auto numer = a * b * c;
  // s is the semi-perimeter of the traingle
  auto s = 0.5 * (a + b + c);
  // Herons formula using the semi-perimeter
  auto deno = 4 * std::sqrt(s * (s - a) * (s - b) * (s - c));
  // Get the circum-circle radius
  auto radius = numer / deno;
  // Side of equilateral triangle is \sqrt{3} radius
  // of the circumcircle
  auto side = std::sqrt(3.0) * radius;
  // Area of the equilateral triangle given side
  return std::sqrt(3.0) / 4.0 * side * side;
}

/**
 * @brief Calculates the volume of a regular tetrahedron given its four
 * vertices.
 *
 * The volume of a regular tetrahedron is calculated using the formula:
 * V = (sqrt(2) / 12) * s^3
 * where V is the volume and s is the side length of the tetrahedron.
 *
 * @param p1 The first vertex of the tetrahedron.
 * @param p2 The second vertex of the tetrahedron.
 * @param p3 The third vertex of the tetrahedron.
 * @param p4 The fourth vertex of the tetrahedron.
 * @return The volume of the regular tetrahedron.
 */
inline double VolumeRegularTetra(const Point3d &p1,
                                 const Point3d &p2,
                                 const Point3d &p3,
                                 const Point3d &p4) {
  auto radius = CircumRadiusTetra(p1, p2, p3, p4);
  // For a regular tetrahedron circumsphere
  // radius is \frac{ \sqrt{6} }{4} * side
  auto side = 4.0 * radius / std::sqrt(6.0);
  // Volume of regular tetrahedron given side (s)
  // is \frac{\sqrt(2)}{12} s^3
  return std::sqrt(2.0) / 12.0 * side * side * side;
}

/**
 * @brief Calculates the equilateral skewness in 2D for each cell in a mesh.
 *
 * This function calculates the equilateral skewness for each cell in a 2D mesh.
 * The equilateral skewness is a measure of the distortion of a cell from an
 * ideal equilateral shape. It is calculated as the difference between the area
 * of the regular equilateral triangle formed by the cell's nodes and the actual
 * area of the cell, divided by the actual area of the cell.
 *
 * @tparam T The type of the equilateral skewness values.
 * @param coord The coordinates of the mesh nodes.
 * @param cells The connectivity information of the mesh elements.
 * @param eq_skew The vector to store the equilateral skewness values for each
 * cell.
 */
template <typename T>
void GetEquilateralSkewness2D(const std::vector<Point3d> &coord,
                              const std::vector<ElementConnectivity> &cells,
                              std::vector<T> &eq_skew) {
  for (my_size_t icell = 0; icell < cells.size(); ++icell) {
    // Count the total number of nodes in cell
    unsigned num_nodes = 0;
    T area_regular = 1.0, area = 1.0;
    for (const auto &node_id : cells[icell].nodes)
      if (node_id > 0)
        num_nodes++;
    const auto &p1 = coord[cells[icell].nodes[0]];
    const auto &p2 = coord[cells[icell].nodes[1]];
    const auto &p3 = coord[cells[icell].nodes[2]];
    switch (num_nodes) {
    case 3:
      area_regular = AreaRegularTriangle(p1, p2, p3);
      area = TriangleArea(p1, p2, p3);
      break;
    default:
      std::cout << "Message : Not implemented\n";
      break;
    }
    eq_skew[icell] = (area_regular - area) / area;
  }
}

/**
 * @brief Calculates the equilateral skewness for 3D mesh elements.
 *
 * This function calculates the equilateral skewness for each 3D mesh element
 * based on its coordinates and connectivity information. The equilateral
 * skewness is a measure of how close the shape of the element is to an
 * equilateral shape. skewness = (vol_reg_tet-volume)/vol_reg_tet
 *
 * @tparam T The type of the equilateral skewness values.
 * @param coord The vector of 3D coordinates of the mesh nodes.
 * @param cells The vector of element connectivity information.
 * @param eq_skew The vector to store the calculated equilateral skewness
 * values.
 */
template <typename T>
void GetEquilateralSkewness3D(const std::vector<Point3d> &coord,
                              const std::vector<ElementConnectivity> &cells,
                              std::vector<T> &eq_skew) {
  for (my_size_t icell = 0; icell < cells.size(); ++icell) {
    // Count the total number of nodes in cell
    unsigned num_nodes = 0;
    T vol_regular = 1.0, vol = 1.0;
    for (const auto &node_id : cells[icell].nodes)
      if (node_id > 0)
        num_nodes++;
    const auto &p1 = coord[cells[icell].nodes[0]];
    const auto &p2 = coord[cells[icell].nodes[1]];
    const auto &p3 = coord[cells[icell].nodes[2]];
    const auto &p4 = coord[cells[icell].nodes[3]];
    switch (num_nodes) {
    case 4:
      vol_regular = VolumeRegularTetra(p1, p2, p3, p4);
      vol = TetraVolume(p1, p2, p3, p4);
      break;

    default:
      std::cout << "Message : Not implemented\n";
      break;
    }
    eq_skew[icell] = (vol_regular - vol) / vol;
  }
}

/**
 * @brief Extracts the edge neighbours of a given vertex.
 *
 * This function takes a vertex ID, an edge list, a vertex-edge mapping, and an
 * empty vector to store the edge neighbours. It iterates over the edges
 * connected to the given vertex and adds the neighboring vertices to a set to
 * avoid duplicates. Finally, it copies the unique neighboring vertices from the
 * set to the output vector.
 *
 * @param idonor The ID of the vertex for which edge neighbours need to be
 * extracted.
 * @param edge_list The list of edges in the graph.
 * @param vertex_edges The mapping of vertices to the edges they are connected
 * to.
 * @param ng_list The vector to store the extracted edge neighbours.
 */
inline void ExtractEdgeNeighbours(
    const id_size_t idonor,
    const std::vector<std::pair<id_size_t, id_size_t>> &edge_list,
    const std::vector<std::vector<int>> &vertex_edges,
    std::vector<id_size_t> &ng_list) {
  // First level of neighbours
  std::set<id_size_t> ng_set;
  for (const auto &iedge : vertex_edges[idonor]) {
    ng_set.insert(edge_list[iedge].first);
    ng_set.insert(edge_list[iedge].second);
  }
  std::copy(ng_set.begin(), ng_set.end(), std::back_inserter(ng_list));
}

} // namespace taru
