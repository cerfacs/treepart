/**
 *  @file mesh_reader_hip.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Documentation for mesh_reader_hip.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "distributed_hash.hpp"
#include "mesh_common.hpp"
#include <utility>

namespace taru {

[[maybe_unused]] static const char *C_HipCoordinateDataSetNames[] = {
    "/Coordinates/x",
    "/Coordinates/y",
    "/Coordinates/z"}; //!< Hip HDF5 data set names for coordinates

[[maybe_unused]] static const char *C_HipElementDataSetNames[] = {
    "/Connectivity/tri->node",
    "/Connectivity/qua->node",
    "/Connectivity/tet->node",
    "/Connectivity/pyr->node",
    "/Connectivity/pri->node",
    "/Connectivity/hex->node"}; //!< Hip HDF5 data set names for element node
                                //!< connectivity
/**
 * @brief
 *
 */
[[maybe_unused]] static const char *C_HipBoundaryNodeDataSetName =
    "/Boundary/bnode->node"; //!<
[[maybe_unused]] static const char *C_HipBoundaryNodeOffsetDataSetName =
    "/Boundary/bnode_lidx"; //!<

/**
 * @brief Boundary line segment (two node edge)
 */
[[maybe_unused]] static const char *C_HipBoundaryBiDataSetName =
    "/Boundary/bnd_bi"; //!<
[[maybe_unused]] static const char *C_HipBoundaryBiElemOffsetDataSetName =
    "/Boundary/bnd_bi_lidx"; //!<
[[maybe_unused]] static const char *C_HipBoundaryBiFaceDataSetName =
    "/Boundary/bnd_bi->face"; //!<
[[maybe_unused]] static const char *C_HipBoundaryBiElemDataSetName =
    "/Boundary/bnd_bi->elem"; //!<
[[maybe_unused]] static const char *C_HipBoundaryBiNodeDataSetName =
    "/Boundary/bnd_bi->node"; //!<

/**
 * @brief Boundary triangle patch (three node element)
 *
 */
[[maybe_unused]] static const char *C_HipBoundaryTriDataSetName =
    "/Boundary/bnd_tri"; //!<
[[maybe_unused]] static const char *C_HipBoundaryTriElemOffsetDataSetName =
    "/Boundary/bnd_tri_lidx"; //!<
[[maybe_unused]] static const char *C_HipBoundaryTriFaceDataSetName =
    "/Boundary/bnd_tri->face"; //!<
[[maybe_unused]] static const char *C_HipBoundaryTriElemDataSetName =
    "/Boundary/bnd_tri->elem"; //!<
[[maybe_unused]] static const char *C_HipBoundaryTriNodeDataSetName =
    "/Boundary/bnd_tri->node"; //!<

[[maybe_unused]] static const char *C_HipBoundaryQuadDataSetName =
    "/Boundary/bnd_quad"; //!<

/**
 * @brief
 *
 */
[[maybe_unused]] static const char *C_HipPeriodicityGroupName =
    "/Periodicity"; //!<
[[maybe_unused]] static const char *C_HipPeriodicityPatchDataSetName =
    "/Periodicity/periodic_patch"; //!<
[[maybe_unused]] static const char *C_HipPeriodicityNodeDataSetName =
    "/Periodicity/periodic_node"; //!<

/**
 * @brief Hip mesh reader interface
 *
 * @todo Merge with the generic HDF5 XML reader
 */
class HipMeshReader {

  /**
   * @brief Write the HDF5 metric using the adapted payload information
   *
   * @param metric
   * @param owned_map
   * @param comm
   */
public:
  /**
   * @brief Write a contiguous distribution of nodes
   *        elements and boundary information to file
   *
   * @param file_prefix
   * @param coord
   * @param conn
   * @param binfo
   * @param periodic_pairs
   * @param comm
   * @param is_2d
   */
  static void WriteMesh(const std::string &file_prefix,
                        const std::vector<Point3d> &coord,
                        const std::vector<ElementConnectivity> &conn,
                        const binfo_t &binfo,
                        const std::vector<int> &periodic_pairs,
                        CommunicatorConcept &comm,
                        bool is_2d = true) {}

  /**
   * @brief Write the HDF5 hip mesh using the adapted payload information
   *
   * @param coord
   * @param conn
   * @param owned_map
   * @param adapt_binfo
   * @param comm
   */
  static void WriteMesh(std::vector<Point3d> &coord,
                        std::vector<ElementConnectivity> &conn,
                        std::map<id_size_t, id_size_t> &owned_map,
                        std::map<id_size_t, id_size_t> &spill_over_map,
                        const int npatches,
                        const std::vector<char> &patch_labels,
                        const std::vector<int> &periodic_pairs,
                        const std::vector<std::set<int>> &in_adapt_binfo,
                        const bface_t &in_adapt_bfaces,
                        CommunicatorConcept &comm,
                        const std::string &adapted_mesh_file_name,
                        bool is_2d = true) {
    // Remove all shared node info from the boundary data
    std::vector<std::set<int>> adapt_binfo(in_adapt_binfo);
    RemoveSharedInfo(adapt_binfo);
    /* Create inverse map */
    std::map<id_size_t, id_size_t> inv_owned_map;
    for (auto &item : owned_map)
      inv_owned_map[item.second] = item.first;

    std::map<id_size_t, id_size_t> inv_spill_map;
    for (auto &item : spill_over_map)
      inv_spill_map[item.second] = item.first;

    hdf5::Hyperslab<hsize_t, 1> m, f;
    hdf5::Hyperslab<hsize_t, 2> m2;
    id_size_t min_vertex_gid = std::numeric_limits<id_size_t>::max(),
              max_vertex_gid = std::numeric_limits<id_size_t>::min();
    for (const auto &item : owned_map) {
      if (min_vertex_gid > item.second)
        min_vertex_gid = item.second;
      if (max_vertex_gid < item.second)
        max_vertex_gid = item.second;
    }
    m.dims[0] = max_vertex_gid - min_vertex_gid + 1;
    m.count[0] = f.count[0] = max_vertex_gid - min_vertex_gid + 1;
    m.stride[0] = f.stride[0] = 1;
    f.dims[0] = max_vertex_gid;
    f.offset[0] = min_vertex_gid - 1;
    MPI_Bcast(
        f.dims, 1, GetMpiType<hsize_t>(), comm.size() - 1, comm.RawHandle());
    std::vector<double> temp_coord;
    /* Create plist object for I/O */
    auto plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, comm.RawHandle(), MPI_INFO_NULL);
    /* Open file collectively */
    auto file = H5Fcreate(
        adapted_mesh_file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    H5Pclose(plist_id);
    hdf5::CreateGroup(file, "Coordinates");
    hdf5::CreateGroup(file, "Connectivity");
    hdf5::CreateGroup(file, "Boundary");
    /* Copy X coordinate */
    temp_coord.clear();
    for (auto &item : inv_owned_map)
      temp_coord.push_back(coord[item.second].x);
    hdf5::WriteCollective(
        file, C_HipCoordinateDataSetNames[X_ID], f, m, temp_coord);
    /* Copy Y coordinate */
    temp_coord.clear();
    for (auto &item : inv_owned_map)
      temp_coord.push_back(coord[item.second].y);
    hdf5::WriteCollective(
        file, C_HipCoordinateDataSetNames[Y_ID], f, m, temp_coord);
    if (!is_2d) {
      /* Copy Z coordinate */
      temp_coord.clear();
      for (auto &item : inv_owned_map)
        temp_coord.push_back(coord[item.second].z);
      hdf5::WriteCollective(
          file, C_HipCoordinateDataSetNames[Z_ID], f, m, temp_coord);
    }

    hsize_t local_dims;
    if (is_2d) {
      /** Write connectivity of triangle */
      m2.dims[0] = conn.size();
      m2.dims[1] = 8;
      m2.count[0] = conn.size();
      m2.count[1] = 3;
      m2.stride[0] = 1;
      m2.stride[1] = 1;
      f.dims[0] = conn.size() * 3;
      f.stride[0] = 1;
      f.count[0] = conn.size() * 3;
      f.offset[0] = 0;
      local_dims = conn.size() * 3;
    } else {
      /** Write connectivity of tets */
      m2.dims[0] = conn.size();
      m2.dims[1] = 8;
      m2.count[0] = conn.size();
      m2.count[1] = 4;
      m2.stride[0] = 1;
      m2.stride[1] = 1;
      f.dims[0] = conn.size() * 4;
      f.stride[0] = 1;
      f.count[0] = conn.size() * 4;
      f.offset[0] = 0;
      local_dims = conn.size() * 4;
    }
    MPI_Allreduce(&local_dims,
                  f.dims,
                  1,
                  GetMpiType<hsize_t>(),
                  MPI_SUM,
                  comm.RawHandle());
    MPI_Exscan(&local_dims,
               f.offset,
               1,
               GetMpiType<hsize_t>(),
               MPI_SUM,
               comm.RawHandle());
    auto global_element_offset = is_2d ? f.offset[0] / 3 : f.offset[0] / 4;
    // auto global_element_size = is_2d ? f.dims[0] / 3 : f.dims[0] / 4;

    auto my_elem_dset = is_2d ? C_HipElementDataSetNames[TRI_ID]
                              : C_HipElementDataSetNames[TET_ID];
    hdf5::WriteCollective(
        file, my_elem_dset, f, m2, reinterpret_cast<id_size_t *>(conn.data()));

    /* Write the physical boundary node information */
    std::vector<id_size_t> bnode_count(npatches + 1);
    std::vector<id_size_t> file_bnode_offset(npatches + 1);
    std::vector<id_size_t> global_bnode_offset(npatches + 1);
    std::vector<std::vector<id_size_t>> bnodes(npatches);

    for (auto &item : inv_owned_map)
      for (auto &pid : adapt_binfo[item.second])
        bnodes[pid - 1].push_back(item.first);

    for (int i = 0; i < npatches; ++i)
      bnode_count[i + 1] = bnodes[i].size();
    MPI_Allreduce(&bnode_count[0],
                  &global_bnode_offset[0],
                  npatches + 1,
                  GetMpiType<id_size_t>(),
                  MPI_SUM,
                  comm.RawHandle());
    MPI_Exscan(&bnode_count[0],
               &file_bnode_offset[0],
               npatches + 1,
               GetMpiType<id_size_t>(),
               MPI_SUM,
               comm.RawHandle());
    for (int i = 0; i < npatches; ++i)
      global_bnode_offset[i + 1] += global_bnode_offset[i];
    for (int i = 0; i < npatches; ++i)
      file_bnode_offset[i + 1] += global_bnode_offset[i];

    /* Write file offset */
    file_bnode_offset.erase(file_bnode_offset.begin());
    bnode_count.erase(bnode_count.begin());
    m.dims[0] = npatches;
    m.offset[0] = 0;
    m.count[0] = npatches;
    m.stride[0] = 1;
    f.dims[0] = npatches;
    f.offset[0] = 0;
    f.count[0] = npatches;
    f.stride[0] = 1;
    if (!comm.IsMaster())
      f.count[0] = m.count[0] = 0;
    hdf5::WriteCollective(file,
                          C_HipBoundaryNodeOffsetDataSetName,
                          f,
                          m,
                          &global_bnode_offset[1]);

    // Write bnodes list
    for (int i = 0; i < npatches; ++i) {
      m.dims[0] = bnodes[i].size();
      m.offset[0] = 0;
      m.count[0] = bnodes[i].size();
      m.stride[0] = 1;
      /* */
      f.dims[0] = global_bnode_offset[npatches];
      f.offset[0] = file_bnode_offset[i];
      f.count[0] = bnode_count[i];
      f.stride[0] = 1;
      hdf5::WriteCollective(file,
                            C_HipBoundaryNodeDataSetName,
                            f,
                            m,
                            reinterpret_cast<id_size_t *>(&(bnodes[i][0])));
    }
    // Clear bnodes and purge heap back
    bnodes.clear();
    bnodes.shrink_to_fit();

    // Write the boundary face information
    std::vector<id_size_t> bface_count(npatches + 1);
    std::vector<id_size_t> file_bface_offset(npatches + 1);
    std::vector<id_size_t> global_bface_offset(npatches + 1);
    std::vector<std::vector<id_size_t>> bface_elmid(
        npatches); // The element connected to the face
    std::vector<std::vector<id_size_t>> bface_faceno(
        npatches); // The cannonical hip face no
    std::vector<std::vector<id_size_t>> bface_nodes(
        npatches); // List of node ids froming the face

    // Convert local boundary face info into hip data
    for (id_size_t ielm = 0; ielm < id_size_t(in_adapt_bfaces.size()); ++ielm) {
      const auto &my_elm = in_adapt_bfaces[ielm];
      for (const auto &my_bface : my_elm) {
        bface_elmid[my_bface.patch_id - 1].push_back(ielm +
                                                     global_element_offset + 1);
        bface_faceno[my_bface.patch_id - 1].push_back(my_bface.face_id + 1);
        if (!is_2d) {
          const auto &iface = C_TriElemFaceIndex[my_bface.face_id];
          bface_nodes[my_bface.patch_id - 1].push_back(
              conn[ielm].nodes[iface[0]]);
          bface_nodes[my_bface.patch_id - 1].push_back(
              conn[ielm].nodes[iface[1]]);
          bface_nodes[my_bface.patch_id - 1].push_back(
              conn[ielm].nodes[iface[2]]);
        } else {
          const auto &iface = C_BiElemFaceIndex[my_bface.face_id];
          bface_nodes[my_bface.patch_id - 1].push_back(
              conn[ielm].nodes[iface[0]]);
          bface_nodes[my_bface.patch_id - 1].push_back(
              conn[ielm].nodes[iface[1]]);
        }
      }
    }

    // Obtain offsets
    for (int i = 0; i < npatches; ++i)
      bface_count[i + 1] = bface_elmid[i].size();
    MPI_Allreduce(&bface_count[0],
                  &global_bface_offset[0],
                  npatches + 1,
                  GetMpiType<id_size_t>(),
                  MPI_SUM,
                  comm.RawHandle());
    MPI_Exscan(&bface_count[0],
               &file_bface_offset[0],
               npatches + 1,
               GetMpiType<id_size_t>(),
               MPI_SUM,
               comm.RawHandle());
    for (int i = 0; i < npatches; ++i)
      global_bface_offset[i + 1] += global_bface_offset[i];
    for (int i = 0; i < npatches; ++i)
      file_bface_offset[i + 1] += global_bface_offset[i];

    /* Write file offset */
    file_bface_offset.erase(file_bface_offset.begin());
    bface_count.erase(bface_count.begin());
    m.dims[0] = npatches;
    m.offset[0] = 0;
    m.count[0] = npatches;
    m.stride[0] = 1;
    f.dims[0] = npatches;
    f.offset[0] = 0;
    f.count[0] = npatches;
    f.stride[0] = 1;
    if (!comm.IsMaster())
      f.count[0] = m.count[0] = 0;
    if (!is_2d)
      hdf5::WriteCollective(file,
                            C_HipBoundaryTriElemOffsetDataSetName,
                            f,
                            m,
                            &global_bface_offset[1]);
    else
      hdf5::WriteCollective(file,
                            C_HipBoundaryBiElemOffsetDataSetName,
                            f,
                            m,
                            &global_bface_offset[1]);

    // Write hip bface informaion
    for (int i = 0; i < npatches; ++i) {
      /* Memory hyperslab */
      m.dims[0] = bface_elmid[i].size();
      m.offset[0] = 0;
      m.count[0] = bface_elmid[i].size();
      m.stride[0] = 1;
      /* File hyperslab */
      f.dims[0] = global_bface_offset[npatches];
      f.offset[0] = file_bface_offset[i];
      f.count[0] = bface_count[i];
      f.stride[0] = 1;

      if (!is_2d) {
        hdf5::WriteCollective(
            file,
            C_HipBoundaryTriElemDataSetName,
            f,
            m,
            reinterpret_cast<id_size_t *>(&(bface_elmid[i][0])));
        hdf5::WriteCollective(
            file,
            C_HipBoundaryTriFaceDataSetName,
            f,
            m,
            reinterpret_cast<id_size_t *>(&(bface_faceno[i][0])));
      } else {
        hdf5::WriteCollective(
            file,
            C_HipBoundaryBiElemDataSetName,
            f,
            m,
            reinterpret_cast<id_size_t *>(&(bface_elmid[i][0])));
        hdf5::WriteCollective(
            file,
            C_HipBoundaryBiFaceDataSetName,
            f,
            m,
            reinterpret_cast<id_size_t *>(&(bface_faceno[i][0])));
      }
      /* Modify hyperslab for face-node connectivity */
      const int num_face_nodes = is_2d ? 2 : 3;
      m.dims[0] = bface_elmid[i].size() * num_face_nodes;
      m.count[0] = bface_elmid[i].size() * num_face_nodes;
      f.dims[0] = global_bface_offset[npatches] * num_face_nodes;
      f.offset[0] = file_bface_offset[i] * num_face_nodes;
      f.count[0] = bface_count[i] * num_face_nodes;
      if (!is_2d)
        hdf5::WriteCollective(
            file,
            C_HipBoundaryTriNodeDataSetName,
            f,
            m,
            reinterpret_cast<id_size_t *>(&(bface_nodes[i][0])));
      else
        hdf5::WriteCollective(
            file,
            C_HipBoundaryBiNodeDataSetName,
            f,
            m,
            reinterpret_cast<id_size_t *>(&(bface_nodes[i][0])));
    }

    H5Fclose(file);
    // open in master rank (serial IO) to output patch_labels
    if (comm.IsMaster()) {
      file = H5Fopen(adapted_mesh_file_name.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
      WritePatchLabels(file, npatches, patch_labels);
      H5Fclose(file);
    }
  }

  /**
   * @brief Populate the boundary information by broadcasting the boundary info
   *        in master rank to slaves. Follows a round-robin assumed partitioning
   *        to achieve extreme scalability.
   *
   * @param num_nodes
   * @param binfo
   */
  int PopulateBoundaryInfo(my_size_t num_nodes, binfo_t &binfo) {
    int npatch = 0;
    if (m_communicator.IsValid()) {
      my_size_t local_begin, local_size;
      std::vector<my_size_t> bnode_offset;
      std::vector<id_size_t> bnodes(1000);
      m_communicator.RankDistribution(num_nodes, local_begin, local_size);
      binfo.resize(local_size);
      GetDsetOffsetMaster(C_HipBoundaryNodeOffsetDataSetName, bnode_offset);
      m_communicator.BroadcastMaster(&bnode_offset[0], bnode_offset.size());
      npatch = int(bnode_offset.size()) - 1;
      for (unsigned ipatch = 0; ipatch < bnode_offset.size() - 1; ++ipatch) {
        int num_bnodes =
            int(bnode_offset[ipatch + 1]) - int(bnode_offset[ipatch]);
        int num_batch = num_bnodes / 1000;
        int num_spill_over = num_bnodes - num_batch * 1000;
        int current_offset = bnode_offset[ipatch];
        if (m_communicator.IsMaster())
          std::cout << "Message: Reading boundary node chunks "
                    << " of patch " << ipatch + 1 << "\n";
        for (int ibatch = 0; ibatch < num_batch + 1; ++ibatch) {
          int num_batch_nodes = (ibatch != num_batch) ? 1000 : num_spill_over;
          GetBnodesMaster(num_batch_nodes, current_offset, bnodes);
          m_communicator.BroadcastMaster(&bnodes[0], num_batch_nodes);
          for (int i = 0; i < num_batch_nodes; ++i)
            if (my_size_t(bnodes[i]) >= local_begin + 1 &&
                my_size_t(bnodes[i]) <= local_begin + local_size)
              binfo[bnodes[i] - local_begin - 1].push_back(ipatch + 1);
          current_offset += num_batch_nodes;
        } // Batch loop ends
      } // Patch loop ends
#if 0
        int binfo_count = 0;
        for( auto &item : binfo )
          if(!item.empty())
            binfo_count++;
        for( int irank =0; irank < m_communicator.size(); ++irank ) {
          if( m_communicator.Rank() == irank )
            std::cout << "Total count = " << binfo_count << "\n";
          m_communicator.Barrier();
        }
#endif
    } // For valid communicator ranks
    return npatch;
  }

  /**
   * @brief Populate the boundary face data i.e., for
   *        each element what are the faces attached to
   *        the element. Note: Only elements having
   *        more than two boundary faces are stored and
   *        we call them ambiguous patch faces because
   *        they cannot be reconstructed from the node
   *        patch id data (aka binfo_t).
   *
   * @param num_elms
   * @param bfaces
   * @return number of patches (int)
   */
  int PopulatePatchFaces(my_size_t num_elms, bface_t &bfaces_info) {
    int npatch = 0;
    if (m_communicator.IsValid()) {
      my_size_t local_begin, local_size;
      std::vector<my_size_t> bface_offset;
      // Store the (elm_id, face_no, patch_id) of each boundary face
      std::vector<id_size_t> bfaces(2 * 1000);
      m_communicator.RankDistribution(num_elms, local_begin, local_size);
      bfaces_info.resize(local_size);
      std::string dst_name;
      int boundary_exists = 0;

      if (!m_is2d)
        dst_name =
            std::string(C_HipBoundaryTriDataSetName) + std::string("_lidx");
      else
        dst_name =
            std::string(C_HipBoundaryBiDataSetName) + std::string("_lidx");
      // Check if boundary DataSet exists
      if (m_communicator.IsMaster())
        if (H5Lexists(m_master_fhandle, dst_name.c_str(), H5P_DEFAULT) <= 0)
          boundary_exists = 1;
      m_communicator.BroadcastMaster(&boundary_exists, 1);

      if (boundary_exists == 0) {
        if (!m_is2d) {
          GetDsetOffsetMaster(dst_name.c_str(), bface_offset);
          m_communicator.BroadcastMaster(&bface_offset[0], bface_offset.size());
          npatch = int(bface_offset.size()) - 1;

          // std::stringstream cat;
          // cat << "debug_bface" << m_communicator.Rank() << ".dat";
          // std::ofstream fout(cat.str());
#if 1
          for (unsigned ipatch = 0; ipatch < bface_offset.size() - 1;
               ++ipatch) {
            int num_bfaces =
                int(bface_offset[ipatch + 1]) - int(bface_offset[ipatch]);
            int num_batch = num_bfaces / 1000;
            int num_spill_over = num_bfaces - num_batch * 1000;
            int current_offset = bface_offset[ipatch];
            if (m_communicator.IsMaster())
              std::cout << "Message: Reading triangle boundary face chunks "
                        << " of patch " << ipatch + 1 << "\n";
            for (int ibatch = 0; ibatch < num_batch + 1; ++ibatch) {
              int num_batch_faces =
                  (ibatch != num_batch) ? 1000 : num_spill_over;
              GetBfacesMaster(num_batch_faces,
                              current_offset,
                              bfaces,
                              C_HipBoundaryTriDataSetName);
              m_communicator.BroadcastMaster(&bfaces[0], 2 * num_batch_faces);
              for (int i = 0; i < num_batch_faces; ++i) {
                if (my_size_t(bfaces[2 * i]) >= local_begin + 1 &&
                    my_size_t(bfaces[2 * i]) <= local_begin + local_size) {
                  // fout << bfaces[2 * i] << " " << bfaces[2 * i + 1] << "\n";
                  // patch id and the face no
                  BoundaryFace temp = {int(ipatch + 1), int(bfaces[2 * i + 1])};
                  bfaces_info[bfaces[2 * i] - local_begin - 1].push_back(temp);
                  // std::cout << "Elm id = " << bfaces[2 * i] << " face num = "
                  //           << temp.face_id << "\n";
                }
              }
              current_offset += num_batch_faces;
            } // Batch loop ends
          } // Patch loop ends
        } // end of 3D mesh if
        // 2D mesh
        else {
          bface_offset.clear();
          GetDsetOffsetMaster(dst_name.c_str(), bface_offset);
          m_communicator.BroadcastMaster(&bface_offset[0], bface_offset.size());
          npatch = int(bface_offset.size()) - 1;
          // For 2D meshes we check boundary segments
          for (unsigned ipatch = 0; ipatch < bface_offset.size() - 1;
               ++ipatch) {
            int num_bfaces =
                int(bface_offset[ipatch + 1]) - int(bface_offset[ipatch]);
            int num_batch = num_bfaces / 1000;
            int num_spill_over = num_bfaces - num_batch * 1000;
            int current_offset = bface_offset[ipatch];
            if (m_communicator.IsMaster())
              std::cout << "Message: Reading boundary segment chunks "
                        << " of patch " << ipatch + 1 << "\n";
            for (int ibatch = 0; ibatch < num_batch + 1; ++ibatch) {
              int num_batch_faces =
                  (ibatch != num_batch) ? 1000 : num_spill_over;
              GetBfacesMaster(num_batch_faces,
                              current_offset,
                              bfaces,
                              C_HipBoundaryBiDataSetName);
              m_communicator.BroadcastMaster(&bfaces[0], 2 * num_batch_faces);
              for (int i = 0; i < num_batch_faces; ++i) {
                if (my_size_t(bfaces[2 * i]) >= local_begin + 1 &&
                    my_size_t(bfaces[2 * i]) <= local_begin + local_size) {
                  // fout << bfaces[2 * i] << " " << bfaces[2 * i + 1] << "\n";
                  // patch id and the face no
                  BoundaryFace temp = {int(ipatch + 1), int(bfaces[2 * i + 1])};
                  bfaces_info[bfaces[2 * i] - local_begin - 1].push_back(temp);
                  // std::cout << "Elm id = " << bfaces[2 * i] << " face num = "
                  //           << temp.face_id << "\n";
                }
              }
              current_offset += num_batch_faces;
            } // Batch loop ends
          } // Patch loop ends
        } // end of dimension if
      }
// Debug print the element and connecting boundary face_no
#if 0
      for (id_size_t i=0; i<bfaces_info.size(); ++i) {
        if( !bfaces_info[i].empty() )
          fout << i + local_begin + 1 << " ";
        for ( int j=0; j<bfaces_info[i].size(); ++j )
          fout << bfaces_info[i][j].face_id << " ";
        if( !bfaces_info[i].empty() )
          fout << "\n";
      }
#endif

#endif

// Check and print total number of boundary faces read
#if 0
      int bfaces_count = 0;
      for( const auto &item : bfaces_info )
        for( const auto &item_in_item : item )
          bfaces_count++;
      m_communicator.AllReduceSum(&bfaces_count, 1);
      if( m_communicator.IsMaster() )
         std::cout << "Total count = " << bfaces_count << "\n";
      // for( int irank =0; irank < m_communicator.size(); ++irank ) {
      //   if( m_communicator.Rank() == irank )
      //     std::cout << "Total count = " << bfaces_count << "\n";
      //   m_communicator.Barrier();
      // }
#endif
      // Look for elements having ambiguous faces
      // for (const auto &face : bfaces_info)
      //   if (!face.empty())
      //     if (face.size() > 1)
      //       std::cout << "Found ambiguous face belonging to " <<
      //       face[0].patch_id
      //                 << " size = " << face.size() << "\n";
    } // For valid communicator ranks
    return npatch;
  }

  /**
   * @brief Read the boundary nodes in the master rank given offset and chunk
   *        size
   *
   * @param num_bnodes
   * @param bnode_offset
   * @param bnodes
   * @param write_debug
   */
  void GetBnodesMaster(int num_batch_nodes,
                       int current_offset,
                       std::vector<id_size_t> &bnodes,
                       bool write_debug = false) {
    // Read the chunk in master and broadcast
    if (m_communicator.IsMaster()) {
      hdf5::Hyperslab<hsize_t, 1> file_slab, memory_slab;

      file_slab.count[0] = num_batch_nodes;
      file_slab.stride[0] = 1;
      file_slab.offset[0] = current_offset;

      memory_slab.dims[0] = num_batch_nodes;
      memory_slab.count[0] = num_batch_nodes;
      memory_slab.stride[0] = 1;
      memory_slab.offset[0] = 0;

      hdf5::Read(m_master_fhandle,
                 C_HipBoundaryNodeDataSetName,
                 file_slab,
                 memory_slab,
                 &bnodes[0]);
      if (write_debug)
        std::cout << "Message: Reading boundary nodes size : "
                  << num_batch_nodes << " offset " << current_offset << "\n";
    }
  }

  /**
   * @brief Read the boundary face data in the master rank given offset and
   * chunk size
   *
   * @param num_bfaces
   * @param bface_offset
   * @param bfaces
   * @param write_debug
   */
  void GetBfacesMaster(int num_bfaces,
                       int bface_offset,
                       std::vector<id_size_t> &bfaces,
                       const char *dset_name,
                       bool write_debug = false) {
    // Read the chunk in master and broadcast
    if (m_communicator.IsMaster()) {
      hdf5::Hyperslab<hsize_t, 1> file_slab, memory_slab;

      file_slab.count[0] = num_bfaces;
      file_slab.stride[0] = 1;
      file_slab.offset[0] = bface_offset;

      // Read the element id of the patch face
      memory_slab.dims[0] = 2 * num_bfaces;
      memory_slab.count[0] = num_bfaces;
      memory_slab.stride[0] = 2;
      memory_slab.offset[0] = 0;
      auto elm_dset_name = std::string(dset_name) + std::string("->elem");
      hdf5::Read(m_master_fhandle,
                 elm_dset_name.c_str(),
                 file_slab,
                 memory_slab,
                 &bfaces[0]);

      // Read the faceno inside of the containing element of the patch face
      memory_slab.dims[0] = 2 * num_bfaces;
      memory_slab.count[0] = num_bfaces;
      memory_slab.stride[0] = 2;
      memory_slab.offset[0] = 1;
      auto fce_dset_name = std::string(dset_name) + std::string("->face");
      hdf5::Read(m_master_fhandle,
                 fce_dset_name.c_str(),
                 file_slab,
                 memory_slab,
                 &bfaces[0]);

      if (write_debug)
        std::cout << "Message: Reading boundary faces size : " << num_bfaces
                  << " offset " << bface_offset << "\n";
    }
  }

  /**
   * @brief Read some arbitrary data in the master rank (used by check_adapt
   *        app)
   *
   * @param num_bnodes
   * @param bnode_offset
   * @param bnodes
   * @param write_debug
   */
  void GetArbitraryDataMaster(const std::string &dset,
                              std::vector<id_size_t> &arb) {
    hsize_t file_dims[2];
    if (m_communicator.IsMaster()) {
      // Read the number of boundary nodes from file
      int rank = hdf5::GetDimensions(m_master_fhandle, dset.c_str(), file_dims);
      if (rank < 0)
        std::runtime_error("Negative rank in dataset");
      std::cout << "Rank of " << dset << " = " << rank
                << " and dim:0 = " << file_dims[0] << "\n";
      arb.resize(file_dims[0]);
      hdf5::Hyperslab<hsize_t, 1> file_slab, memory_slab;
      file_slab.dims[0] = file_dims[0];
      file_slab.count[0] = file_dims[0];
      file_slab.stride[0] = 1;
      file_slab.offset[0] = 0;

      memory_slab.dims[0] = file_dims[0];
      memory_slab.count[0] = file_dims[0];
      memory_slab.stride[0] = 1;
      memory_slab.offset[0] = 0;

      hdf5::Read(
          m_master_fhandle, dset.c_str(), file_slab, memory_slab, &arb[0]);
    }
  }

  /**
   * @brief Read the boundary nodes offset information from HDF5 mesh in the
   *        master MPI rank
   *
   * @param bnode_offset
   */
  void GetDsetOffsetMaster(const char *dset_name,
                           std::vector<my_size_t> &bnode_offset) {
    hsize_t file_dims[2];
    if (m_communicator.IsMaster()) {
      // Read the number of boundary nodes from file
      int rank = hdf5::GetDimensions(m_master_fhandle, dset_name, file_dims);
      if (rank < 0)
        std::runtime_error("Negative rank in dataset");
      // std::cout << "Rank of bnode-offset = " << rank
      //          << " and dim:0 = " << file_dims[0] << "\n";
    }
    m_communicator.BroadcastMaster(file_dims, 1);
    bnode_offset.resize(file_dims[0] + 1);
    if (m_communicator.IsMaster()) {
      // std::cout << "Message: Size of bnode offset = " << file_dims[0] + 1
      //          << "\n";
      hdf5::Hyperslab<hsize_t, 1> file_slab, memory_slab;

      file_slab.dims[0] = file_dims[0];
      file_slab.count[0] = file_dims[0];
      file_slab.stride[0] = 1;
      file_slab.offset[0] = 0;

      memory_slab.dims[0] = file_dims[0] + 1;
      memory_slab.count[0] = file_dims[0];
      memory_slab.stride[0] = 1;
      memory_slab.offset[0] = 1;

      hdf5::Read(m_master_fhandle,
                 dset_name,
                 file_slab,
                 memory_slab,
                 &bnode_offset[0]);
    }
  }

  /**
   * @brief Hip HDF5 mesh reader constructor
   *
   * @param filename
   * @param communicator
   */
  HipMeshReader(const std::string filename, CommunicatorConcept &communicator)
      : m_mesh_filename(filename),
        m_communicator("HipMeshReader") {
    m_communicator.Copy(communicator);
    if (m_communicator.IsMaster()) {
      /* Open HDF5 file in serial mode and read sizes */
      m_master_fhandle =
          H5Fopen(m_mesh_filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
      if (H5Lexists(m_master_fhandle,
                    C_HipCoordinateDataSetNames[Z_ID],
                    H5P_DEFAULT) <= 0)
        m_is2d = true;
    }
    MPI_Bcast(&m_is2d, 1, MPI_CXX_BOOL, 0, m_communicator.RawHandle());
  }

  HipMeshReader() {}

  template <typename T>
  void GetPeriodicPatch(std::vector<T> &patch_pairs, T &num_patch_pairs) {
    patch_pairs.clear();
    patch_pairs.shrink_to_fit();
    /* Check for Communicator validity */
    if (m_communicator.IsValid()) {
      /* Open in master and read periodic data if present */
      if (m_communicator.IsMaster())
        if (H5Lexists(
                m_master_fhandle, C_HipPeriodicityGroupName, H5P_DEFAULT) > 0)
          hdf5::Read(
              m_master_fhandle, C_HipPeriodicityPatchDataSetName, patch_pairs);

      num_patch_pairs = patch_pairs.size();

      /* Broadcast the sizes */
      m_communicator.BroadcastMaster(&num_patch_pairs, 1);

      /* Resize using bcast size */
      if (!m_communicator.IsMaster())
        patch_pairs.resize(num_patch_pairs);

      /* Bcast the entire array from master to slaves */
      if (num_patch_pairs > 0)
        m_communicator.BroadcastMaster(&patch_pairs[0], patch_pairs.size());

      if (m_communicator.IsMaster())
        std::cout << "Message: Found " << num_patch_pairs / 2
                  << " periodic patch pairs in mesh file" << std::endl;
    }
  }

  /**
   * @brief Get the Periodic Nodes
   *
   * @tparam T
   * @param patch_nodes
   */
  template <typename T> void GetPeriodicNodes(std::vector<T> &patch_nodes) {
    /* Check for Communicator validity */
    if (m_communicator.IsValid()) {
      /* Only implement perioidc nodes for single or serial cases for now */
      if (m_communicator.size() == 1) {
        if (H5Lexists(
                m_master_fhandle, C_HipPeriodicityGroupName, H5P_DEFAULT) > 0) {
          hdf5::Read(
              m_master_fhandle, C_HipPeriodicityNodeDataSetName, patch_nodes);
          std::vector<T> after(patch_nodes.size());
          // std::ofstream fout("ppp.dat");
          int i2 = patch_nodes.size() - 1;
          for (unsigned i1 = 0; i1 < patch_nodes.size() / 2; ++i1, --i2) {
            after[2 * i1] = patch_nodes[i1];
            after[2 * i1 + 1] = patch_nodes[i2];
          }
          patch_nodes.swap(after);
        }
      }
    }
  }

  /**
   * @brief Get the element sizes (of all element types) in master and broadcast
   * to slaves
   *
   * @tparam T
   * @param element_sizes
   */
  template <typename T> void GetElementSizes(T *element_sizes) {
    /* Check for Communicator validity */
    if (m_communicator.IsValid()) {
      /* Open in master */
      if (m_communicator.IsMaster()) {
        /* Read each element sizes */
        for (unsigned i = 0; i < C_NumElementTypes; ++i) {
          if (H5Lexists(m_master_fhandle,
                        C_HipElementDataSetNames[i],
                        H5P_DEFAULT) > 0) {
            hsize_t file_dims[2];
            int file_rank = hdf5::GetDimensions(
                m_master_fhandle, C_HipElementDataSetNames[i], file_dims);
            if (file_rank > 1) {
            }
            assert(file_rank < 2);
            element_sizes[i] = file_dims[0] / C_ElementNumNodes[i];
          }
        }
      }
      /* Broadcast the sizes */
      m_communicator.BroadcastMaster(element_sizes, C_NumElementTypes);
    }
  }

  /**
   *
   * @param file
   * @param node_dht
   */
  void ReadNodeCoordinates(my_size_t global_size,
                           DistributedHashTable<id_size_t, Point3d> &node_dht) {
    node_dht.SetSize(global_size);
    node_dht.PopulatePoint(m_mesh_filename, "/Coordinates");
  }

  /**
   * @brief Get the number of nodes in master and broadcast to slaves
   *
   * @tparam T
   * @param node_size
   */
  template <typename T> void GetNodeSize(T &node_size) {
    /* Check for Communicator validity */
    if (m_communicator.IsValid()) {
      /* Open in master */
      if (m_communicator.IsMaster()) {
        /* Open HDF5 file in serial mode and read sizes */
        hsize_t file_dims[2];
        int file_rank = hdf5::GetDimensions(
            m_master_fhandle, C_HipCoordinateDataSetNames[X_ID], file_dims);
        if (file_rank > 1) {
        }
        assert(file_rank < 2);
        node_size = file_dims[0];
      }
      /* Broadcast the sizes */
      m_communicator.BroadcastMaster(&node_size, 1);
    }
  }

  /**
   * @brief Reads the element-node connectivity of the mesh
   *
   *
   * @param dht
   * @param node_keys
   */
  std::pair<Point3d, Point3d>
  ReadElements(my_size_t global_size,
               DistributedHashTable<id_size_t, Point3d> &node_dht,
               DistributedHashTable<id_size_t, ElementConnectivity> &dht,
               DistributedHashTable<id_size_t, Point3d> &centroid_dht) {
    std::pair<Point3d, Point3d> ret_point =
        std::make_pair(Point3d{std::numeric_limits<double>::max()},
                       Point3d{std::numeric_limits<double>::min()});
    if (m_communicator.IsValid()) {
      dht.SetSize(global_size);
      centroid_dht.SetSize(global_size);
      // Keys stored in current MPI Rank
      std::vector<id_size_t> keys;
      std::vector<ElementConnectivity> buffer;
      // Local and Global Element Offsets
      my_size_t global_offsets[C_NumElementTypes + 1] = {0};
      /** Get element sizes and offsets **/
      GetElementSizes(&global_offsets[1]);
      for (id_size_t i = 0; i < id_size_t(C_NumElementTypes); ++i)
        global_offsets[i + 1] += global_offsets[i];
      dht.PopulateElements(m_mesh_filename.c_str(),
                           C_HipElementDataSetNames,
                           global_offsets,
                           keys,
                           buffer);
      /** Get Nodes of the local list of elements **/
      std::vector<id_size_t> node_keys;
      std::vector<Point3d> node_coordinates;
      ConvertToLocalIndex(buffer, node_keys);
      node_coordinates.resize(node_keys.size());
      for (auto &item : node_keys)
        item--;
      node_dht.GetDataFromKeys(node_keys, node_coordinates);
      node_keys.clear();
      node_keys.shrink_to_fit();
      /** Form the AABB **/
      for (const auto &item : node_coordinates) {
        // Min
        if (ret_point.first.x > item.x)
          ret_point.first.x = item.x;
        if (ret_point.first.y > item.y)
          ret_point.first.y = item.y;
        if (ret_point.first.z > item.z)
          ret_point.first.z = item.z;
        // Max
        if (ret_point.second.x < item.x)
          ret_point.second.x = item.x;
        if (ret_point.second.y < item.y)
          ret_point.second.y = item.y;
        if (ret_point.second.z < item.z)
          ret_point.second.z = item.z;
      }
      /** Form the centroid of the elements **/
      std::vector<Point3d> centroid(keys.size());
      for (id_size_t i_element = 0; i_element < id_size_t(buffer.size());
           ++i_element) {
        int num_nodes_element = 0;
        for (auto &node_index : buffer[i_element].nodes) {
          if (node_index > 0) {
            if (node_index > id_size_t(node_coordinates.size()))
              std::cout << "node_index = " << node_index
                        << " coordinate_size = " << node_coordinates.size()
                        << "\n";
            centroid[i_element].x += node_coordinates[node_index - 1].x;
            centroid[i_element].y += node_coordinates[node_index - 1].y;
            centroid[i_element].z += node_coordinates[node_index - 1].z;
            num_nodes_element++;
          }
        }
        centroid[i_element].x /= num_nodes_element;
        centroid[i_element].y /= num_nodes_element;
        centroid[i_element].z /= num_nodes_element;
      }
      node_coordinates.clear();
      centroid_dht.Populate(keys, centroid);
      node_dht.CommunicatorHandle().AllReduceMin(&(ret_point.first.x), 3);
      node_dht.CommunicatorHandle().AllReduceMax(&(ret_point.second.x), 3);
    } // Valid root communicator handle
    return ret_point;
  }

  /**
   * @brief Close the master file handle
   *
   */
  void close() {
    if (m_communicator.IsValid())
      if (m_communicator.IsMaster())
        if (m_master_fhandle != 0)
          H5Fclose(m_master_fhandle);
    m_master_fhandle = 0;
  }

  /**
   * @brief Destructor of @ref HipMeshReader
   *
   * Closes the serial file handle @ref hdf_file_handle_ in @ref
   * CommunicatorConcept::MasterRank()
   */
  ~HipMeshReader() { close(); }

  /**
   * @brief Check if mesh is a 2D mesh
   * @return
   */
  bool Is2D() { return m_is2d; }

  /**
   *
   * @brief Reproduction of hip's h5_read_fxStr function to handle
   *        the awkward (ugly?) patch label data of hip HDF5
   *
   * @param link Label of the dataset
   * @param dim
   * @param fxStr Type of string (fx80 or fx240)
   * @param string To create newly and allocated here
   *
   * @return int Number of elements of fxStr present (if string==NULL), or #
   * read
   *
   */
  void ReadPatchLabels(const int npatches, std::vector<char> &str) {
    if (m_communicator.IsMaster()) {
      // Read dataset only if it exists
      if (H5Lexists(m_master_fhandle, "/Boundary/PatchLabels", H5P_DEFAULT)) {
        auto dset_id =
            H5Dopen(m_master_fhandle, "/Boundary/PatchLabels", H5P_DEFAULT);
        auto dtype_id = H5Dget_type(dset_id);
        auto sdim = H5Tget_size(dtype_id);
        str.resize(npatches * (sdim + 1)); // +1 for NULL term

        auto mTyp_id = H5Tcopy(H5T_C_S1);
        H5Tset_size(mTyp_id, sdim);
        auto dspc_id = H5Dget_space(dset_id);
        hsize_t hsz_dim[1] = {0};
        H5Sget_simple_extent_dims(dspc_id, hsz_dim, NULL);
        auto status = H5Dread(
            dset_id, mTyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, str.data());
        if (status < 0)
          throw std::runtime_error("Error reading dataset");
        status = H5Dclose(dset_id);
        status = H5Sclose(dspc_id);
        status = H5Tclose(dtype_id);
        status = H5Tclose(mTyp_id);
      }
    }
  }

  /**
   *
   * @brief Reproduction of hip's h5_write_fxStr function to handle
   *        the awkward (ugly?) patch label data of hip HDF5
   *
   * @param fhandle
   * @param link
   * @param npatches
   * @param str
   *
   * @return int
   *
   */
  static void WritePatchLabels(hid_t &fhandle,
                               const int npatches,
                               const std::vector<char> &str) {
    // Write dataset only if it is not empty
    if (!str.empty()) {
      hsize_t hsz = 240; // Max string size for a patch in hip
      auto typ_id = H5Tcopy(H5T_C_S1);
      H5Tset_size(typ_id, hsz);

      hsize_t hsz_dim[1] = {hsize_t(npatches)};
      auto dspc_id = H5Screate_simple(1, hsz_dim, NULL);
      auto dset_id = H5Dcreate(fhandle,
                               "/Boundary/PatchLabels",
                               typ_id,
                               dspc_id,
                               H5P_DEFAULT,
                               H5P_DEFAULT,
                               H5P_DEFAULT);
      auto status =
          H5Dwrite(dset_id, typ_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, str.data());
      if (status < 0)
        throw std::runtime_error("Error writing dataset");
      status = H5Sclose(dspc_id);
      status = H5Dclose(dset_id);
    }
  }

private:
  const std::string m_mesh_filename;  //!<
  CommunicatorConcept m_communicator; //!<
  hid_t m_master_fhandle = 0;         //!< Serial file handle on master rank
  bool m_is2d = false;                //!<
};

} // namespace taru
