/**
 *  @file communicator_level.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Documentation for communicator_level.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "communicator_mpi.hpp"

namespace taru {

/**
 * @brief Level class
 *
 */
template <typename T_Payload> struct CommunicatorLevel {
public:
  /**
   * @brief Empty constructor give names for debug
   *
   */
  CommunicatorLevel()
      : m_leaves_per_branch(0),
        m_parent("m_parent"),
        m_branch("m_branch"),
        m_leaf("m_leaf"){};

  /**
   * @brief  Construct the level given the parent, root, and leaves_per_branch
   *
   * @param in_parent
   * @param leaves_per_branch
   */
  void set(const CommunicatorConcept &in_parent,
           const rank_t leaves_per_branch) {
    m_leaves_per_branch = leaves_per_branch;
    /* Check if I am not at the last level
      if so then deduce the @ref leaves_per_branch_ using
      the communicator size of @ref parent_  */
    auto num_branches = in_parent.size() / leaves_per_branch;
    m_parent.Copy(in_parent);
    /* Split communicator into core sizes */
    auto colour = m_parent.Rank() / leaves_per_branch;
    m_parent.Split(colour, m_leaf);
    /* Select ranks from communicator and form branches */
    std::vector<rank_t> select_ranks(num_branches);
    select_ranks[0] = 0;
    for (rank_t i = 1; i < num_branches; ++i)
      select_ranks[i] = select_ranks[i - 1] + leaves_per_branch;
    m_parent.Select(select_ranks, m_branch);
  }

  /**
   * @brief  Access branch communicator
   *
   * @return
   */
  CommunicatorConcept &Branch() { return m_branch; }

  /**
   * @brief  Access leaf communicator
   *
   * @return
   */
  CommunicatorConcept &Leaf() { return m_leaf; }

  /**
   * @brief  Access parent communicator
   *
   * @return
   */
  CommunicatorConcept &Parent() { return m_parent; }

  /**
   * @brief  Access to payload data in the communicator
   *
   * @return
   */
  T_Payload &Payload() { return m_payload; }

  /**
   *
   * @return
   */
  rank_t GetLeavesPerBranch() { return m_leaves_per_branch; }

  /**
   *
   * @return
   */
  void SetLeavesPerBranch(rank_t leaves_per_branch) {
    m_leaves_per_branch = leaves_per_branch;
  }

private:
  rank_t m_leaves_per_branch = 0; //!< The number of leaves per branch
                                  //!< (@ref parent is divided into
                                  //!< @ref leaves_per_branch groups of
                                  //!< @ref leaf communicator each
                                  //!< with a common @ref branch communicator)
  CommunicatorConcept m_parent;   //!< The parent communicator that
                                  //!< will be divided into @ref branch
                                  //!< and @ref leaf communicator
  CommunicatorConcept m_branch;   //!< The branch communicator
                                  //!< (common one to a group of
                                  //!< @ref leaves_per_branch leaf(s))
  CommunicatorConcept m_leaf;     //!< The leaf communicator (each branch
                                  //!< has @ref leaves_per_branch leaf(s))
  T_Payload m_payload; //!< Payload to store (generic) data at each level
};
} // namespace taru
