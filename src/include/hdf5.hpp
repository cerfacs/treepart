/**
 *  @file hdf5.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Documentation for hdf5.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "communicator_mpi.hpp"
#include "global_timer.hpp"
#include <fstream>
#include <hdf5.h>
#include <memory>
#include <sstream>

/**
 * @namespace hdf5
 * @brief HDF5 namespace for templated hdf5 routines
 *
 */

namespace hdf5 {

/**
 * @brief Hyperslab data-structure to hold IO hyperslab info
 *
 * This is a POD type for easy serialisation using MPI so
 * we restrict to max 2D matrices
 *
 * @tparam T_Data
 * @tparam N
 */
template <typename T_Data, int N> struct Hyperslab {
  T_Data offset[N] = {0}; //!< Offset of the hyperslab selection
  T_Data stride[N] = {0}; //!< Stride of the hyperslab selection
  T_Data count[N] = {0};  //!< Number of items in the hyperslab selection
  T_Data dims[N] = {0};   //!< Original dimensions of the data space
};

/**
 * @brief Generic function to obtain HDF5 data type using template
 *
 * @tparam T
 * @return
 */
template <typename T> hid_t &GetDatatype();

template <> inline hid_t &GetDatatype<char>() { return H5T_NATIVE_CHAR; }

template <> inline hid_t &GetDatatype<signed char>() {
  return H5T_NATIVE_SCHAR;
}

template <> inline hid_t &GetDatatype<unsigned char>() {
  return H5T_NATIVE_UCHAR;
}

template <> inline hid_t &GetDatatype<short>() { return H5T_NATIVE_SHORT; }

template <> inline hid_t &GetDatatype<unsigned short>() {
  return H5T_NATIVE_USHORT;
}

template <> inline hid_t &GetDatatype<int>() { return H5T_NATIVE_INT; }

template <> inline hid_t &GetDatatype<unsigned>() { return H5T_NATIVE_UINT; }

template <> inline hid_t &GetDatatype<long>() { return H5T_NATIVE_LONG; }

template <> inline hid_t &GetDatatype<unsigned long>() {
  return H5T_NATIVE_ULONG;
}

template <> inline hid_t &GetDatatype<long long>() { return H5T_NATIVE_LLONG; }

template <> inline hid_t &GetDatatype<unsigned long long>() {
  return H5T_NATIVE_ULLONG;
}

template <> inline hid_t &GetDatatype<long double>() {
  return H5T_NATIVE_LDOUBLE;
}

template <> inline hid_t &GetDatatype<double>() { return H5T_NATIVE_DOUBLE; }

template <> inline hid_t &GetDatatype<float>() { return H5T_NATIVE_FLOAT; }

template <> inline hid_t &GetDatatype<int const>() { return H5T_NATIVE_INT32; }

template <> inline hid_t &GetDatatype<unsigned const>() {
  return H5T_NATIVE_UINT;
}

template <> inline hid_t &GetDatatype<long const>() { return H5T_NATIVE_LONG; }

template <> inline hid_t &GetDatatype<unsigned long const>() {
  return H5T_NATIVE_ULONG;
}

/**
 * @brief Open a HDF5 file using the provided communicator
 *
 * @param comm
 * @return hid_t
 */
inline hid_t OpenFile(const std::string &file,
                      unsigned flags,
                      taru::CommunicatorConcept &comm) {
  hid_t hdf_file_handle = 0;
  if (comm.IsValid()) {
    // In the master rank check if file exists
    if (comm.IsMaster()) {
      std::ifstream fin(file);
      if (fin.fail()) {
        std::cerr << "Error: Failed to open file " << file << "\n";
        MPI_Abort(comm.RawHandle(), -1);
      }
    }
    // All ok open file now
    auto plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, comm.RawHandle(), MPI_INFO_NULL);
    /* Open file collectively */
    hdf_file_handle = H5Fopen(file.c_str(), flags, plist_id);
    H5Pclose(plist_id);
  }
  return hdf_file_handle;
}

/**
 * @brief Open a serial HDF5 file
 *
 * @param comm
 * @return hid_t
 */
inline hid_t OpenFile(const std::string &file, unsigned flags) {
  hid_t hdf_file_handle = 0;
  std::ifstream fin(file);
  if (fin.fail()) {
    std::cerr << "Error: Failed to open file " << file << "\n";
    abort();
  }
  fin.close();
  hdf_file_handle = H5Fopen(file.c_str(), flags, H5P_DEFAULT);
  return hdf_file_handle;
}

/**
 * @brief Open a serial HDF5 file
 *
 * @param comm
 * @return hid_t
 */
inline hid_t CreateFile(const std::string &file,
                        unsigned flags,
                        taru::CommunicatorConcept &comm) {
  hid_t hdf_file_handle = 0;
  if (comm.IsValid()) {
    auto plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, comm.RawHandle(), MPI_INFO_NULL);
    hdf_file_handle = H5Fcreate(file.c_str(), flags, H5P_DEFAULT, plist_id);
    H5Pclose(plist_id);
  }
  return hdf_file_handle;
}

/**
 * @brief Returns the dimensions of the given dataset
 *
 * @param file_handle
 * @param path
 * @param file_dims
 * @return
 */
inline int
GetDimensions(hid_t file_handle, const char *path, hsize_t *file_dims) {
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStart);
  hid_t dset, dspace;
  int rank;
  if (H5Lexists(file_handle, path, H5P_DEFAULT) <= 0)
    return -1;
  dset = H5Dopen(file_handle, path, H5P_DEFAULT);
  dspace = H5Dget_space(dset);
  rank = H5Sget_simple_extent_dims(dspace, file_dims, nullptr);
  H5Sclose(dspace);
  H5Dclose(dset);
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStop);
  return rank;
}

/**
 * @brief Read the given dataset in the hyperslab into the buffer
 *
 * @tparam T
 * @tparam N
 * @tparam M
 * @param file_handle
 * @param link
 * @param file_slab
 * @param memory_slab
 * @param buffer
 * @return
 */
template <typename T, int N, int M>
herr_t Read(hid_t &file_handle,
            const char link[],
            Hyperslab<hsize_t, N> &file_slab,
            Hyperslab<hsize_t, M> &memory_slab,
            T *buffer) {
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStart);
  hid_t data_set_file;
  hid_t data_space_file, data_space_memory;
  herr_t status;

  data_space_memory = H5Screate_simple(M, memory_slab.dims, NULL);
  status = H5Sselect_hyperslab(data_space_memory,
                               H5S_SELECT_SET,
                               memory_slab.offset,
                               memory_slab.stride,
                               memory_slab.count,
                               NULL);
  assert(status >= 0);
  data_set_file = H5Dopen(file_handle, link, H5P_DEFAULT);
  data_space_file = H5Dget_space(data_set_file);
#ifdef DEBUG_HDF5_IO
  int frank = H5Sget_simple_extent_ndims(data_space_file);
  hsize_t fdims_current[frank], fdims_max[frank];
  H5Sget_simple_extent_dims(data_space_file, fdims_current, fdims_max);
  std::cout << "Message: Memory space {rank: " << 1
            << ", dims: " << mslab.dims[0] << ", stride: " << mslab.stride[0]
            << ", count: " << mslab.count[0] << "}\n";
  std::cout << "Message: File dataset \"" << link << "\": {rank: " << frank
            << ", dims: " << fdims_current[0] << "}\n";
#endif
  status = H5Sselect_hyperslab(data_space_file,
                               H5S_SELECT_SET,
                               file_slab.offset,
                               file_slab.stride,
                               file_slab.count,
                               NULL);
  assert(status >= 0);
  status = H5Dread(data_set_file,
                   GetDatatype<T>(),
                   data_space_memory,
                   data_space_file,
                   H5P_DEFAULT,
                   buffer);
  H5Dclose(data_set_file);
  H5Sclose(data_space_file);
  H5Sclose(data_space_memory);
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStop);
  return status;
}

template <typename T>
static int Read(hid_t &file, const char *link, std::vector<T> &buf) {
  if (H5Lexists(file, link, H5P_DEFAULT) == 0) {
    std::stringstream cat;
    cat << "Error: Cannot find dataset " << link << " in hdf5 file";
    throw std::runtime_error(cat.str().c_str());
  }
  hsize_t dims[10];
  auto dset = H5Dopen(file, link, H5P_DEFAULT);
  auto dspace = H5Dget_space(dset);
  auto rank = H5Sget_simple_extent_dims(dspace, dims, nullptr);
  hsize_t bufsize = 1;
  for (int i = 0; i < rank; ++i)
    bufsize *= dims[i];
  buf.resize(bufsize);
  auto status = H5Dread(
      dset, GetDatatype<T>(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT, &buf[0]);
  if (status < 0)
    throw std::runtime_error("Error reading dataset");
  H5Sclose(dspace);
  H5Dclose(dset);
  return rank;
}

/**
 * @brief Write the given dataset in the hyperslab into the buffer
 *
 * @tparam T
 * @tparam N
 * @tparam M
 * @param file_handle
 * @param link
 * @param file_slab
 * @param memory_slab
 * @param buffer
 * @return
 */
template <typename T, int N, int M>
herr_t Write(hid_t &file_handle,
             const char link[],
             Hyperslab<hsize_t, N> &file_slab,
             Hyperslab<hsize_t, M> &memory_slab,
             T *buffer) {
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStart);
  hid_t data_set_file;
  hid_t data_space_file, data_space_memory;
  herr_t status;

  // Create memory dataset and dataspace
  data_space_memory = H5Screate_simple(M, memory_slab.dims, NULL);
  status = H5Sselect_hyperslab(data_space_memory,
                               H5S_SELECT_SET,
                               memory_slab.offset,
                               memory_slab.stride,
                               memory_slab.count,
                               NULL);
  assert(status >= 0);
  // Check if dataset already exists in file if not create the dataset
  if (!H5Lexists(file_handle, link, H5P_DEFAULT)) {
    data_space_file = H5Screate_simple(N, file_slab.dims, NULL);
    status = H5Sselect_hyperslab(data_space_file,
                                 H5S_SELECT_SET,
                                 file_slab.offset,
                                 file_slab.stride,
                                 file_slab.count,
                                 NULL);
    assert(status >= 0);
    data_set_file = H5Dcreate1(
        file_handle, link, GetDatatype<T>(), data_space_file, H5P_DEFAULT);
  }
  // Open the data set and check if the size matches with the file dims input
  else {
    data_set_file = H5Dopen(file_handle, link, H5P_DEFAULT);
    data_space_file = H5Dget_space(data_set_file);
    status = H5Sselect_hyperslab(data_space_file,
                                 H5S_SELECT_SET,
                                 file_slab.offset,
                                 file_slab.stride,
                                 file_slab.count,
                                 NULL);
    assert(status >= 0);
  }
  // Write the data-set to file based on the Hyperslab
  status = H5Dwrite(data_set_file,
                    GetDatatype<T>(),
                    data_space_memory,
                    data_space_file,
                    H5P_DEFAULT,
                    buffer);
  H5Dclose(data_set_file);
  H5Sclose(data_space_file);
  H5Sclose(data_space_memory);
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStop);
  return status;
}

/**
 * @brief Overloaded variant of Write function to write flat arrays
 * @tparam T
 * @tparam T_Int
 * @param file_handle
 * @param link
 * @param buffer
 * @param buffer_length
 * @return
 */
template <typename T, typename T_Int>
herr_t Write(hid_t &file_handle,
             const char link[],
             T *buffer,
             const T_Int buffer_length) {
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStart);

  // Create memory dataset and dataspace
  hsize_t length = buffer_length;
  auto data_space_file = H5Screate_simple(1, &length, nullptr);
  auto data_set_file = H5Dcreate1(
      file_handle, link, GetDatatype<T>(), data_space_file, H5P_DEFAULT);
  auto status = H5Dwrite(
      data_set_file, GetDatatype<T>(), H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer);
  if (status < 0)
    throw std::runtime_error("Error writing dataset");
  H5Dclose(data_set_file);
  H5Sclose(data_space_file);
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStop);
  return status;
}

/**
 * @brief Overloaded variant of Write function with groups support
 * @tparam T
 * @tparam T_Int
 * @param file_handle
 * @param link1
 * @param link2
 * @param buffer
 * @param buffer_length
 * @return
 */
template <typename T, typename T_Int>
herr_t Write(hid_t &file_handle,
             const std::string &link1,
             const std::string &link2,
             T *buffer,
             const T_Int buffer_length) {
  auto link = link1 + "/" + link2;
  return Write(file_handle, link.c_str(), buffer, buffer_length);
}

/**
 * @brief Create a new group in the hdf5 dataset
 * @param file_handle
 * @param link
 */
inline void CreateGroup(hid_t &file_handle, const char *link) {
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStart);
  auto part_group_id =
      H5Gcreate(file_handle, link, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  assert(part_group_id >= 0);
  H5Gclose(part_group_id);
  Time(global_timer::TIME_HDF5_IO, global_timer::C_TimerStop);
}

/**
 * @brief Create the MPI_Info object from a map of key->value pairs.
 *        This is created for giving IO hints to the HDF5 parallel
 *        library to optimise IO operation
 * @param keyval
 * @return
 */
inline std::unique_ptr<MPI_Info>
CreateInfoFromMap(std::map<std::string, std::string> &keyval) {
  std::unique_ptr<MPI_Info> ret = std::unique_ptr<MPI_Info>(new MPI_Info);
  return ret;
}

/**
 * @brief
 *
 * @tparam T
 * @tparam N
 * @tparam M
 * @param file_handle
 * @param link
 * @param f
 * @param m
 * @param data
 */
template <typename T, int N, int M>
void ReadCollective(hid_t &file_handle,
                    const std::string &link,
                    Hyperslab<hsize_t, N> &f,
                    Hyperslab<hsize_t, M> &m,
                    std::vector<T> &data,
                    bool check_failure = true) {
  ReadCollective(file_handle, link, f, m, data.data(), check_failure);
}

/**
 * @brief
 *
 * @tparam T
 * @tparam N
 * @tparam M
 * @param file_handle
 * @param link
 * @param f
 * @param m
 * @param data
 */
template <typename T, int N, int M>
void ReadCollective(hid_t &file_handle,
                    const std::string &link,
                    Hyperslab<hsize_t, N> &f,
                    Hyperslab<hsize_t, M> &m,
                    T *data,
                    bool check_failure = true) {
  hid_t dset_file;
  hid_t dspace_file, dspace_memory;
  hid_t plist_id;
  bool failed = false;

  plist_id = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
  dspace_memory = H5Screate_simple(M, m.dims, NULL);
  // Only create a data-space hyperslab if the total
  // count is non-zero
  hsize_t total_dset_count(1);
  for (int i = 0; i < M; ++i)
    total_dset_count *= m.count[i];
  if (total_dset_count != 0) {
    auto status = H5Sselect_hyperslab(
        dspace_memory, H5S_SELECT_SET, m.offset, m.stride, m.count, NULL);
    if (status < 0)
      throw std::runtime_error("Hyperslab selection failed");
  } else {
    H5Sselect_none(dspace_memory);
  }
  // Check if dataset already exists
  if (!H5Lexists(file_handle, link.c_str(), H5P_DEFAULT)) {
    failed = true;
    if (check_failure) {
      std::cerr << "Error: In reading dataset (not found)\n";
      abort();
    }
  } else {
    // If the dataset exists open it from the file
    dset_file = H5Dopen(file_handle, link.c_str(), H5P_DEFAULT);
    dspace_file = H5Dget_space(dset_file);
  }

  if (!failed) {
    // Hyperslab selection for file is non-null
    // only for non-zero total counts
    total_dset_count = 1;
    for (int i = 0; i < N; ++i)
      total_dset_count *= f.count[i];
    if (total_dset_count != 0) {
      auto status = H5Sselect_hyperslab(
          dspace_file, H5S_SELECT_SET, f.offset, f.stride, f.count, NULL);
      if (status < 0)
        throw std::runtime_error("Hyperslab selection failed");
    } else {
      H5Sselect_none(dspace_file);
    }
    plist_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
    auto status = H5Dread(dset_file,
                          GetDatatype<T>(),
                          dspace_memory,
                          dspace_file,
                          plist_id,
                          data);
    if (status < 0)
      throw std::runtime_error("Hyperslab selection failed");
    H5Dclose(dset_file);
    H5Sclose(dspace_file);
  }
  H5Sclose(dspace_memory);
  H5Pclose(plist_id);
}

/**
 * @brief
 *
 * @tparam T
 * @tparam N
 * @tparam M
 * @param file_handle
 * @param link
 * @param f
 * @param m
 * @param data
 */
template <typename T, int N, int M>
void WriteCollective(hid_t &file_handle,
                     const std::string &link,
                     Hyperslab<hsize_t, N> &f,
                     Hyperslab<hsize_t, M> &m,
                     std::vector<T> &data) {
  WriteCollective(file_handle, link, f, m, data.data());
}

/**
 * @brief
 *
 * @tparam T
 * @tparam N
 * @tparam M
 * @param file_handle
 * @param link
 * @param f
 * @param m
 * @param data
 */
template <typename T, int N, int M>
void WriteCollective(hid_t &file_handle,
                     const std::string &link,
                     Hyperslab<hsize_t, N> &f,
                     Hyperslab<hsize_t, M> &m,
                     T *data) {
  hid_t dset_file;
  hid_t dspace_file, dspace_memory;
  hid_t plist_id;

  plist_id = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
  dspace_memory = H5Screate_simple(M, m.dims, NULL);
  // Only create a data-space hyperslab if the total
  // count is non-zero
  hsize_t total_dset_count(1);
  for (int i = 0; i < M; ++i)
    total_dset_count *= m.count[i];
  if (total_dset_count != 0) {
    auto status = H5Sselect_hyperslab(
        dspace_memory, H5S_SELECT_SET, m.offset, m.stride, m.count, NULL);
    if (status < 0)
      throw std::runtime_error("Hyperslab selection failed in dataset");
  } else {
    H5Sselect_none(dspace_memory);
  }
  // Check if dataset already exists
  if (!H5Lexists(file_handle, link.c_str(), H5P_DEFAULT)) {
    dspace_file = H5Screate_simple(N, f.dims, NULL);
    dset_file = H5Dcreate(file_handle,
                          link.c_str(),
                          GetDatatype<T>(),
                          dspace_file,
                          H5P_DEFAULT,
                          H5P_DEFAULT,
                          H5P_DEFAULT);
  } else {
    // If the dataset exists open it from the file
    dset_file = H5Dopen(file_handle, link.c_str(), H5P_DEFAULT);
    dspace_file = H5Dget_space(dset_file);
  }
  // Hyperslab selection for file is non-null only if count
  // is greater than zero
  total_dset_count = 1;
  for (int i = 0; i < N; ++i)
    total_dset_count *= f.count[i];
  if (total_dset_count != 0) {
    auto status = H5Sselect_hyperslab(
        dspace_file, H5S_SELECT_SET, f.offset, f.stride, f.count, NULL);
    if (status < 0)
      throw std::runtime_error("Hyperslab selection failed in dataset");
    assert(status >= 0);
  } else {
    H5Sselect_none(dspace_file);
  }
  plist_id = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
  auto status = H5Dwrite(
      dset_file, GetDatatype<T>(), dspace_memory, dspace_file, plist_id, data);
  if (status < 0)
    throw std::runtime_error("Hyperslab selection failed in dataset");
  H5Dclose(dset_file);
  H5Sclose(dspace_file);
  H5Sclose(dspace_memory);
  H5Pclose(plist_id);
}

} // namespace hdf5
