/** @file global_timer.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 11 May 2019
 *  @brief Documentation for global_timer.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include <iostream>
#include <mpi.h>

namespace global_timer {

/**
 * The timing objects each function gets a new enum
 */
enum C_TimingObjects {
  TIME_PART_BEGIN = 0,
  TIME_HASH_CREATION,     //!< Time hash creation function
  TIME_HASH_FIND,         //!< Time hash query function
  TIME_MESH_SETUP,        //!< Time mesh setup function
  TIME_HDF5_IO,           //!< Time I/O function
  TIME_PARTITIONING,      //!< Time partitioning function
  TIME_PARTITIONING_CORE, //!< Time core partitioning function
  TIME_BOOTSTRAP,         //!< Time partitioning bootstrap
  TIME_OVERALL,           //!< Time overall application
  TIME_PART_END,
  TIME_ADAPT_BEGIN,
  TIME_ADAPT_LB,
  TIME_ADAPT_GLOBAL2LOCAL,
  TIME_ADAPT_METRIC,
  TIME_ADAPT_LIBCALL,
  TIME_ADAPT_LOCAL2GLOBAL,
  TIME_ADAPT_INTERP_STENCIL,
  TIME_ADAPT_INTERP_CHK,
  TIME_ADAPT_INTERP_METRIC,
  TIME_ADAPT_INTERP_FIELD,
  TIME_ADAPT_INTERP_ALL,
  TIME_ADAPT_CHECKPOINT_INMEM,
  TIME_ADAPT_IO,
  TIME_ADAPT_LOOP,
  TIME_ADAPT_END,
  TIME_ARRAY_SIZE
};

static double C_TimerStart = -1.0; //!< Scale factor for timer start
static double C_TimerStop = 1.0;   //!< Scale factor for timer end

static double C_GlobalTimerValues[TIME_ARRAY_SIZE] = {
    0.0}; //!< Timing values for each routine is stored here
static double C_GlobalTimerValuesMinMax[TIME_ARRAY_SIZE * 2] = {
    0.0}; //!< Min/Max Timing values for each routine is stored here

static const char *C_TimingObjectNames[] = {"TIME_PART_BEGIN",
                                            "TIME_HASH_CREATION",
                                            "TIME_HASH_FIND",
                                            "TIME_MESH_SETUP",
                                            "TIME_HDF5_IO",
                                            "TIME_PARTITIONING",
                                            "TIME_PARTITIONING_CORE",
                                            "TIME_BOOTSTRAP",
                                            "TIME_OVERALL",
                                            "TIME_PART_END",
                                            "TIME_ADAPT_BEGIN",
                                            "TIME_ADAPT_LB",
                                            "TIME_ADAPT_GLOBAL2LOCAL",
                                            "TIME_ADAPT_METRIC",
                                            "TIME_ADAPT_LIBCALL",
                                            "TIME_ADAPT_LOCAL2GLOBAL",
                                            "TIME_ADAPT_INTERP_STENCIL",
                                            "TIME_ADAPT_INTERP_CHK",
                                            "TIME_ADAPT_INTERP_METRIC",
                                            "TIME_ADAPT_INTERP_FIELD",
                                            "TIME_ADAPT_INTERP_ALL",
                                            "TIME_ADAPT_CHECKPOINT_INMEM",
                                            "TIME_ADAPT_IO",
                                            "TIME_ADAPT_LOOP",
                                            "TIME_ADAPT_END",
                                            "TIME_ARRAY_SIZE"};

/**
 * @brief Access to high precision timer
 * @return double
 */
static double GetCurrentTime() { return MPI_Wtime(); }

/**
 * @brief Print timining statistics
 * @param rank
 */
static void PrintTimingStats(int rank) {
  if (rank == 0) {
    std::cout << "Message: Tree Partition timing information\n";
    for (int i = TIME_PART_BEGIN + 1; i < TIME_PART_END; ++i)
      std::cout << C_TimingObjectNames[i] << " = " << C_GlobalTimerValues[i]
                << " (s)\n";
  }
}

/**
 * @brief Print timining adaptation statistics
 * @param rank
 */
static void PrintAdaptTimingStats(int rank) {
  if (rank == 0) {
    std::cout << "Message: Tree Adaptation timing information\n";
    for (int i = TIME_ADAPT_BEGIN + 1; i < TIME_ADAPT_END; ++i)
      std::cout << C_TimingObjectNames[i]
                << " (MIN/MAX) = " << -C_GlobalTimerValuesMinMax[i] << " / "
                << C_GlobalTimerValuesMinMax[i + TIME_ARRAY_SIZE] << " (s)\n";
  }
}

/**
 * @brief Function to start/stop timing for function
 */
static void Time(C_TimingObjects t, double start_stop) {
  C_GlobalTimerValues[t] += GetCurrentTime() * start_stop;
}

/**
 * @brief Clear all timing information
 */
static void ClearTimers() {
  for (auto &i : C_GlobalTimerValues)
    i = 0;
  for (auto &i : C_GlobalTimerValuesMinMax)
    i = 0;
}
} // namespace global_timer
