/**
 * @file mesh_interpolation.hpp
 * @author Pavanakumar Mohanamuraly (mpkumar@cerfacs.fr)
 * @brief Mesh to mesh solution interpolation routines
 *        for triangle (2D) and tet (3D) meshes
 * @copyright Copyright (c) 2022 CERFACS
 *
 */
#pragma once

#include "mesh_common.hpp"
#include "mesh_point_cloud.hpp"
#include "communicator_mpi.hpp"

#include <algorithm>
#include <cmath>
#include <limits>
#include <sstream>
#include <fstream>
#include <nanoflann.hpp>

namespace taru {

const double C_EpsilonWLS = 1e-6;

// Array assignment lambda
template <typename T_Real>
inline void assign(T_Real *const __restrict x,
                   const T_Real x1,
                   const T_Real x2,
                   const T_Real x3) {
  x[0] = x1;
  x[1] = x2;
  x[2] = x3;
};

/**
 * @brief Triangle area given three coordinates of vertices
 *
 * @tparam T_Real
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @param x3
 * @param y3
 * @return T_Real
 */
template <typename T_Real>
inline T_Real triangle_area(const T_Real &__restrict x1,
                            const T_Real &__restrict y1,
                            const T_Real &__restrict x2,
                            const T_Real &__restrict y2,
                            const T_Real &__restrict x3,
                            const T_Real &__restrict y3) {
  return (x1 - x2) * (y2 - y3) - (x2 - x3) * (y1 - y2);
}

/**
 * @brief Cross product function
 *
 * @tparam T_Real
 * @param u
 * @param v
 * @param u_x_v
 */
template <typename T_Real>
inline void cross_product(const T_Real *const __restrict u,
                          const T_Real *const __restrict v,
                          T_Real *const __restrict u_x_v) {
  auto t1 = u[0] - u[1];
  auto t2 = v[1] + v[2];
  auto t3 = u[0] * v[2];
  auto t4 = t1 * t2 - t3;
  u_x_v[0] = v[1] * (t1 - u[2]) - t4;
  u_x_v[1] = u[2] * v[0] - t3;
  u_x_v[2] = t4 - u[1] * (v[0] - t2);
}

/**
 * @brief Returns the dot product of two vectors
 *
 * @tparam T_Real
 * @param u
 * @param v
 * @return T_Real
 */
template <typename T_Real>
inline T_Real dot_product(const T_Real *const __restrict u,
                          const T_Real *const __restrict v) {
  return (u[0] * v[0] + u[1] * v[1] + u[2] * v[2]);
}

/**
 * @brief
 *
 * @tparam T_Real
 * @param u
 * @return T_Real
 */
template <typename T_Real>
inline T_Real dot_product(const T_Real *const __restrict u) {
  return (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
}

/**
 * @brief
 *
 * @param A = [ u v w ]
 * @param x is also the rhs
 * @return double
 */
template <typename T_Real>
inline void solve3x3(const T_Real *const __restrict U,
                     const T_Real *const __restrict V,
                     const T_Real *const __restrict W,
                     T_Real *const __restrict x) {
  T_Real a[3], b[3], c[3], _x[3];
  cross_product(V, W, a); // a = V \times W
  cross_product(W, U, b); // b = W \times U
  cross_product(U, V, c); // c = U \times V
  // Get the triple product (parallelepiped volume or determinant)
  const T_Real inv_vtp = 1.0 / dot_product(U, a);
  // _x as dot_product with rhs x
  for (int k = 0; k < 3; ++k)
    _x[k] = a[k] * x[k] + b[k] * x[k] + c[k] * x[k];
  for (int k = 0; k < 3; ++k)
    x[k] = _x[k] * inv_vtp;
}

/**
 * @brief Barycentric coordiates of point p on reference traingle (a,b,c)
 *
 * @tparam T_Real
 * @param a
 * @param b
 * @param c
 * @param p
 * @param u
 * @param v
 * @param w
 */
template <typename T_Real>
inline void barycentric_triangle(const T_Real *const __restrict a,
                                 const T_Real *const __restrict b,
                                 const T_Real *const __restrict c,
                                 const T_Real *const __restrict p,
                                 T_Real *__restrict u) {
  T_Real m[3], b_a[3], c_a[3];
  for (short i = 0; i < 3; ++i) {
    b_a[i] = b[i] - a[i];
    c_a[i] = c[i] - a[i];
  }
  // Non-normalised triangle normal
  cross_product(b_a, c_a, m);
  // Numeriator and inv denominator for u and v ratio
  T_Real nu, nv, ood;
  // Absolute components for determining projection plane
  T_Real x = std::abs(m[0]), y = std::abs(m[1]), z = std::abs(m[2]);
  // Compute areas in place of largest projection
  if (x >= y && x >= z) {
    // Since x is largest, project to the yz plane
    nu = triangle_area(p[1],
                       p[2],
                       b[1],
                       b[2],
                       c[1],
                       c[2]); // Area of PBC in yz plane
    nv = triangle_area(p[1],
                       p[2],
                       c[1],
                       c[2],
                       a[1],
                       a[2]); // Area of PCA in yz plane
    ood = 1.0 / m[0];         // 1/(2 * area of ABC in yz plane)
  } else if (y >= x && y >= z) {
    // Since y is largest, project to the xz plane
    nu = triangle_area(p[0],
                       p[2],
                       b[0],
                       b[2],
                       c[0],
                       c[2]); // Area of PBC in xz plane
    nv = triangle_area(p[0],
                       p[2],
                       c[0],
                       c[2],
                       a[0],
                       a[2]); // Area of PCA in xz plane
    ood = -1.0 / m[1];        // 1/(2 * area of ABC in xz plane)
  } else {
    // Since z is largest, project to the xy plane
    nu = triangle_area(p[0],
                       p[1],
                       b[0],
                       b[1],
                       c[0],
                       c[1]); // Area of PBC in xy plane
    nv = triangle_area(p[0],
                       p[1],
                       c[0],
                       c[1],
                       a[0],
                       a[1]); // Area of PCA in xy plane
    ood = 1.0 / m[2];         // 1/(2 * area of ABC in xy plane)
  }
  u[0] = nu * ood;
  u[1] = nv * ood;
  u[2] = 1.0 - u[0] - u[1];
}

/**
 * @brief Test if point p is inside the triangle (a,b,c)
 *
 * @tparam T_Real
 * @param a
 * @param b
 * @param c
 * @param p
 * @return int
 */
template <typename T_Real>
int point_in_traingle_test(const T_Real *const __restrict a,
                           const T_Real *const __restrict b,
                           const T_Real *const __restrict c,
                           const T_Real *const __restrict p,
                           T_Real *const __restrict u) {
  barycentric_triangle(a, b, c, p, u);
  return static_cast<int>(
      (u[1] >= -50.0 * std::numeric_limits<T_Real>::epsilon()) &&
      (u[2] >= -50.0 * std::numeric_limits<T_Real>::epsilon()) &&
      ((u[1] + u[2]) <= 1.0 + 100.0 * std::numeric_limits<T_Real>::epsilon()));
}

/**
 * @brief Calculate the signed distance of point p and the triangle (a,b,c)
 *
 * @tparam T_Real
 * @param p
 * @param a
 * @param b
 * @param c
 * @return T_Real
 */
template <typename T_Real>
inline T_Real signed_distance(const T_Real *const __restrict p,
                              const T_Real *const __restrict a,
                              const T_Real *const __restrict b,
                              const T_Real *const __restrict c) {
  const T_Real p_a[3] = {p[0] - a[0], p[1] - a[1], p[2] - a[2]};
  const T_Real b_a[3] = {b[0] - a[0], b[1] - a[1], b[2] - a[2]};
  const T_Real c_a[3] = {c[0] - a[0], c[1] - a[1], c[2] - a[2]};
  T_Real m[3];
  cross_product(b_a, c_a, m);
  return dot_product(p_a, m);
}

/**
 * @brief
 *
 * @tparam T_Real
 * @param p
 * @param a
 * @param b
 * @param c
 * @return int
 */
template <typename T_Real>
inline int point_outside_of_plane(const T_Real *const __restrict p,
                                  const T_Real *const __restrict a,
                                  const T_Real *const __restrict b,
                                  const T_Real *const __restrict c) {
  return signed_distance(p, a, b, c) >= 0.0; // [AP AB AC] >= 0
}

/**
 * @brief The Barycentric weights (u1,u2,u3) of point p with tetrahedron
 * (a,b,c,d)
 *
 * @tparam T_Real
 * @param a
 * @param b
 * @param c
 * @param p
 * @param u1
 * @param u2
 * @param u3
 */
template <typename T_Real>
inline void barycentric_tetrahedron(const T_Real *__restrict a,
                                    const T_Real *__restrict b,
                                    const T_Real *__restrict c,
                                    const T_Real *__restrict d,
                                    const T_Real *const __restrict p,
                                    T_Real *const __restrict u) {
  // Signed distance approach
  const T_Real sd[3] = {1.0 / signed_distance(a, b, d, c),
                        1.0 / signed_distance(b, a, c, d),
                        1.0 / signed_distance(c, a, d, b)};
  u[0] = signed_distance(p, b, d, c);
  u[1] = signed_distance(p, a, c, d);
  u[2] = signed_distance(p, a, d, b);
  for (short i = 0; i < 3; ++i)
    u[i] *= sd[i];
  // Inverse matrix approach
  // T_Real U[3] = {b[0] - a[0], c[0] - a[0], d[0] - a[0]},
  //        V[3] = {b[1] - a[1], c[1] - a[1], d[1] - a[1]},
  //        W[3] = {b[2] - a[2], c[2] - a[2], d[2] - a[2]};
  // u[0] = p[0] - a[0];
  // u[1] = p[1] - a[1];
  // u[2] = p[2] - a[2];
  // solve3x3(U, V, W, u);
  u[3] = 1.0 - u[0] - u[1] - u[2];
}

/**
 * @brief Test if point p is inside tetrahedron (a,b,c,d)
 *
 * @tparam T_Real
 * @param a
 * @param b
 * @param c
 * @param d
 * @param p
 * @return int
 */
template <typename T_Real>
int point_in_tetra_test(const T_Real *__restrict a,
                        const T_Real *__restrict b,
                        const T_Real *__restrict c,
                        const T_Real *__restrict d,
                        const T_Real *__restrict p,
                        T_Real *__restrict u) {
  barycentric_tetrahedron(a, b, c, d, p, u);
  return (u[1] >= -100.0 * std::numeric_limits<T_Real>::epsilon()) &&
         (u[2] >= -100.0 * std::numeric_limits<T_Real>::epsilon()) &&
         (u[3] >= -100.0 * std::numeric_limits<T_Real>::epsilon()) &&
         ((u[1] + u[2] + u[3]) <=
          1.0 + 300.0 * std::numeric_limits<T_Real>::epsilon());
}

/**
 * @brief
 *
 * @tparam T_Real
 * @param x
 * @param y
 * @return T_Real
 */
template <typename T_Real>
inline T_Real distance_squared(const T_Real *const __restrict x,
                               const T_Real *const __restrict y) {
  return (x[0] - y[0]) * (x[0] - y[0]) + (x[1] - y[1]) * (x[1] - y[1]) +
         (x[2] - y[2]) * (x[2] - y[2]);
}

template <typename T_Real>
inline T_Real distance_squared(const T_Real alpha,
                               const T_Real *const __restrict x1,
                               const T_Real *const __restrict x,
                               const T_Real *const __restrict y) {
  return (x[0] + alpha * x1[0] - y[0]) * (x[0] + alpha * x1[0] - y[0]) +
         (x[1] + alpha * x1[1] - y[1]) * (x[1] + alpha * x1[1] - y[1]) +
         (x[2] + alpha * x1[2] - y[2]) * (x[2] + alpha * x1[2] - y[2]);
}

template <typename T_Real>
inline T_Real distance_squared(const T_Real alpha,
                               const T_Real beta,
                               const T_Real *const __restrict x1,
                               const T_Real *const __restrict x2,
                               const T_Real *const __restrict x,
                               const T_Real *const __restrict y) {
  return (x[0] + alpha * x1[0] + beta * x1[0] - y[0]) *
             (x[0] + alpha * x1[0] + beta * x1[0] - y[0]) +
         (x[1] + alpha * x1[1] + beta * x1[1] - y[1]) *
             (x[1] + alpha * x1[1] + beta * x1[1] - y[1]) +
         (x[2] + alpha * x1[2] + beta * x1[2] - y[2]) *
             (x[2] + alpha * x1[2] + beta * x1[2] - y[2]);
}

template <typename T_Real>
inline T_Real distance_squared(const T_Real alpha,
                               const T_Real *const __restrict x1,
                               const T_Real *const __restrict x2,
                               const T_Real *const __restrict x,
                               const T_Real *const __restrict y) {
  return (x[0] + alpha * (x1[0] - x2[0]) - y[0]) *
             (x[0] + alpha * (x1[0] - x2[0]) - y[0]) +
         (x[1] + alpha * (x1[1] - x2[1]) - y[1]) *
             (x[1] + alpha * (x1[1] - x2[1]) - y[1]) +
         (x[2] + alpha * (x1[2] - x2[2]) - y[2]) *
             (x[2] + alpha * (x1[2] - x2[2]) - y[2]);
}

/**
 * @brief
 *
 * @param p
 * @param a
 * @param b
 * @param u
 * @return double
 */
template <typename T_Real>
double closest_point_to_segment(const T_Real *const __restrict p,
                                const T_Real *const __restrict a,
                                const T_Real *const __restrict b,
                                T_Real *const __restrict u) {
  T_Real ab[3] = {b[0] - a[0], b[1] - a[1], b[2] - a[2]};
  T_Real ap[3] = {p[0] - a[0], p[1] - a[1], p[2] - a[2]};
  // Project c onto ab, computing parameterized position d(t)=a+t*(b – a)
  T_Real t = dot_product(ap, ab) / dot_product(ab);
  // If outside segment, clamp t (and therefore u) to the closest endpoint
  if (t < 0.0)
    t = 0.0;
  if (t > 1.0)
    t = 1.0;
  // Compute the interpolation coeff using clamped t
  u[0] = 1.0 - t;
  u[1] = t;
  return distance_squared(t, ab, a, p); // d = (1-t) a + t * b
}

/**
 * @brief Find the closest point to the triangle to a given point p
 *        and returns the barycentric coordinate of the closes point
 * @tparam T_Real
 * @param p
 * @param a
 * @param b
 * @param c
 * @param u
 */
template <typename T_Real>
double closest_pt_to_triangle(const T_Real *const __restrict p,
                              const T_Real *const __restrict a,
                              const T_Real *const __restrict b,
                              const T_Real *const __restrict c,
                              T_Real *const __restrict u) {
  // Check if P in vertex region outside A
  const T_Real ab[3] = {b[0] - a[0], b[1] - a[1], b[2] - a[2]};
  const T_Real ac[3] = {c[0] - a[0], c[1] - a[1], c[2] - a[2]};
  const T_Real ap[3] = {p[0] - a[0], p[1] - a[1], p[2] - a[2]};
  const T_Real d1 = dot_product(ab, ap);
  const T_Real d2 = dot_product(ac, ap);
  if (d1 <= 0.0 && d2 <= 0.0) {
    // barycentric coordinates (1, 0, 0)
    assign(u, 1.0, 0.0, 0.0);
    return distance_squared(a, p);
  }
  // Check if P in vertex region outside B
  const T_Real bp[3] = {p[0] - b[0], p[1] - b[1], p[2] - b[2]};
  const T_Real d3 = dot_product(ab, bp);
  const T_Real d4 = dot_product(ac, bp);
  if (d3 >= 0.0 && d4 <= d3) {
    // barycentric coordinates (0, 1, 0)
    assign(u, 0.0, 1.0, 0.0);
    return distance_squared(b, p);
  }
  // Check if P in edge region of AB, if so return projection of P onto AB
  const T_Real vc = d1 * d4 - d3 * d2;
  if (vc <= 0.0 && d1 >= 0.0 && d3 <= 0.0) {
    const T_Real v = d1 / (d1 - d3);
    // barycentric coordinates (1-v, v, 0)
    assign(u, 1.0 - v, v, 0.0);
    return distance_squared(v, ab, a, p); // a + v * ab
  }

  // Check if P in vertex region outside C
  const T_Real cp[3] = {p[0] - c[0], p[1] - c[1], p[2] - c[2]};
  const T_Real d5 = dot_product(ab, cp);
  const T_Real d6 = dot_product(ac, cp);

  if (d6 >= 0.0 && d5 <= d6) {
    // barycentric coordinates (0, 0, 1)
    assign(u, 0.0, 0.0, 1.0);
    return distance_squared(c, p);
  }

  // Check if P in edge region of AC, if so return projection of P onto AC
  const T_Real vb = d5 * d2 - d1 * d6;
  if (vb <= 0.0 && d2 >= 0.0 && d6 <= 0.0) {
    const T_Real w = d2 / (d2 - d6);
    // barycentric coordinates (1-w, 0, w)
    assign(u, 1.0 - w, 0.0, w);
    return distance_squared(w, ac, a, p); // a + w * ac
  }

  // Check if P in edge region of BC, if so return projection of P onto BC
  T_Real va = d3 * d6 - d5 * d4;
  if (va <= 0.0 && (d4 - d3) >= 0.0 && (d5 - d6) >= 0.0) {
    const T_Real w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
    // barycentric coordinates (0, 1-w, w)
    assign(u, 0.0, 1.0 - w, w);
    return distance_squared(w, c, b, b, p); // b + w * (b - c)
  }

  // P inside face region. Compute Q through its barycentric coordinates (u,v,w)
  const T_Real denom = 1.0 / (va + vb + vc);
  const T_Real v = vb * denom;
  const T_Real w = vc * denom;
  assign(u, 1.0 - v - w, v, w); // = u*a + v*b + w*c, u = va *denom = 1.0-v-w
  return distance_squared(v, w, ab, ac, a, p); // a + ab * v + ac * w;
}

/**
 * @brief Find the closest point to the triangle to a given point p
 *        and returns the actual coordinate value of the closes point
 * @tparam T_Real
 * @param p
 * @param a
 * @param b
 * @param c
 * @param _p
 */
template <typename T_Real>
void closest_pt_to_triangle1(const T_Real *const __restrict p,
                             const T_Real *const __restrict a,
                             const T_Real *const __restrict b,
                             const T_Real *const __restrict c,
                             T_Real *const __restrict _p) {
  // Check if P in vertex region outside A
  const T_Real ab[3] = {b[0] - a[0], b[1] - a[1], b[2] - a[2]};
  const T_Real ac[3] = {c[0] - a[0], c[1] - a[1], c[2] - a[2]};
  const T_Real ap[3] = {p[0] - a[0], p[1] - a[1], p[2] - a[2]};
  const T_Real d1 = dot_product(ab, ap);
  const T_Real d2 = dot_product(ac, ap);
  if (d1 <= 0.0 && d2 <= 0.0) {
    // barycentric coordinates (1, 0, 0)
    assign(_p, a[0], a[1], a[2]);
    return;
  }
  // Check if P in vertex region outside B
  const T_Real bp[3] = {p[0] - b[0], p[1] - b[1], p[2] - b[2]};
  const T_Real d3 = dot_product(ab, bp);
  const T_Real d4 = dot_product(ac, bp);
  if (d3 >= 0.0 && d4 <= d3) {
    // barycentric coordinates (0, 1, 0)
    assign(_p, b[0], b[1], b[2]);
    return;
  }
  // Check if P in edge region of AB, if so return projection of P onto AB
  const T_Real vc = d1 * d4 - d3 * d2;
  if (vc <= 0.0 && d1 >= 0.0 && d3 <= 0.0) {
    const T_Real v = d1 / (d1 - d3);
    // barycentric coordinates (1-v, v, 0)
    assign(_p, a[0] + v * ab[0], a[1] + v * ab[1], a[2] + v * ab[2]);
    return;
  }
  // Check if P in vertex region outside C
  const T_Real cp[3] = {p[0] - c[0], p[1] - c[1], p[2] - c[2]};
  const T_Real d5 = dot_product(ab, cp);
  const T_Real d6 = dot_product(ac, cp);
  if (d6 >= 0.0 && d5 <= d6) {
    // barycentric coordinates (0, 0, 1)
    assign(_p, c[0], c[1], c[2]);
    return;
  }
  // Check if P in edge region of AC, if so return projection of P onto AC
  const T_Real vb = d5 * d2 - d1 * d6;
  if (vb <= 0.0 && d2 >= 0.0 && d6 <= 0.0) {
    const T_Real w = d2 / (d2 - d6);
    // barycentric coordinates (1-w, 0, w)
    assign(_p, a[0] + w * ac[0], a[1] + w * ac[1], a[2] + w * ac[2]);
    return;
  }
  // Check if P in edge region of BC, if so return projection of P onto BC
  T_Real va = d3 * d6 - d5 * d4;
  if (va <= 0.0 && (d4 - d3) >= 0.0 && (d5 - d6) >= 0.0) {
    const T_Real w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
    // barycentric coordinates (0, 1-w, w)
    assign(_p,
           b[0] + w * (c[0] - b[0]),
           b[1] + w * (c[1] - b[1]),
           b[2] + w * (c[2] - b[2]));
    return;
  }
  // P inside face region. Compute Q through its barycentric coordinates (u,v,w)
  const T_Real denom = 1.0 / (va + vb + vc);
  const T_Real v = vb * denom;
  const T_Real w = vc * denom;
  // = u*a + v*b + w*c, u = va * denom = 1.0-v-w
  assign(_p,
         a[0] + ab[0] * v + ac[0] * w,
         a[1] + ab[1] * v + ac[1] * w,
         a[2] + ab[2] * v + ac[2] * w);
  return;
}

/**
 * @brief
 *
 * @tparam T_Real
 * @param p
 * @param a
 * @param b
 * @param c
 * @param _p
 */
template <typename T_Real>
void closest_pt_to_tetrahedron(const T_Real *const __restrict p,
                               const T_Real *const __restrict a,
                               const T_Real *const __restrict b,
                               const T_Real *const __restrict c,
                               const T_Real *const __restrict d,
                               T_Real *const __restrict _p) {
  // Start out assuming point inside all halfspaces, so closest to itself
  T_Real closestPt[3] = {p[0], p[1], p[2]};
  T_Real q[3];
  T_Real bestSqDist = std::numeric_limits<T_Real>::max();
  // If point outside face abc then compute closest point on abc
  if (point_outside_of_plane(p, a, b, c)) {
    closest_pt_to_triangle1(p, a, b, c, q);
    const T_Real qp[3] = {q[0] - p[0], q[1] - p[1], q[2] - p[2]};
    const T_Real sqDist = dot_product(qp);
    // Update best closest point if (squared) distance is less than current best
    if (sqDist < bestSqDist) {
      bestSqDist = sqDist;
      assign(closestPt, q[0], q[1], q[2]);
    }
  }
  // Repeat test for face acd
  if (point_outside_of_plane(p, a, c, d)) {
    closest_pt_to_triangle1(p, a, c, d, q);
    const T_Real qp[3] = {q[0] - p[0], q[1] - p[1], q[2] - p[2]};
    const T_Real sqDist = dot_product(qp);
    // Update best closest point if (squared) distance is less than current best
    if (sqDist < bestSqDist) {
      bestSqDist = sqDist;
      assign(closestPt, q[0], q[1], q[2]);
    }
  }
  // Repeat test for face adb
  if (point_outside_of_plane(p, a, d, b)) {
    closest_pt_to_triangle1(p, a, d, b, q);
    const T_Real qp[3] = {q[0] - p[0], q[1] - p[1], q[2] - p[2]};
    const T_Real sqDist = dot_product(qp);
    // Update best closest point if (squared) distance is less than current best
    if (sqDist < bestSqDist) {
      bestSqDist = sqDist;
      assign(closestPt, q[0], q[1], q[2]);
    }
  }
  // Repeat test for face bdc
  if (point_outside_of_plane(p, b, d, c)) {
    closest_pt_to_triangle1(p, b, d, c, q);
    const T_Real qp[3] = {q[0] - p[0], q[1] - p[1], q[2] - p[2]};
    const T_Real sqDist = dot_product(qp);
    // Update best closest point if (squared) distance is less than current best
    if (sqDist < bestSqDist) {
      bestSqDist = sqDist;
      assign(closestPt, q[0], q[1], q[2]);
    }
  }
  assign(_p, closestPt[0], closestPt[1], closestPt[2]);
  return;
}

/**
 * @brief Find if point p lies in the AABB bounding box
 *
 * @tparam NDIM
 * @tparam T_Real
 * @param p
 * @param AABBc
 * @param AABBr
 * @return int
 */
template <int NDIM, typename T_Real>
int point_in_aabb_test(const T_Real *const __restrict p,
                       const T_Real *const __restrict AABBc,
                       const T_Real *const __restrict AABBr,
                       const T_Real tolerance = 0) {
  T_Real dr[NDIM];
  for (int k = 0; k < NDIM; ++k)
    dr[k] = std::abs(p[k] - AABBc[k]);
  if (NDIM == 2)
    return (dr[0] <= AABBr[0] + tolerance) && (dr[1] <= AABBr[1] + tolerance);
  else
    return (dr[0] <= AABBr[0] + tolerance) && (dr[1] <= AABBr[1] + tolerance) &&
           (dr[2] <= AABBr[2] + tolerance);
}

/**
 * @brief Compute the local and global bounding boxes of elements
 *
 * @tparam NDIM
 * @tparam T_Real
 * @tparam T_LID
 * @tparam T_GID
 * @param nnodes
 * @param nelms
 * @param x
 * @param conn
 * @param lAABBc
 * @param lAABBr
 * @param gAABBc
 * @param gAABBr
 */
template <int NDIM, typename T_LID, typename T_GID>
void get_aabb(const T_GID nnodes,
              const T_GID nelms,
              const double *const __restrict x,
              const T_LID *const __restrict conn,
              double *const __restrict lAABBc,
              double *const __restrict lAABBr,
              double *const __restrict gAABBc,
              double *const __restrict gAABBr) {
  double gxmin[NDIM];
  double gxmax[NDIM];
  const auto d_infty = std::numeric_limits<double>::infinity();
  // Assign good default values
  for (int k = 0; k < NDIM; ++k) {
    gxmin[k] = std::numeric_limits<double>::max();
    gxmax[k] = -std::numeric_limits<double>::max();
  }
  // Loop over all element connectivity to collect the local element AABB
  // min and max coordinates
  for (T_GID i = 0; i < nelms; ++i) {
    double xmin[NDIM];
    double xmax[NDIM];
    for (int k = 0; k < NDIM; ++k) {
      xmin[k] = std::numeric_limits<double>::max();
      xmax[k] = -std::numeric_limits<double>::max();
    }
    for (int j = 0; j < NDIM + 1; ++j) {
      const auto nid = conn[(NDIM + 1) * i + j] - 1;
      // Find the min and max of the element vertex coordinates
      for (int k = 0; k < NDIM; ++k) {
        xmin[k] = std::min(xmin[k], x[3 * nid + k]);
        xmax[k] = std::max(xmax[k], x[3 * nid + k]);
      }
    }
    ///////////////////////////////////////////////////////
    // Process the min/max to centroid and radius
    ///////////////////////////////////////////////////////
    // Update the global AABB values
    for (int k = 0; k < NDIM; ++k) {
      gxmin[k] = std::min(gxmin[k], xmin[k]);
      gxmax[k] = std::max(gxmax[k], xmax[k]);
    }
    // Find the centroid of the box and the extent
    double cx[NDIM];
    double rx[NDIM];
    for (int k = 0; k < NDIM; ++k) {
      cx[k] = (xmax[k] + xmin[k]) * 0.5;
      rx[k] = (xmax[k] - xmin[k]) * 0.5;
    }
    // Make sure we are well above the upper and lower limits
    for (int k = 0; k < NDIM; ++k) {
      const auto e1 = std::abs(cx[k] - xmax[k]);
      const auto e2 = std::abs(cx[k] - xmin[k]);
      for (;;) {
        if ((e1 <= rx[k]) && (e2 <= rx[k]))
          break;
        rx[k] = std::nextafter(rx[k], d_infty);
      }
    }
    // Assign the local AABB centroid and extent values
    for (int k = 0; k < NDIM; ++k) {
      lAABBc[i * NDIM + k] = cx[k];
      lAABBr[i * NDIM + k] = rx[k];
    }
  }
  ///////////////////////////////////////////////////////
  // Local AABB processing complete
  ///////////////////////////////////////////////////////
  double cx[NDIM];
  double rx[NDIM];
  // Now convert the global AABB using min/max
  for (int k = 0; k < NDIM; ++k) {
    cx[k] = (gxmax[k] + gxmin[k]) * 0.5;
    rx[k] = (gxmax[k] - gxmin[k]) * 0.5;
  }
  // Make sure AABB does not loose robustness
  for (int k = 0; k < NDIM; ++k) {
    const auto e1 = std::abs(cx[k] - gxmax[k]);
    const auto e2 = std::abs(cx[k] - gxmin[k]);
    for (;;) {
      if ((e1 <= rx[k]) && (e2 <= rx[k]))
        break;
      rx[k] = std::nextafter(rx[k], d_infty);
    }
  }
  // Assign the cx and rx values to global AABB
  for (int k = 0; k < NDIM; ++k) {
    gAABBc[k] = cx[k];
    gAABBr[k] = rx[k];
  }
}

/**
 * @brief Get the aabb surf object
 *
 * @tparam NDIM
 * @tparam T_LID
 * @tparam T_GID
 * @param nnodes
 * @param nelms
 * @param x
 * @param conn
 * @param lAABBc
 * @param lAABBr
 * @param gAABBc
 * @param gAABBr
 */
template <int NDIM, typename T_LID, typename T_GID>
void get_aabb_surf(const T_GID nnodes,
                   const T_GID nelms,
                   const double *const __restrict x,
                   const T_LID *const __restrict conn,
                   double *const __restrict lAABBc,
                   double *const __restrict lAABBr,
                   double *const __restrict gAABBc,
                   double *const __restrict gAABBr) {}

/**
 * @brief Get the interpolation data object
 *
 * @tparam NDIM
 * @param x
 * @param conn
 * @param p
 * @param pelm
 * @param weights
 */
template <int NDIM, typename T_Real, typename T_GID, typename T_LID>
void get_interpolation_data(const T_GID num_dst_nodes,
                            const T_GID num_src_elm,
                            const T_Real *__restrict x,
                            const T_LID *__restrict conn,
                            const T_Real *__restrict p,
                            T_LID *const __restrict pelm,
                            T_Real *const __restrict weights) {
  // Loop over all the destination points requiring interpolation
  // from the source mesh
  for (T_GID i = 0; i < num_dst_nodes; ++i) {
    // Loop over each element and check if destination point
    // is contained inside this element
    for (T_GID j = 0; j < num_src_elm; ++j) {
      int is_in_elm = 0;
      T_Real u[4];
      T_LID inod[4] = {conn[(NDIM + 1) * j] - 1,
                       conn[(NDIM + 1) * j + 1] - 1,
                       conn[(NDIM + 1) * j + 2] - 1,
                       0};
      if (NDIM == 3)
        inod[NDIM] = conn[(NDIM + 1) * j + 3] - 1;

      if (NDIM == 2)
        is_in_elm = point_in_traingle_test(&(x[3 * inod[0]]),
                                           &(x[3 * inod[1]]),
                                           &(x[3 * inod[2]]),
                                           p + 3 * i,
                                           u);
      else
        is_in_elm = point_in_tetra_test(&(x[3 * inod[0]]),
                                        &(x[3 * inod[1]]),
                                        &(x[3 * inod[2]]),
                                        &(x[3 * inod[3]]),
                                        p + 3 * i,
                                        u);

      // If element was found add this to the interpolation data
      // and break out of the element loop
      if (is_in_elm) {
        pelm[i] = j + 1;
        for (int k = 0; k < NDIM + 1; ++k)
          weights[i * (NDIM + 1) + k] = u[k];
        break;
      }
    }
  }
}

/**
 * @brief Get the interpolation data object (using local
 *        and global AABB data)
 *
 * @tparam NDIM
 * @tparam T_Real
 * @tparam T_GID
 * @tparam T_LID
 * @param num_dst_nodes
 * @param num_src_elm
 * @param x
 * @param conn
 * @param AABBc
 * @param AABBr
 * @param p
 * @param pelm
 * @param weights
 */
template <int NDIM, typename T_Real, typename T_GID, typename T_LID>
void get_interpolation_data(const T_GID num_dst_nodes,
                            const T_GID num_src_elm,
                            const T_Real *__restrict x,
                            const T_LID *__restrict conn,
                            const T_Real *__restrict lAABBc,
                            const T_Real *__restrict lAABBr,
                            const T_Real *__restrict gAABBc,
                            const T_Real *__restrict gAABBr,
                            const T_Real *__restrict p,
                            T_LID *const __restrict pelm,
                            T_Real *const __restrict weights) {
  // Loop over all the destination points requiring interpolation
  // from the source mesh
  for (T_GID i = 0; i < num_dst_nodes; ++i) {
    const auto *const my_p = p + 3 * i;
    // First check if this node is inside the global AABB of
    // this mesh otherwise tag this as nodes that need to be
    // projected to the surface mesh of the source mesh and
    // then obtain the interpolation weights
    if (point_in_aabb_test<NDIM>(my_p, gAABBc, gAABBr)) {
      [&] { // Lambda function that checks for point in element
        // Loop over each element
        for (T_GID j = 0; j < num_src_elm; ++j) {
          // Check if destination point is contained
          // inside this element AABB
          if (point_in_aabb_test<NDIM>(
                  my_p, lAABBc + NDIM * j, lAABBr + NDIM * j)) {
            int is_in_elm = 0;
            T_Real u[4];
            T_LID inod[4] = {conn[(NDIM + 1) * j] - 1,
                             conn[(NDIM + 1) * j + 1] - 1,
                             conn[(NDIM + 1) * j + 2] - 1,
                             0};
            if (NDIM == 3)
              inod[NDIM] = conn[(NDIM + 1) * j + 3] - 1;

            if (NDIM == 2)
              is_in_elm = point_in_traingle_test(&(x[3 * inod[0]]),
                                                 &(x[3 * inod[1]]),
                                                 &(x[3 * inod[2]]),
                                                 p + 3 * i,
                                                 u);
            else
              is_in_elm = point_in_tetra_test(&(x[3 * inod[0]]),
                                              &(x[3 * inod[1]]),
                                              &(x[3 * inod[2]]),
                                              &(x[3 * inod[3]]),
                                              p + 3 * i,
                                              u);

            // If element was found add this to the interpolation data
            // and break out of the element loop
            if (is_in_elm) {
              pelm[i] = j + 1;
              for (int k = 0; k < NDIM + 1; ++k)
                weights[i * (NDIM + 1) + k] = u[k];
              return;
            }
          } // Local AABB check ends
        } // Source element loop
      }(); // End of lambda
    } // Global AABB check ends
  } // Loop over destination nodes
}

/**
 * @brief Interpolates the source values to the destination values using the
 * weights and element info provided by the GetInterpolationWeights function.
 * The source values are interpolated to the destination values using the
 * barycentric interpolation method.
 *
 * @tparam N_DIM The dimension of the mesh.
 * @param source_value The source values to be interpolated.
 * @param source_conn The connectivity of the source mesh.
 * @param dest_eid The element id of the destination mesh.
 * @param dest_wgt The weights of the destination mesh.
 * @param dest_value The interpolated values.
 */
template <int N_DIM>
void BarycentricInterpolate(const std::vector<double> &source_value,
                            const std::vector<id_size_t> &source_conn,
                            const std::vector<id_size_t> &dest_eid,
                            const std::vector<double> &dest_wgt,
                            std::vector<double> &dest_value) {
  const my_size_t npts = dest_value.size();
  dest_value.resize(npts, 0.0);
  // Now interpolate using the weights and element info
  for (my_size_t i = 0; i < npts; ++i) {
    // Only interpolation things that have found a donor element
    if (dest_eid[i] > 0) {
      const auto ie = dest_eid[i] - 1;
      dest_value[i] = 0.0;
      // For all j vertices of the element id = ie
      for (int j = 0; j < (N_DIM + 1); ++j) {
        const auto inode = source_conn[ie * (N_DIM + 1) + j] - 1;
        dest_value[i] += source_value[inode] * dest_wgt[i * (N_DIM + 1) + j];
      }
    } else {
      throw std::runtime_error(
          "Adapted mesh node has no interpolation neighbour");
    }
  }
}

/**
 * @brief Get the interpolation weights for the destination mesh nodes using the
 * source mesh nodes and boundary information. The interpolation weights are
 * calculated using the bounding box of the source mesh and the element
 * information of the source mesh.
 * @tparam N_DIM The dimension of the mesh.
 * @param source_coord The coordinates of the source mesh nodes.
 * @param source_conn The connectivity of the source mesh.
 * @param source_belm The boundary elements of the source mesh.
 * @param source_bfaceno The boundary face numbers of the source mesh.
 * @param dest_coord The coordinates of the destination mesh nodes.
 * @param dest_eid The element id of the destination mesh.
 * @param dest_wgt The weights of the destination mesh.
 * @param comm The CommunicatorConcept object for communication.
 */
template <int N_DIM>
void GetInterpolationWeights(const std::vector<Point3d> &source_coord,
                             const std::vector<id_size_t> &source_conn,
                             const std::vector<id_size_t> &source_bnodes,
                             const std::vector<id_size_t> &source_belm,
                             const std::vector<int> &source_bfaceno,
                             const std::vector<std::set<int>> &adapt_binfo,
                             const std::vector<Point3d> &dest_coord,
                             std::vector<id_size_t> &dest_eid,
                             std::vector<double> &dest_wgt,
                             CommunicatorConcept &comm) {
  dest_wgt.resize(dest_coord.size() * (N_DIM + 1), 0);
  dest_eid.resize(dest_coord.size(), 0);
#if 1
  // Shared nodes get assigned interpolation weights of nearest neighbour first
  {
    assert(adapt_binfo.size() == dest_coord.size());
    // Create the shared node info
    std::set<id_size_t> shared_nodes;
    for (decltype(adapt_binfo.size()) i = 0; i < adapt_binfo.size(); ++i)
      for (const auto &iref : adapt_binfo[i])
        if (iref < 0)
          shared_nodes.insert(i);

    // std::stringstream cat;
    // cat << "shared_" << comm.Rank() << ".dat";
    // std::ofstream fout(cat.str());
    // cat.str("");
    // cat << "shared_nodes_" << comm.Rank() << ".txt";
    // std::ofstream fout1(cat.str());
    // fout << "VARIABLES=\"X\", \"Y\", \"Z\", \"WGT\"\n ";
    // fout1 << "\"X\", \"Y\", \"Z\"\n ";
    // fout << "ZONE DATAPACKING=POINT, NODES="
    //      << shared_nodes.size() * (N_DIM + 1)
    //      << ", ELEMENTS=" << shared_nodes.size();
    // if (N_DIM == 2)
    //   fout << ", ZONETYPE=FETRIANGLE\n";
    // else
    //   fout << ", ZONETYPE=FETETRAHEDRON\n";

    // Build the KDTree using the source boundary nodes (also includes shared
    // nodes)
    BoundaryPointCloud donor_cloud(source_coord, source_bnodes);
    nanoflann::KDTreeSingleIndexAdaptor<
        nanoflann::L2_Simple_Adaptor<double, BoundaryPointCloud>,
        BoundaryPointCloud,
        N_DIM,
        size_t>
        index(
            N_DIM, donor_cloud, nanoflann::KDTreeSingleIndexAdaptorParams(30));
    index.buildIndex();

    // std::cout << "shared_nodes.size() = " << shared_nodes.size() << "\n";
    // Search for the nearest source node to the destination shared node
    for (const auto &i : shared_nodes) {
      size_t ret_index;
      double out_dist_sqr;
      // Find the approximate nearest neighbour
      auto num_results =
          index.knnSearch(&(dest_coord[i].x), 1, &ret_index, &out_dist_sqr);
      if (!num_results)
        throw std::runtime_error(
            "KDTree search failed to locate nearest neighbour");

      // Range search using the radius of the approximate closest point
      std::vector<nanoflann::ResultItem<size_t, double>> ret_matches;
      nanoflann::SearchParameters params;
      params.sorted = true;
      num_results = index.radiusSearch(
          &(dest_coord[i].x),
          out_dist_sqr + std::numeric_limits<double>::epsilon(),
          ret_matches,
          params);

      // Convert boundary local index to global node index
      ret_index = source_bnodes[ret_matches[0].first];
      // fout1 << source_coord[ret_index - 1].x << ", "
      //       << source_coord[ret_index - 1].y << ", "
      //       << source_coord[ret_index - 1].z << "\n";
      // fout1 << dest_coord[i].x << ", " << dest_coord[i].y << ", "
      //       << dest_coord[i].z << "\n";
      // Search within connectivity array to locate an arbitrary element
      // containing this element in its connectivity
      auto it = std::find(source_conn.begin(), source_conn.end(), ret_index);
      auto offset = std::distance(source_conn.begin(), it);
      id_size_t eid = offset / (N_DIM + 1);
      // Get the relative position of this node in the element
      id_size_t npos = offset - eid * (N_DIM + 1);
      // Now make this node weight at this position to 1
      dest_wgt[i * (N_DIM + 1) + npos] = 1;
      dest_eid[i] = eid + 1;
      // for (int k = 0; k < N_DIM + 1; ++k) {
      //   auto nid = source_conn[eid * (N_DIM + 1) + k] - 1;
      //   fout << source_coord[nid].x << " " << source_coord[nid].y << " "
      //        << source_coord[nid].z << " " << dest_wgt[i * (N_DIM + 1) + k]
      //        << "\n";
      // }
    }
    // for (my_size_t i = 0; i < shared_nodes.size(); ++i) {
    //   for (int k = 0; k < N_DIM + 1; ++k)
    //     fout << i * (N_DIM + 1) + k + 1 << " ";
    //   fout << "\n";
    // }
    // fout.close();
  }
#endif
  // Internal node interpolation
  {
#if 0
    {
      std::stringstream cat;
      cat << "mesh_" << comm.Rank() << ".dat";
      std::ofstream fout(cat.str());
      fout << "VARIABLES=\"X\", \"Y\", \"Z\" \n";
      fout << "ZONE DATAPACKING=POINT, NODES=" << source_coord.size()
          << ", ELEMENTS=" << source_conn.size();
      if(N_DIM == 2)
        fout << ", ZONETYPE=FETRIANGLE\n";
      else
        fout << ", ZONETYPE=FETETRAHEDRON\n";

      for (std::size_t i = 0; i < source_coord.size(); ++i)
        fout << source_coord[i].x << " " << source_coord[i].y
                  << " " << source_coord[i].z << "\n";
      for (std::size_t i = 0; i < source_conn.size() / (N_DIM + 1); ++i) {
        if( N_DIM == 2 ) {
          fout << source_conn[i * (N_DIM + 1) + 0] << " "
               << source_conn[i * (N_DIM + 1) + 1] << " "
               << source_conn[i * (N_DIM + 1) + 2] << "\n";
        }
        else {
          fout << source_conn[i * (N_DIM + 1) + 0] << " "
               << source_conn[i * (N_DIM + 1) + 1] << " "
               << source_conn[i * (N_DIM + 1) + 2] << " "
               << source_conn[i * (N_DIM + 1) + 3] << "\n";
        }
      }
    }
#endif
    // Obtain the AABB for the source mesh
    std::vector<double> lAABBc((source_conn.size() / (N_DIM + 1)) * N_DIM);
    std::vector<double> lAABBr((source_conn.size() / (N_DIM + 1)) * N_DIM);
    double gAABBc[N_DIM];
    double gAABBr[N_DIM];
    get_aabb<N_DIM>(source_coord.size(),
                    source_conn.size() / (N_DIM + 1),
                    reinterpret_cast<const double *>(source_coord.data()),
                    source_conn.data(),
                    lAABBc.data(),
                    lAABBr.data(),
                    gAABBc,
                    gAABBr);
    for (decltype(dest_coord.size()) i = 0; i < dest_coord.size(); ++i) {
      if (dest_eid[i] == 0) {
        // Now obtain the interpolation weights using the AABB and element info
        get_interpolation_data<N_DIM>(
            static_cast<decltype(source_conn.size())>(1),
            source_conn.size() / (N_DIM + 1),
            reinterpret_cast<const double *>(source_coord.data()),
            source_conn.data(),
            lAABBc.data(),
            lAABBr.data(),
            gAABBc,
            gAABBr,
            reinterpret_cast<const double *>(&(dest_coord[i].x)),
            &dest_eid[i],
            &dest_wgt[i * (N_DIM + 1)]);
      }
    }
#if 0
    {
      // Test if the boundary data supplied is faithful
      std::stringstream cat;
      cat << "boundary_" << comm.Rank() << ".dat";
      std::ofstream fout(cat.str());
      fout << "VARIABLES=\"X\", \"Y\", \"Z\" \n";
      fout << "ZONE DATAPACKING=POINT, NODES=" << source_coord.size()
          << ", ELEMENTS=" << source_belm.size();
      if(N_DIM == 2)
        fout << ", ZONETYPE=FELINESEG \n";
      else
        fout << ", ZONETYPE=FETRIANGLE\n";

      for (std::size_t i = 0; i < source_coord.size(); ++i)
        fout << source_coord[i].x << " " << source_coord[i].y
                  << " " << source_coord[i].z << "\n";
      for (std::size_t i = 0; i < source_belm.size(); ++i) {
        const auto belm = source_belm[i] - 1;
        if( N_DIM == 2 ) {
          const auto fpos = C_BiElemFaceIndex[source_bfaceno[i] - 1];
          fout << source_conn[belm * (N_DIM + 1) + fpos[0]] << " "
              << source_conn[belm * (N_DIM + 1) + fpos[1]] << "\n";
        }
        else {
          const auto fpos = C_TriElemFaceIndex[source_bfaceno[i] - 1];
          fout << source_conn[belm * (N_DIM + 1) + fpos[0]] << " "
              << source_conn[belm * (N_DIM + 1) + fpos[1]] << " "
              << source_conn[belm * (N_DIM + 1) + fpos[2]] << "\n";
        }
      }
    }
#endif
  }
#if 1
  // TODO: Weights for boundary nodes outside the source domain
  {
    // std::stringstream cat;
    // cat << "boundar_nodes_" << comm.Rank() << ".txt";
    // std::ofstream fout1(cat.str());
    // fout1 << "\"X\", \"Y\", \"Z\"\n ";
    // First find an approximate nearest neighbour
    for (decltype(dest_coord.size()) i = 0; i < dest_coord.size(); ++i) {
      if (dest_eid[i] == 0) {
        // fout1 << dest_coord[i].x << ", " << dest_coord[i].y << ", " <<
        // dest_coord[i].z << "\n";
        double closest_distance = std::numeric_limits<double>::max();
        // Find the closest point to the boundary element
        for (decltype(source_belm.size()) j = 0; j < source_belm.size(); ++j) {
          double check_distance;
          double u[4] = {0.0, 0.0, 0.0, 0.0};
          const auto belm = source_belm[j] - 1;
          if (N_DIM == 2) {
            // Get the two nodes forming the boundary segment
            const auto fpos = C_BiElemFaceIndex[source_bfaceno[j] - 1];
            const id_size_t fnid[] = {
                source_conn[belm * (N_DIM + 1) + fpos[0]] - 1,
                source_conn[belm * (N_DIM + 1) + fpos[1]] - 1};

            double _u[N_DIM];
            check_distance = closest_point_to_segment(
                reinterpret_cast<const double *>(&(dest_coord[i].x)),
                reinterpret_cast<const double *>(&(source_coord[fnid[0]].x)),
                reinterpret_cast<const double *>(&(source_coord[fnid[1]].x)),
                _u);
            u[fpos[0]] = _u[0];
            u[fpos[1]] = _u[1];
          } else {
            // Get the three nodes forming the boundary triangle
            const auto fpos = C_TriElemFaceIndex[source_bfaceno[j] - 1];
            const id_size_t fnid[] = {
                source_conn[belm * (N_DIM + 1) + fpos[0]] - 1,
                source_conn[belm * (N_DIM + 1) + fpos[1]] - 1,
                source_conn[belm * (N_DIM + 1) + fpos[2]] - 1};
            double _u[N_DIM];
            check_distance = closest_pt_to_triangle(
                reinterpret_cast<const double *>(&(dest_coord[i].x)),
                reinterpret_cast<const double *>(&(source_coord[fnid[0]].x)),
                reinterpret_cast<const double *>(&(source_coord[fnid[1]].x)),
                reinterpret_cast<const double *>(&(source_coord[fnid[2]].x)),
                _u);
            u[fpos[0]] = _u[0];
            u[fpos[1]] = _u[1];
            u[fpos[2]] = _u[2];
          }
          // If the closest point is less than the current then reassign wgt and
          // eid
          if (check_distance < closest_distance) {
            closest_distance = check_distance;
            dest_eid[i] = belm + 1;
            for (int k = 0; k < (N_DIM + 1); ++k)
              dest_wgt[i * (N_DIM + 1) + k] = u[k];
          }
        }
      }
    }
  }
#endif
#if 0
  // Check if all nodes are found inside the AABB
  {
    // Obtain the AABB for the source mesh
    std::vector<double> lAABBc((source_conn.size() / (N_DIM + 1)) * N_DIM);
    std::vector<double> lAABBr((source_conn.size() / (N_DIM + 1)) * N_DIM);
    double gAABBc[N_DIM];
    double gAABBr[N_DIM];
    get_aabb<N_DIM>(source_coord.size(), source_conn.size() / (N_DIM + 1),
                    reinterpret_cast<const double *>(source_coord.data()),
                    source_conn.data(), lAABBc.data(), lAABBr.data(), gAABBc,
                    gAABBr);
    bool error = false;
    for (decltype(dest_coord.size()) i = 0; i < dest_coord.size(); ++i) {
      if( dest_eid[i] != 0 ) {
        auto j = dest_eid[i] - 1;
        auto my_p = reinterpret_cast<const double *>(&(dest_coord[i].x));
        if(!point_in_aabb_test<N_DIM>(my_p, lAABBc.data() + N_DIM * j,
                                      lAABBr.data() + N_DIM * j), 1.0e-3) {
          std::cout << "point = " << i << " box " << j << " otuside box\n";
          error = true;
        }
      }
    }
    comm.Barrier();
    if( error )
      throw std::runtime_error("destination element not in bounding box");
  }
#endif
}

/**
 * @brief Interpolates the source values to the destination values using the
 * weights and element info provided by the GetInterpolationWeights function.
 * The source values are interpolated to the destination values using the
 * barycentric interpolation method.
 *
 * @tparam N_DIM The dimension of the mesh.
 * @param source_value The source values to be interpolated.
 * @param source_conn The connectivity of the source mesh.
 * @param dest_eid The element id of the destination mesh.
 * @param dest_wgt The weights of the destination mesh.
 * @param dest_value The interpolated values.
 */
template <int N_DIM>
double BarycentricInterpolateTest(const std::vector<Point3d> &source_coord,
                                  const std::vector<id_size_t> &source_conn,
                                  const std::vector<Point3d> &dest_coord,
                                  const std::vector<id_size_t> &dest_eid,
                                  const std::vector<double> &dest_wgt,
                                  std::vector<double> &error,
                                  CommunicatorConcept &comm) {
  // Cache size in variables
  const my_size_t nnodes = source_coord.size();
  const my_size_t npts = dest_coord.size();
  error.resize(npts, 0);
  const double *x = reinterpret_cast<const double *>(source_coord.data());
  const double *p = reinterpret_cast<const double *>(dest_coord.data());
  // Create a linear source field
  std::vector<double> fx_linear(nnodes, 0.0);
  // Obtain the min.max of function from donor
  auto f_min = std::numeric_limits<double>::max();
  auto f_max = std::numeric_limits<double>::min();
  for (my_size_t i = 0; i < nnodes; ++i) {
    for (int k = 0; k < N_DIM; ++k)
      fx_linear[i] += x[3 * i + k]; // f = x + y + z
    f_min = std::min(f_min, fx_linear[i]);
    f_max = std::max(f_max, fx_linear[i]);
  }

  std::vector<double> fp_interp(npts, 0.0);
  std::vector<double> fp_exact(npts, 0.0);
  for (my_size_t i = 0; i < npts; ++i)
    for (int k = 0; k < N_DIM; ++k)
      fp_exact[i] += p[3 * i + k]; // x + y + z

  auto max_error = std::numeric_limits<double>::min();
  // Now interpolate using the weights and element info
  for (my_size_t i = 0; i < npts; ++i) {
    // Only interpolation things that have found a donor element
    if (dest_eid[i] > 0) {
      const auto ie = dest_eid[i] - 1;
      fp_interp[i] = 0.0;
      // For all j vertices of the element id = ie
      for (int j = 0; j < (N_DIM + 1); ++j) {
        const auto inode = source_conn[ie * (N_DIM + 1) + j] - 1;
        fp_interp[i] += fx_linear[inode] * dest_wgt[i * (N_DIM + 1) + j];
      }
      // Compare the exact and interpolated results
      error[i] = std::abs(fp_exact[i] - fp_interp[i]);
      max_error = std::max(error[i], max_error);
    }
  }
  auto scale = std::abs(1.0 / (f_max - f_min));
  max_error = max_error * scale;
  // std::cout << "Max error = " << max_error << "\n";

  // Debug printing only if missing nodes were found
  int count_missing = 0;
  for (my_size_t i = 0; i < npts; ++i) {
    // Points that do have any neighbour information
    if (dest_eid[i] == 0)
      count_missing++;
  }
  if (count_missing > 0) {
    std::stringstream cat;
    cat << "missing_" << comm.Rank() << ".txt";
    std::ofstream fout(cat.str());
    fout << "\"NID\", \"X\", \"Y\", \"Z\"\n";
    for (my_size_t i = 0; i < npts; ++i) {
      // Points that do have any neighbour information
      if (dest_eid[i] == 0)
        fout << i + 1 << ", " << dest_coord[i].x << ", " << dest_coord[i].y
             << ", " << dest_coord[i].z << "\n";
    }
    fout.close();
  }

  // Debug print max error nodes only if present
  int count_max_error_nodes = 0;
  for (my_size_t i = 0; i < npts; ++i) {
    if (error[i] * scale > 1.0e-7)
      count_max_error_nodes++;
  }
  if (count_max_error_nodes > 0) {
    std::stringstream cat;
    cat << "max_error_" << comm.Rank() << ".txt";
    std::ofstream fout(cat.str());
    fout << "\"NID\", \"X\", \"Y\", \"Z\", \"ERROR\" \n";
    for (my_size_t i = 0; i < npts; ++i) {
      if (error[i] * scale > 1.0e-7)
        fout << i + 1 << ", " << dest_coord[i].x << ", " << dest_coord[i].y
             << ", " << dest_coord[i].z << ", " << error[i] * scale << "\n";
    }
    fout.close();
    // Find the location of the node that has the maximum error
    // and plot its location and containing element with weights
    cat.str("");
    cat << "max_error_elm_" << comm.Rank() << ".dat";
    fout.open(cat.str());
    fout << "VARIABLES=\"X\", \"Y\", \"Z\", \"WGT\"\n ";
    fout << "ZONE DATAPACKING=POINT, NODES="
         << count_max_error_nodes * (N_DIM + 1)
         << ", ELEMENTS=" << count_max_error_nodes;
    if (N_DIM == 2)
      fout << ", ZONETYPE=FETRIANGLE\n";
    else
      fout << ", ZONETYPE=FETETRAHEDRON\n";
    for (my_size_t i = 0; i < npts; ++i) {
      if (error[i] * scale > 1.0e-7) {
        for (int k = 0; k < N_DIM + 1; ++k) {
          fout
              << source_coord[source_conn[(dest_eid[i] - 1) * (N_DIM + 1) + k] -
                              1]
                     .x
              << " "
              << source_coord[source_conn[(dest_eid[i] - 1) * (N_DIM + 1) + k] -
                              1]
                     .y
              << " "
              << source_coord[source_conn[(dest_eid[i] - 1) * (N_DIM + 1) + k] -
                              1]
                     .z
              << " " << dest_wgt[i * (N_DIM + 1) + k] << "\n";
        }
      }
    }
    // Write element info
    size_t my_count = 0;
    for (my_size_t i = 0; i < npts; ++i) {
      if (error[i] * scale > 1.0e-7) {
        for (int k = 0; k < N_DIM + 1; ++k)
          fout << my_count * (N_DIM + 1) + k + 1 << " ";
        fout << "\n";
        my_count++;
      }
    }
    fout.close();
  }
  return max_error;
}

/**
 * @brief Prints a 3D WLS (Weighted Least Squares) matrix to the specified
 * stream.
 *
 * This function takes a pointer to a 16-element array representing a 4x4 matrix
 * and prints its elements to the specified stream. The matrix is printed row by
 * row, with each element separated by a space. After each row, a newline
 * character is printed.
 *
 * @param mat The pointer to the 16-element array representing the 4x4 matrix.
 * @param stream The stream to which the matrix elements will be printed.
 *
 * @tparam T_Stream The type of the stream object.
 */
template <typename T_Stream>
void Print3DWLSMatrix(const double *mat, T_Stream &stream) {
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      stream << mat[i * 4 + j] << " ";
    }
    stream << "\n";
  }
}

/**
 * Prints a 2D matrix of type double to the specified stream.
 *
 * @param mat The input matrix to be printed.
 * @param stream The output stream to which the matrix will be printed.
 */
template <typename T_Stream>
void Print2DWLSMatrix(const double *mat, T_Stream &stream) {
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 3; ++j) {
      stream << mat[i * 3 + j] << " ";
    }
    stream << "\n";
  }
}

/**
 * @brief Performs weighted least squares regression in 3D space.
 *
 * This function calculates the weighted least squares regression in 3D space
 * using the provided inputs. It takes a list of neighboring indices, a
 * destination coordinate, a list of source coordinates, and a list of source
 * values. The function forms a least-squares system and solves it to obtain the
 * regression result.
 *
 * @param ng_list The list of neighboring indices.
 * @param dest_coord The destination coordinate.
 * @param source_coord The list of source coordinates.
 * @param source_value The list of source values.
 * @return The result of the weighted least squares regression.
 */
inline double WLSRegression3D(const std::vector<id_size_t> &ng_list,
                              const Point3d &dest_coord,
                              const std::vector<Point3d> &source_coord,
                              const std::vector<double> &source_value) {
  double val[16] = {0}; // 4x4 LS matrix (normal form)
  double term[4] = {0};
  // Form the least-squares system (normal form)
  for (const auto &inbr : ng_list) {
    Point3d dr = dest_coord;
    dr -= source_coord[inbr];
    const double wgt = 1.0 / (C_EpsilonWLS + Point3d::normSq(dr));
    // const double wgt = 1.0;
    const double fun_wgt = wgt * source_value[inbr];
    val[0 * 4 + 0] = val[0 * 4 + 0] + wgt;
    val[1 * 4 + 0] = val[1 * 4 + 0] + dr.x * wgt;
    val[1 * 4 + 1] = val[1 * 4 + 1] + dr.x * dr.x * wgt;
    val[2 * 4 + 0] = val[2 * 4 + 0] + dr.y * wgt;
    val[2 * 4 + 1] = val[2 * 4 + 1] + dr.y * dr.x * wgt;
    val[2 * 4 + 2] = val[2 * 4 + 2] + dr.y * dr.y * wgt;
    val[3 * 4 + 0] = val[3 * 4 + 0] + dr.z * wgt;
    val[3 * 4 + 1] = val[3 * 4 + 1] + dr.z * dr.x * wgt;
    val[3 * 4 + 2] = val[3 * 4 + 2] + dr.z * dr.y * wgt;
    val[3 * 4 + 3] = val[3 * 4 + 3] + dr.z * dr.z * wgt;
    term[0] += fun_wgt;
    term[1] += dr.x * fun_wgt;
    term[2] += dr.y * fun_wgt;
    term[3] += dr.z * fun_wgt;
  }
  // Fill in the symm-mirror terms
  val[0 * 4 + 1] = val[1 * 4 + 0];
  val[0 * 4 + 2] = val[2 * 4 + 0];
  val[1 * 4 + 2] = val[2 * 4 + 1];
  val[0 * 4 + 3] = val[3 * 4 + 0];
  val[1 * 4 + 3] = val[3 * 4 + 1];
  val[2 * 4 + 3] = val[3 * 4 + 2];
  auto denom = det4x4(val);
  val[0 * 4 + 0] = term[0];
  val[1 * 4 + 0] = term[1];
  val[2 * 4 + 0] = term[2];
  val[3 * 4 + 0] = term[3];
  return det4x4(val) / denom;
}

/**
 * @brief Performs a weighted least-squares regression in 2D.
 *
 * This function calculates the weighted least-squares regression in 2D using a
 * given set of neighboring points. It takes the destination coordinate, source
 * coordinates, and source values as input and returns the regression result.
 *
 * @param ng_list The list of neighboring points.
 * @param dest_coord The destination coordinate.
 * @param source_coord The source coordinates.
 * @param source_value The source values.
 * @return The result of the weighted least-squares regression in 2D.
 */
inline double WLSRegression2D(const std::vector<id_size_t> &ng_list,
                              const Point3d &dest_coord,
                              const std::vector<Point3d> &source_coord,
                              const std::vector<double> &source_value) {
  double val[9] = {0}; // 3x3 LS matrix (normal form)
  double term[3] = {0};
  // Form the least-squares system (normal form)
  for (const auto &inbr : ng_list) {
    Point3d dr = dest_coord;
    dr -= source_coord[inbr];
    const double wgt = 1.0 / (C_EpsilonWLS + Point3d::normSq(dr));
    // const double wgt = 1.0;
    const double fun_wgt = wgt * source_value[inbr];
    val[0 * 3 + 0] = val[0 * 3 + 0] + wgt;
    val[1 * 3 + 0] = val[1 * 3 + 0] + dr.x * wgt;
    val[1 * 3 + 1] = val[1 * 3 + 1] + dr.x * dr.x * wgt;
    val[2 * 3 + 0] = val[2 * 3 + 0] + dr.y * wgt;
    val[2 * 3 + 1] = val[2 * 3 + 1] + dr.y * dr.x * wgt;
    val[2 * 3 + 2] = val[2 * 3 + 2] + dr.y * dr.y * wgt;
    term[0] += fun_wgt;
    term[1] += dr.x * fun_wgt;
    term[2] += dr.y * fun_wgt;
  }
  // Fill in the symm-mirror terms
  val[0 * 3 + 1] = val[1 * 3 + 0];
  val[0 * 3 + 2] = val[2 * 3 + 0];
  val[1 * 3 + 2] = val[2 * 3 + 1];
  auto denom = det3x3(val);
  val[0 * 3 + 0] = term[0];
  val[1 * 3 + 0] = term[1];
  val[2 * 3 + 0] = term[2];
  return det3x3(val) / denom;
}

/**
 * @brief Performs Weighted Least Squares (WLS) regression interpolation.
 *
 * This function calculates the interpolated value at a given destination
 * coordinate using the Weighted Least Squares (WLS) regression method. It takes
 * a list of nearest neighbor nodes, their corresponding source coordinates, and
 * source values as input.
 *
 * @tparam N_DIM The number of dimensions for the interpolation (2 or 3).
 * @param ng_list The list of nearest neighbor nodes.
 * @param dest_coord The destination coordinate for interpolation.
 * @param source_coord The list of source coordinates.
 * @param source_value The list of source values.
 * @return The interpolated value at the destination coordinate.
 */
template <int N_DIM>
double WLSRegression(const std::vector<id_size_t> &ng_list,
                     const Point3d &dest_coord,
                     const std::vector<Point3d> &source_coord,
                     const std::vector<double> &source_value) {
  double result = 0.0;
  bool use_averaging = false;
  size_t min_ls_nodes = (N_DIM == 2) ? 4 : 5;
  // When you have a proper stencil do the WLS interpolation
  if (ng_list.size() >= min_ls_nodes) {
    if (N_DIM == 2)
      result = WLSRegression2D(ng_list, dest_coord, source_coord, source_value);
    else
      result = WLSRegression3D(ng_list, dest_coord, source_coord, source_value);
  }
  // If the interpolated value is infinity or Nan then assign the
  // inv-distance weighted average value
  if (std::isinf(result) || std::isnan(result)) {
    use_averaging = true;
    std::cout << "Warning: Invalid WLS interpol value = " << result
              << "(default to averaging) \n";
  }
  // We have a deficient stencil or the source/dest node are at same
  // location we resort to nearest neighbour inv-distance weighted average
  if (ng_list.size() < min_ls_nodes) {
    use_averaging = true;
    // std::cout << "Warning: Defaulting to wgt-avg in point "
    //           << dest_coord.x << ", " << dest_coord.y << ", "
    //           << dest_coord.z << " due to deficient stencil.\n";
  }
  // If interpolated value is not bounded by the stencil min/max
  // we resort to a inv-distance weighted interpolation
  double min_value = source_value[ng_list[0]],
         max_value = source_value[ng_list[0]];
  if (!use_averaging) {
    for (const auto &item : ng_list) {
      min_value = std::min(source_value[item], min_value);
      max_value = std::max(source_value[item], max_value);
    }
    if (result > max_value || result < min_value) {
      use_averaging = true;
      // std::cout << "Warning: Min/max failed " << min_value << " ~< " <<
      // result
      //         << " ~< " << max_value << " (default to averaging)\n";
    }
  }
  // Now check if use_averaging is enabled if so fix the result to
  // the inv-distance weighted average
  if (use_averaging) {
    result = 0.0;
    double deno = 0.0;
    for (const auto &item : ng_list) {
      auto sq_dist =
          1.0 /
          (Point3d::distance_sq(source_coord[item], dest_coord) + C_EpsilonWLS);
      result += source_value[item] * sq_dist;
      deno += sq_dist;
    }
    result = result / deno;
  }
  // Return the correct interpolated result
  return result;
}

/**
 * @brief Performs Weighted Least Squares Regression Test.
 *
 * This function calculates the error value between the destination coordinates
 * and the source coordinates using the Weighted Least Squares Regression
 * algorithm. The error value is calculated as the absolute difference between
 * the sum of the x, y, and z coordinates of the destination point and the sum
 * of the x, y, and z coordinates of the corresponding source point. The error
 * value is then normalized by dividing it by the absolute value of the sum of
 * the x, y, and z coordinates of the destination point.
 *
 * @tparam N_DIM The number of dimensions.
 * @param v_ng_list A vector of neighbor lists for each destination point.
 * @param dest_coord A vector of destination coordinates.
 * @param source_coord A vector of source coordinates.
 * @param error_value A vector to store the calculated error values.
 */
template <int N_DIM>
void WLSRegressionTest(const std::vector<std::vector<id_size_t>> &v_ng_list,
                       const std::vector<Point3d> &dest_coord,
                       const std::vector<Point3d> &source_coord,
                       std::vector<double> &error_value) {
  std::vector<double> source_value(source_coord.size());
  for (my_size_t i = 0; i < source_value.size(); ++i)
    source_value[i] = source_coord[i].x + source_coord[i].y + source_coord[i].z;
  error_value.resize(dest_coord.size());
  for (my_size_t i = 0; i < dest_coord.size(); ++i)
    error_value[i] = WLSRegression<N_DIM>(
        v_ng_list[i], dest_coord[i], source_coord, source_value);
  for (my_size_t i = 0; i < error_value.size(); ++i) {
    auto exact_value = (dest_coord[i].x + dest_coord[i].y + dest_coord[i].z);
    error_value[i] = std::abs((error_value[i] - exact_value));
    if (std::abs(exact_value) > std::numeric_limits<double>::epsilon())
      error_value[i] /= std::abs(exact_value);
  }
}

/**
 * @brief Calculates the least squares (LS) stencil for a given set of donor and
 * acceptor points.
 *
 * @param donor_size The number of donor points.
 * @param donor The vector of donor points.
 * @param acceptor_size The number of acceptor points.
 * @param acceptor The vector of acceptor points.
 * @param edge_list The list of edges connecting the vertices.
 * @param vertex_edges The adjacency list of edges for each vertex.
 * @param lsq_stencil The output vector of LSQ stencils for each acceptor point.
 */
template <int N_DIM, int MAX_LEAF>
void GetLSQStencil(
    const int donor_size,
    const std::vector<Point3d> &donor,
    const int acceptor_size,
    const std::vector<Point3d> &acceptor,
    const std::vector<std::pair<id_size_t, id_size_t>> &edge_list,
    const std::vector<std::vector<int>> &vertex_edges,
    std::vector<std::vector<id_size_t>> &lsq_stencil) {
  lsq_stencil.clear();
  lsq_stencil.shrink_to_fit();
  lsq_stencil.resize(acceptor_size);
  PointCloud donor_cloud(donor);
  nanoflann::KDTreeSingleIndexAdaptor<
      nanoflann::L2_Simple_Adaptor<double, PointCloud>,
      PointCloud,
      N_DIM,
      size_t>
      index(N_DIM,
            donor_cloud,
            nanoflann::KDTreeSingleIndexAdaptorParams(MAX_LEAF));
  index.buildIndex();

  for (int i = 0; i < acceptor_size; ++i) {
    // First find an approximate nearest neighbour
    size_t num_results = (N_DIM == 2) ? 4 : 5;
    std::vector<size_t> ret_index(num_results,
                                  std::numeric_limits<size_t>::max());
    std::vector<double> out_dist_sqr(num_results,
                                     std::numeric_limits<double>::max());
    num_results = index.knnSearch(
        &(acceptor[i].x), num_results, &ret_index[0], &out_dist_sqr[0]);
    if (num_results == 0 ||
        out_dist_sqr[0] <= std::numeric_limits<double>::epsilon()) {
      // std::cout << i << "th acceptor node in same location as donor node ";
      lsq_stencil[i].push_back(ret_index[0]);
    } else {
      // Now set the radius to the largest node distance and search with eps
      // tolerance
      std::vector<nanoflann::ResultItem<size_t, double>> ret_matches;
      nanoflann::SearchParameters params;
      params.sorted = true;
      num_results = index.radiusSearch(
          &(acceptor[i].x),
          out_dist_sqr.back() + std::numeric_limits<double>::epsilon(),
          ret_matches,
          params);
      ExtractEdgeNeighbours(
          ret_matches[0].first, edge_list, vertex_edges, lsq_stencil[i]);
      const auto max_ngbr = (N_DIM == 2) ? 4 : 5;
      if (lsq_stencil[i].size() < max_ngbr)
        std::cout << "Warning: Deficient LS stencil "
                  << " size = " << lsq_stencil[i].size()
                  << " defaulting to distance-weighted averaging\n";
    }
  }
}

} // namespace taru