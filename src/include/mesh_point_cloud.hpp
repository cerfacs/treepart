/**
 * @file mesh_point_cloud.hpp
 * @author Pavanakumar Mohanamuraly (mpkumar@cerfacs.fr)
 * @brief Thin wrapper around nanoflann to represent point clouds
 * @copyright Copyright (c) 2023 CERFACS
 *
 */
#pragma once

#include "mesh_common.hpp"

namespace taru {

/**
 * @brief
 *
 */
struct PointCloud {
private:
  const std::vector<Point3d> &pts;

public:
  // Construct from vector of point3d
  PointCloud(const std::vector<Point3d> &my_pts)
      : pts(my_pts) {}

  // Must return the number of data points
  inline size_t kdtree_get_point_count() const { return pts.size(); }

  // Returns the dim'th component of the idx'th point in the class:
  // Since this is inlined and the "dim" argument is typically an immediate
  // value, the
  //  "if/else's" are actually solved at compile time.
  inline double kdtree_get_pt(const size_t idx, const size_t dim) const {
    if (dim == 0)
      return pts[idx].x;
    else if (dim == 1)
      return pts[idx].y;
    else
      return pts[idx].z;
  }

  // Optional bounding-box computation: return false to default to a standard
  // bbox computation loop.
  //   Return true if the BBOX was already computed by the class and returned in
  //   "bb" so it can be avoided to redo it again. Look at bb.size() to find out
  //   the expected dimensionality (e.g. 2 or 3 for point clouds)
  template <class BBOX> bool kdtree_get_bbox(BBOX & /* bb */) const {
    return false;
  }
};

/**
 * @brief
 *
 */
struct BoundaryPointCloud {
private:
  const std::vector<Point3d> &pts;
  const std::vector<id_size_t> &bnode2node;
  const int start_index = 1;

public:
  /**
   * @class BoundaryPointCloud
   * @brief Represents a boundary point cloud.
   *
   * This class stores a boundary point cloud, which consists of a set of 3D
   * points and a mapping between boundary nodes and nodes in the point cloud.
   */
  BoundaryPointCloud(const std::vector<Point3d> &my_pts,
                     const std::vector<id_size_t> &my_bnode2node)
      : pts(my_pts),
        bnode2node(my_bnode2node) {}

  /**
   * @brief Constructs a BoundaryPointCloud object.
   *
   * This constructor initializes a BoundaryPointCloud object with the given
   * parameters.
   *
   * @param my_pts The vector of Point3d objects representing the points of the
   * point cloud.
   * @param my_bnode2node The vector of id_size_t objects representing the
   * mapping from boundary nodes to nodes.
   * @param my_start_index The starting index of the point cloud.
   */
  BoundaryPointCloud(const std::vector<Point3d> &my_pts,
                     const std::vector<id_size_t> &my_bnode2node,
                     const int my_start_index)
      : pts(my_pts),
        bnode2node(my_bnode2node),
        start_index(my_start_index) {}

  /**
   * @brief Must return the number of data points
   *
   * @return size_t number of data points
   */
  inline size_t kdtree_get_point_count() const { return bnode2node.size(); }

  /**
   * @brief Returns the dim'th component of the idx'th point in the class: Since
   * this is inlined and the "dim" argument is typically an immediate value, the
   * "if/else's" are actually solved at compile time.
   *
   * @param idx index
   * @param dim  dimension
   * @return double value
   */
  inline double kdtree_get_pt(const size_t idx, const size_t dim) const {
    if (dim == 0)
      return pts[bnode2node[idx] - start_index].x;
    else if (dim == 1)
      return pts[bnode2node[idx] - start_index].y;
    else
      return pts[bnode2node[idx] - start_index].z;
  }

  /**
   * @brief Optional bounding-box computation: return false to default to a
   * standard bbox computation loop.
   *
   * Return true if the BBOX was already computed by the class and returned in
   * "bb" so it can be avoided to redo it again. Look at bb.size() to find out
   * the expected dimensionality (e.g. 2 or 3 for point clouds)
   *
   * @tparam BBOX
   */
  template <class BBOX> bool kdtree_get_bbox(BBOX & /* bb */) const {
    return false;
  }
};

} // namespace taru
