/**
 *  @file mesh.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Documentation for mesh.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "distributed_hash.hpp"
#include "partition_method.hpp"
#include <locale>
#include <numeric>
#include <sstream>

namespace taru {

typedef DistributedHashTable<id_size_t, Point3d>
    PointDHT; //!< Typedef for point distributed hash table
typedef DistributedHashTable<id_size_t, ElementConnectivity>
    ElementDHT; //!< Typedef for element connectivity distributed hash table
typedef DistributedHashTable<id_size_t, double>
    ScalarDHT; //!< Typedef for double scalar distributed hash table
typedef DistributedHashTable<id_size_t, float>
    FloatDHT; //!< Typedef for floating point hash table

/**
 * @brief Abstract mesh class
 */
class Mesh {

public:
  /**
   * @brief Constructor for abstract mesh class using an abstract reader
   * interface
   *
   * @param file : File name to read mesh from
   * @param comm : Communicator object
   */
  template <typename T_MeshReader>
  Mesh(T_MeshReader &reader, CommunicatorConcept &comm)
      : m_element_connectivity_dht(comm),
        m_node_dht(comm),
        m_centroid_dht(comm),
        m_volume_dht(comm),
        m_element_weights_dht(comm) {
    Time(global_timer::TIME_MESH_SETUP, global_timer::C_TimerStart);
    reader.GetElementSizes(m_num_elements);
    m_is_2d = reader.Is2D();
    static_assert(sizeof(Point3d) == sizeof(double) * 3,
                  "Error: Bad padding in double struct to array\n");
    static_assert(sizeof(ElementConnectivity) == sizeof(id_size_t) * 8,
                  "Error: Bad padding in id_size_t struct to array\n");
    reader.GetNodeSize(m_num_nodes);
    /* Form the element type offsets */
    m_element_offsets[0] = 0;
    for (unsigned i = 0; i < C_NumElementTypes; ++i)
      m_element_offsets[i + 1] = m_element_offsets[i] + m_num_elements[i];
    if (comm.IsValid())
      if (comm.IsMaster())
        std::cout << "Message: Distributing mesh between " << comm.size()
                  << " ranks\n";
    // Check if reader allocates dictionary otherwise do it
    reader.GetPeriodicPatch(m_periodic_patch_ids, m_num_periodic_patches);
    reader.GetPeriodicNodes(m_periodic_node_pairs);
    std::locale locale("");
    std::cout.imbue(locale);
    if (comm.IsValid()) {
      if (comm.IsMaster()) {
        std::cout << "-------------------------------\n";
        std::cout << "Element statistics\n-------------------------------\n";
        std::cout << "TRI = " << m_num_elements[TRI_ID] << "\n";
        std::cout << "QUA = " << m_num_elements[QUA_ID] << "\n";
        std::cout << "TET = " << m_num_elements[TET_ID] << "\n";
        std::cout << "HEX = " << m_num_elements[HEX_ID] << "\n";
        std::cout << "PRI = " << m_num_elements[PRI_ID] << "\n";
        std::cout << "PYR = " << m_num_elements[PYR_ID] << "\n";
        std::cout << "-------------------------------\n";
        std::cout << "Number of Node = " << m_num_nodes << "\n";
        if (!m_periodic_patch_ids.empty())
          std::cout << "Number of periodic patch pairs = "
                    << m_num_periodic_patches / 2 << "\n";
        std::cout << "-------------------------------\n";
      }
    }
    std::cout.imbue(std::locale::classic());
    reader.ReadNodeCoordinates(GetNumNodes(), m_node_dht);
    m_AABB = reader.ReadElements(GetNumElements(),
                                 m_node_dht,
                                 m_element_connectivity_dht,
                                 m_centroid_dht);
    m_num_patch = reader.PopulateBoundaryInfo(m_num_nodes, m_boundary_info);
    reader.PopulatePatchFaces(m_element_offsets[C_NumElementTypes],
                              m_bfaces_info);
// Debug the boundary faces created
#if 0
{
      // Collect all the elements that have a boundary face
      std::vector<id_size_t> debug_boundary_eid;
      std::vector<ElementConnectivity> debug_element_connectivity;
      std::vector<id_size_t> debug_boundary_nid;
      std::vector<Point3d> debug_boundary_xyz;
      id_size_t debug_counter = 0;
      for( const auto &item : m_bfaces_info ) {
        if(!item.empty())
          debug_boundary_eid.push_back(debug_counter +
                                       m_element_connectivity_dht.begin());
        debug_counter++;
      }
      debug_element_connectivity.resize(debug_boundary_eid.size());
      // Get the element node connectivity information
      m_element_connectivity_dht.GetDataFromKeys(debug_boundary_eid, debug_element_connectivity);
      // Create a unique mapping for connectivity and get the local
      // set of nodes for boundary element
      ConvertToLocalIndex(debug_element_connectivity, debug_boundary_nid);
      debug_boundary_xyz.resize(debug_boundary_nid.size());
      // Get the xyz of boundary node ids
      for( auto &item : debug_boundary_nid)
        --item;
      m_node_dht.GetDataFromKeys(debug_boundary_nid, debug_boundary_xyz);
      // Write the boundary element for visualisation
      std::stringstream debug_cat;
      debug_cat << "debug_boundary_elm_" << comm.Rank() << ".dat";
      std::ofstream debug_fout( debug_cat.str() );
      if (!m_is_2d) {
        debug_fout << "VARIABLES=\"X\",\"Y\",\"Z\"\n";
        debug_fout << "ZONE DATAPACKING=BLOCK, NODES=" << debug_boundary_nid.size()
             << ", ELEMENTS=" << debug_element_connectivity.size()
             << ", ZONETYPE=FETETRAHEDRON\n";
      } else {
        debug_fout << "VARIABLES=\"X\",\"Y\"\n";
        debug_fout << "ZONE DATAPACKING=BLOCK, NODES=" << debug_boundary_nid.size()
             << ", ELEMENTS=" << debug_element_connectivity.size()
             << ", ZONETYPE=FETRIANGLE\n";
      }
      for (const auto &item : debug_boundary_xyz)
        debug_fout << item.x << "\n";
      for (const auto &item : debug_boundary_xyz)
        debug_fout << item.y << "\n";
      if (!m_is_2d)
        for (const auto &item : debug_boundary_xyz)
          debug_fout << item.z << "\n";
      // Write internal data
      int debug_num_vertex = m_is_2d ? 3 : 4;
      for (const auto &item : debug_element_connectivity ) {
        for (int j = 0; j < debug_num_vertex; ++j) {
          debug_fout << item.nodes[j] << "  ";
        }
        debug_fout << "\n";
      }
      debug_fout.close();
}
#endif
    // Read the patch label info
    reader.ReadPatchLabels(m_num_patch, m_patch_labels);
    reader.close();
    Time(global_timer::TIME_MESH_SETUP, global_timer::C_TimerStop);
  }

  /**
   * @brief In-memory checkpointing for adapted meshes
   *
   * @param coord : On return contains the centroids of the elements in conn)
   * @param conn  : Not modified (but cannot make this const due to Zoltan C
   * call)
   * @param owned_map : Mapping of global node ids to local node ids
   * @param spill_over_map : Mapping of global node ids to local node ids
   * @param num_patches : Number of patches
   * @param periodic_pairs  : Periodic pairs
   * @param adapt_binfo : Boundary information
   * @param comm  : Communicator object
   * @param ptr_global_ids : If not null returns the element keys of conn
   */
  Mesh(bool is_2d,
       std::vector<Point3d> &coord,
       std::vector<ElementConnectivity> &conn,
       const std::map<id_size_t, id_size_t> &owned_map,
       const std::map<id_size_t, id_size_t> &spill_over_map,
       const int num_patches,
       const std::vector<char> &patch_labels,
       const std::vector<int> &periodic_pairs,
       const std::vector<std::set<int>> &in_adapt_binfo,
       const bface_t &local_bface_info,
       CommunicatorConcept &comm,
       std::vector<id_size_t> *ptr_global_ids = nullptr)
      : m_is_2d(is_2d),
        m_element_connectivity_dht(comm),
        m_node_dht(comm),
        m_centroid_dht(comm),
        m_volume_dht(comm),
        m_element_weights_dht(comm),
        m_num_patch(num_patches),
        m_patch_labels(patch_labels) {

    std::vector<std::set<int>> adapt_binfo(in_adapt_binfo);
    const rank_t my_rank = comm.Rank();
    const int64_t num_procs = comm.size();

    //////  ADD NODE COORDINATES TO DICTIONARY  ///////
    std::map<id_size_t, id_size_t> inv_owned_map;
    for (auto &item : owned_map)
      inv_owned_map[item.second] = item.first;

    std::map<id_size_t, id_size_t> node_global_to_local;
    for (auto &item : owned_map)
      node_global_to_local[item.second] = item.first;
    for (auto &item : spill_over_map)
      node_global_to_local[item.second] = item.first;

    m_num_nodes = comm.MinMaxMapDataGlobal(owned_map)[1];
    // Form the keys of the adapted mesh for hash
    std::vector<Point3d> temp_coord;
    std::vector<id_size_t> keys;
    keys.reserve(owned_map.size());
    for (auto &item : inv_owned_map) {
      temp_coord.push_back(coord[item.second]);
      // Note the index are Fortran indexed 1 is the starting index
      keys.push_back(item.first - 1);
    }
    m_node_dht.SetSize(m_num_nodes);
    m_node_dht.Populate(keys, temp_coord);
    temp_coord.clear();
    temp_coord.shrink_to_fit();
    keys.clear();
    keys.shrink_to_fit();

    ///////// PUT CONNECTIVITY TO HASH TABLE /////////
    my_size_t local_elem_size = conn.size();
    my_size_t global_elem_size = 0;
    my_size_t global_elem_offset = 0;
    MPI_Allreduce(&local_elem_size,
                  &global_elem_size,
                  1,
                  GetMpiType<my_size_t>(),
                  MPI_SUM,
                  comm.RawHandle());
    m_element_connectivity_dht.SetSize(global_elem_size);
    keys.reserve(local_elem_size);
    MPI_Allreduce(&local_elem_size,
                  &global_elem_size,
                  1,
                  GetMpiType<my_size_t>(),
                  MPI_SUM,
                  comm.RawHandle());
    MPI_Exscan(&local_elem_size,
               &global_elem_offset,
               1,
               GetMpiType<my_size_t>(),
               MPI_SUM,
               comm.RawHandle());
    for (my_size_t i = 0; i < local_elem_size; ++i)
      keys.push_back(i + global_elem_offset);
    m_element_connectivity_dht.Populate(keys, conn);

    /// Build the element centroid information
    std::vector<Point3d> centroid(local_elem_size);
    m_centroid_dht.SetSize(m_element_connectivity_dht.GlobalSize());
    id_size_t elm_count = 0;
    for (const auto &item : conn) {
      int node_count = 0;
      for (const auto &elm_node : item.nodes) {
        if (elm_node > 0) {
          centroid[elm_count].x += coord[node_global_to_local[elm_node]].x;
          centroid[elm_count].y += coord[node_global_to_local[elm_node]].y;
          if (!m_is_2d)
            centroid[elm_count].z += coord[node_global_to_local[elm_node]].z;
          node_count++;
        }
      }
      centroid[elm_count].x /= double(node_count);
      centroid[elm_count].y /= double(node_count);
      if (!m_is_2d)
        centroid[elm_count].z /= double(node_count);
      ++elm_count;
    }
    m_centroid_dht.Populate(keys, centroid);

    // Swap the coordinates to bootstrap
    centroid.swap(coord);
    if (ptr_global_ids != nullptr)
      ptr_global_ids->swap(keys);

    /////// Fix the element count and offset information
    for (const auto &ielem : conn) {
      unsigned num_elm_nodes = 0;
      for (unsigned j = 0; j < C_NumElementTypes; ++j) {
        if (ielem.nodes[j] != 0)
          num_elm_nodes++;
        else
          break;
      }
      if (m_is_2d)
        m_num_elements[num_elm_nodes - 3]++; // Valid for triangles and quads
      else
        m_num_elements[num_elm_nodes -
                       2]++; // Valid for tetrahedron, ..., hexahedron
    }
    MPI_Allreduce(MPI_IN_PLACE,
                  m_num_elements,
                  C_NumElementTypes,
                  GetMpiType<my_size_t>(),
                  MPI_SUM,
                  comm.RawHandle());
    for (unsigned i = 0; i < C_NumElementTypes; ++i)
      m_element_offsets[i + 1] = m_element_offsets[i] + m_num_elements[i];

    // Create boundary face information (just move data to the assumed
    // partition)
    my_size_t assumed_elem_begin, assumed_elem_size;
    comm.RankDistribution(
        global_elem_size, assumed_elem_begin, assumed_elem_size);

    m_bfaces_info.clear();
    m_bfaces_info.shrink_to_fit();
    m_bfaces_info.resize(assumed_elem_size);
    int64_t assumed_quotient = global_elem_size / comm.size();
    int64_t assumed_remainder = global_elem_size % comm.size();
    int64_t assumed_threshold = assumed_remainder * (assumed_quotient + 1);
    // Now create the send list of bfaces using the assumed partitioning
    std::map<int, std::vector<id_size_t>> bface_send_pkdata;
    for (my_size_t i = 0; i < local_elem_size; ++i) {
      // Note that PROC_ID needs index in Fortran style "1" is starting idx
      auto i_proc = PROC_ID(int64_t(i + global_elem_offset + 1),
                            assumed_threshold,
                            assumed_quotient,
                            assumed_remainder);
      // Pack element data if bface info is non-empty
      if (!local_bface_info[i].empty()) {
        bface_send_pkdata[i_proc].push_back(i + global_elem_offset);
        bface_send_pkdata[i_proc].push_back(local_bface_info[i].size());
        for (const auto &item : local_bface_info[i]) {
          bface_send_pkdata[i_proc].push_back(item.face_id);  // face_id
          bface_send_pkdata[i_proc].push_back(item.patch_id); // patch_id
        }
      }
    }
    // Infer num recv on the assumed ranks to receive the packed send
    std::vector<rank_t> bface_assumed_send_ranks;
    for (const auto &item : bface_send_pkdata)
      bface_assumed_send_ranks.push_back(item.first);
    auto bface_assumed_num_recv =
        comm.InferNumReceive(bface_assumed_send_ranks);

    std::vector<MPI_Request> send_req(bface_assumed_send_ranks.size());
    std::vector<MPI_Status> send_stat(bface_assumed_send_ranks.size());
    MPI_Status recv_stat;
    id_size_t count = 0;
    // Now send the packed data and receive unpack
    for (const auto &item : bface_send_pkdata)
      MPI_Isend(&item.second[0],
                item.second.size(),
                GetMpiType<id_size_t>(),
                item.first,
                my_rank,
                comm.RawHandle(),
                &send_req[count++]);
    // Receive and unpack
    for (int irecv = 0; irecv < bface_assumed_num_recv; ++irecv) {
      int recv_size;
      MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
      MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
      std::vector<id_size_t> temp_buffer(recv_size);
      MPI_Recv(&temp_buffer[0],
               recv_size,
               GetMpiType<id_size_t>(),
               recv_stat.MPI_SOURCE,
               recv_stat.MPI_TAG,
               comm.RawHandle(),
               &recv_stat);
      // std::cout << "Received " << recv_size << " from rank " <<
      // recv_stat.MPI_TAG << " in rank " << my_rank << "\n";
      auto cur_item = temp_buffer.begin();
      for (;;) {
        // First item will be GID of element
        auto my_elm_lid = *cur_item - assumed_elem_begin;
        ++cur_item;
        if (my_elm_lid < 0 || my_elm_lid >= m_bfaces_info.size()) {
          // std::cout << "my_elm_lid = " << my_elm_lid << "\n";
          // std::cout << "m_bfaces_info.size() = " << m_bfaces_info.size() <<
          // "\n";
          throw std::runtime_error("Received GID exceeds LID of elements !");
        }
        // Next item is number of boundary faces for this element
        auto num_bfaces = *cur_item;
        ++cur_item;
        // Pull out the individual boundary face info
        for (decltype(num_bfaces) ibface = 0; ibface < num_bfaces; ++ibface) {
          BoundaryFace temp_face;
          temp_face.face_id =
              *cur_item + 1; // TODO: Make this consistent across mesh object
          ++cur_item;
          temp_face.patch_id = *cur_item;
          ++cur_item;
          if (temp_face.patch_id <= 0 || temp_face.patch_id > m_num_patch)
            throw std::runtime_error("Negative or zero patch id !");
          if (temp_face.face_id <= 0)
            throw std::runtime_error("Negative or zero face id !");
          // std::cout << "Element id = " << my_elm_lid << " faces = " <<
          // num_bfaces << "\n";
          m_bfaces_info[my_elm_lid].push_back(temp_face);
        }
        if (cur_item == temp_buffer.end())
          break;
      }
    }
    // Wait for send to complete
    MPI_Waitall(send_req.size(), &send_req[0], &send_stat[0]);
    send_req.clear();
    send_stat.clear();

    ////// PUT PERIODIC DATA /////////
    m_periodic_patch_ids = periodic_pairs;
    m_num_periodic_patches = periodic_pairs.size();

    /////// PUT PATCH DATA ///////////
    RemoveSharedInfo(adapt_binfo);

    // This data-structure is designed in such a way that we
    // get the global-node-id ==>> patch-ids map and it is
    // naturally sorted in increasing order of global-node-id
    // so easy to construct the assumed partition send/recv
    std::map<rank_t, std::vector<id_size_t>> packed_data;
    std::map<rank_t, id_size_t> packed_size;
    std::map<rank_t, id_size_t> packed_counter;
    // Input for proc id determination using the macro PROC_ID
    const int64_t num_nodes = m_node_dht.GlobalSize();
    const int64_t quotient = num_nodes / num_procs;
    const int64_t remainder = num_nodes % num_procs;
    const int64_t threshold = remainder * (quotient + 1);
    m_boundary_info.resize(m_node_dht.size());
    for (const auto &item : inv_owned_map) {
      int64_t global_id = item.first;
      rank_t proc_id = PROC_ID(global_id, threshold, quotient, remainder);
      // If item is in the same rank just copy the data to m_boundary_info
      if (proc_id == my_rank) {
        if (!adapt_binfo[item.second].empty())
          for (const auto &bpatch_id : adapt_binfo[item.second])
            m_boundary_info[item.first - m_node_dht.begin() - 1].push_back(
                bpatch_id);
      }
      // If not in the same rank keep track of the data that need to be packed
      else {
        if (!adapt_binfo[item.second].empty()) {
          // This process was not allocated so the first entry should be 1
          // which is the place holder for the total # packed node ids
          if (packed_size.find(proc_id) == packed_size.end()) {
            packed_data[proc_id].push_back(0);
            packed_size[proc_id] = 1;
          }
          // Push the global gid of node and the number of connected patches
          packed_data[proc_id].push_back(global_id);
          packed_data[proc_id].push_back(adapt_binfo[item.second].size());
          // Push the connected patch list
          for (const auto &bpatch_id : adapt_binfo[item.second])
            packed_data[proc_id].push_back(bpatch_id);
          packed_size[proc_id] += adapt_binfo[item.second].size() + 1 + 1;
          packed_counter[proc_id]++;
        }
      }
    }
    // Now assign the first item of packed data as the number of node gids
    for (const auto &item : packed_size)
      packed_data[item.first][0] = packed_counter[item.first];
    // Now send/recv the remaining boundary node info
    std::vector<rank_t> send_ranks;
    send_ranks.reserve(packed_data.size());
    // Create send rank list
    for (const auto &item : packed_data)
      send_ranks.push_back(item.first);
    // Infer number of receives
    auto num_recv = comm.InferNumReceive(send_ranks);
    // Send=recv the pack boundary data and form the local
    // boundary dictionary
    send_req.resize(send_ranks.size());
    send_stat.resize(send_ranks.size());
    count = 0;
    for (const auto &item : packed_data)
      MPI_Isend(&item.second[0],
                item.second.size(),
                GetMpiType<id_size_t>(),
                item.first,
                my_rank,
                comm.RawHandle(),
                &send_req[count++]);
    // Receive by probing the packed data
    m_boundary_info.resize(m_node_dht.size());
    for (decltype(num_recv) irecv = 0; irecv < num_recv; ++irecv) {
      int recv_size;
      MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
      MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
      std::vector<id_size_t> temp_buffer(recv_size);
      MPI_Recv(&temp_buffer[0],
               recv_size,
               GetMpiType<id_size_t>(),
               recv_stat.MPI_SOURCE,
               recv_stat.MPI_TAG,
               comm.RawHandle(),
               &recv_stat);
      // Unpack the temp buffer : First the number of nodes is given in the
      // first entry
      auto num_recv_nodes = temp_buffer[0];
      count = 0;
      for (decltype(num_recv_nodes) i = 0; i < num_recv_nodes; ++i) {
        // Note: global node id is in Fortran index so I need to subtract 1 to
        // make it C index
        auto my_node_lid = temp_buffer[++count] - m_node_dht.begin() - 1;
        auto num_recv_patches = temp_buffer[++count];
        for (decltype(num_recv_patches) ipatch = 0; ipatch < num_recv_patches;
             ++ipatch)
          m_boundary_info[my_node_lid].push_back(temp_buffer[++count]);
      }
      // Make sure we unpacked correctly. Note that cout is zero based.
      assert(int(count) + 1 == recv_size);
    }
    MPI_Waitall(send_req.size(), &send_req[0], &send_stat[0]);
    std::locale locale("");
    std::cout.imbue(locale);
    if (comm.IsValid()) {
      if (comm.IsMaster()) {
        std::cout << "-------------------------------\n";
        std::cout << "Element statistics\n-------------------------------\n";
        std::cout << "TRI = " << m_num_elements[TRI_ID] << "\n";
        std::cout << "QUA = " << m_num_elements[QUA_ID] << "\n";
        std::cout << "TET = " << m_num_elements[TET_ID] << "\n";
        std::cout << "HEX = " << m_num_elements[HEX_ID] << "\n";
        std::cout << "PRI = " << m_num_elements[PRI_ID] << "\n";
        std::cout << "PYR = " << m_num_elements[PYR_ID] << "\n";
        std::cout << "-------------------------------\n";
        std::cout << "Number of Node = " << m_num_nodes << "\n";
        std::cout << "-------------------------------\n";
      }
    }
    std::cout.imbue(std::locale::classic());
    // comm.Barrier();
    // exit(0);
  }

  /**
   * @brief Clear all mesh contents
   */
  void clear() {
    for (auto &item : m_num_elements)
      item = 0;
    for (auto &item : m_element_offsets)
      item = 0;
    m_num_nodes = 0;
    m_element_connectivity_dht.clear();
    m_node_dht.clear();
    m_centroid_dht.clear();
    m_volume_dht.clear();
    m_element_weights_dht.clear();
    m_boundary_info.clear();
    m_bfaces_info.clear();
    //    m_num_patch = 0;
    //    m_num_periodic_pairs = 0;
    //    m_periodic_pairs.clear();
  }

  /**
   * @brief Get the deviation element weights
   * @return size_t   Number of nodes in the mesh
   * @param method : Partitioner type (as named string)
   * @param metric_dht : Metric hash table
   */
  void DeviationElementWeights(const partition_method method,
                               ScalarDHT &metric_dht) {
    if (is_graph_partitioner(method)) {
      if (m_node_dht.CommunicatorHandle().IsMaster())
        std::cout << "Message: Performing Bootstrap weight calculation\n";
      // Read local element chunk and renumber to local index
      std::vector<id_size_t> keys(m_element_connectivity_dht.size());
      for (id_size_t i = 0; i < id_size_t(m_element_connectivity_dht.size());
           ++i)
        keys[i] = i + m_element_connectivity_dht.begin();
      std::vector<ElementConnectivity> econn(m_element_connectivity_dht.size());
      m_element_connectivity_dht.GetDataFromKeys(keys, econn);
      std::vector<id_size_t> node_keys;
      ConvertToLocalIndex(econn, node_keys);
      for (auto &item : node_keys)
        --item;
      std::vector<Point3d> coord(node_keys.size());
      std::vector<double> target_metric(node_keys.size());
      std::vector<float> deviation_metric(keys.size());
      m_node_dht.GetDataFromKeys(node_keys, coord);
      metric_dht.GetDataFromKeys(node_keys, target_metric);

      // Form the element-wise metric deviation
      if (!Is2D()) {
        const auto all_tet_edges = TetEdgeNodes();
        for (id_size_t icell = 0; icell < id_size_t(econn.size()); ++icell) {
          const auto &cell_nodes = econn[icell].nodes;
          float mean_element_size = 0.0;
          float mean_element_target = 0.0;
          for (auto &tet_edge : all_tet_edges) {
            const auto inode = cell_nodes[tet_edge[0]] - 1;
            const auto jnode = cell_nodes[tet_edge[1]] - 1;
            mean_element_size +=
                Point3d::distance_l2(coord[inode], coord[jnode]);
            mean_element_target += target_metric[inode] + target_metric[jnode];
          }
          // For tet divide the sum by num_edges and for target 4 * 2 (double
          // counting)
          mean_element_size /= TetEdgeNodes().size();
          mean_element_target /= 2.0 * C_ElementNumNodes[TET_ID];
          deviation_metric[icell] =
              std::pow(mean_element_size, 3) / std::pow(mean_element_target, 3);
        }
      } else {
        const auto all_tri_edges = TriEdgeNodes();
        for (id_size_t icell = 0; icell < id_size_t(econn.size()); ++icell) {
          const auto &cell_nodes = econn[icell].nodes;
          float mean_element_size = 0.0;
          float mean_element_target = 0.0;
          for (auto &tri_edge : all_tri_edges) {
            const auto inode = cell_nodes[tri_edge[0]] - 1;
            const auto jnode = cell_nodes[tri_edge[1]] - 1;
            mean_element_size +=
                Point3d::distance_l2(coord[inode], coord[jnode]);
            mean_element_target += target_metric[inode] + target_metric[jnode];
          }
          // For tri divide the sum by num_edges and for target 3 * 2 (double
          // counting)
          mean_element_size /= TriEdgeNodes().size();
          mean_element_target /= 2.0 * C_ElementNumNodes[TRI_ID];
          deviation_metric[icell] =
              std::pow(mean_element_size, 2) / std::pow(mean_element_target, 2);
        }
      }

      // Increment deviation metric to prevent errors in Zoltan
      for (auto &item : deviation_metric)
        item += 1.0;

      // Commit it to m_element_weights_dht
      m_element_weights_dht.SetSize(m_element_connectivity_dht.GlobalSize());
      m_element_weights_dht.Populate(keys, deviation_metric);

      auto &comm = m_node_dht.CommunicatorHandle();
      auto sum_weights = std::accumulate(
          deviation_metric.begin(), deviation_metric.end(), 0.0);
      std::vector<double> min_max_weights_v = {-sum_weights, sum_weights};
      comm.AllReduceSum(&sum_weights, 1);
      comm.AllReduceMax(min_max_weights_v.data(), 2);
      min_max_weights_v[0] =
          -min_max_weights_v[0]; // This trick avoids doing allreduce twice
      if (comm.IsMaster())
        std::cout << "Message: Estimated total elements after adapt ("
                  << long(sum_weights) << ") min/max ("
                  << long(min_max_weights_v[0]) << ")/("
                  << long(min_max_weights_v[1]) << ")\n";
    }
  }

  /**
   * @brief Read the element weights from a file
   * @param file : File name
   * @param dset : Data set name
   */
  void ReadElementWeights(const std::string &file, const std::string &dset) {
    m_element_weights_dht.Populate<float>(file, dset);
  }

  /**
   * @brief Checks if two meshes are equivalent
   *        WARNING: This is a very expensive function only use for debugging!
   * @param msh : Mesh to compare with
   * @return true if the meshes are equivalent
   */
  bool IsEqual(Mesh &msh) {
    bool all_ok = true;
    std::stringstream cat;
    cat << "mesh_errors_" << m_node_dht.CommunicatorHandle().Rank() << ".dat";
    std::ofstream err_out(cat.str());
    if (m_is_2d != msh.m_is_2d) {
      err_out << "Error: Comparing 2d with a 3d mesh!\n";
      all_ok = false;
    }
    if (!m_node_dht.IsEqual(msh.m_node_dht, &err_out)) {
      err_out << "Error: Comparing node-coordinate hash table!\n";
      all_ok = false;
    }
    if (!m_element_connectivity_dht.IsEqual(msh.m_element_connectivity_dht,
                                            &err_out)) {
      err_out << "Error: Comparing element connectivity hash table!\n";
      all_ok = false;
    }
    if (!m_centroid_dht.IsEqual(msh.m_centroid_dht, &err_out)) {
      err_out << "Error: Comparing element centroid hash table!\n";
      all_ok = false;
    }
    // if (!m_volume_dht.IsEqual(msh.m_volume_dht, &err_out)) {
    //   err_out << "Error: Comparing element volume hash table!\n";
    //   all_ok = false;
    // }
    // if (!m_element_weights_dht.IsEqual(msh.m_element_weights_dht, &err_out))
    // {
    //   err_out << "Error: Comparing element centroid hash table!\n";
    //   all_ok = false;
    // }
    if (m_num_patch != msh.m_num_patch) {
      err_out << "Error: Number of patches do not match between meshes\n";
      all_ok = false;
    }
    if (m_num_periodic_patches != msh.m_num_periodic_patches) {
      err_out
          << "Error: Number of periodic patches do not match between meshes\n";
      all_ok = false;
    }
    if (m_periodic_patch_ids != msh.m_periodic_patch_ids) {
      err_out
          << "Error: Periodic patch pairs ids do not match between meshes\n";
      all_ok = false;
    }
    if (m_num_nodes != msh.m_num_nodes) {
      err_out << "Error: Number of nodes do not match between meshes\n";
      all_ok = false;
    }
    for (unsigned i = 0; i < C_NumElementTypes; ++i) {
      if (m_num_elements[i] != msh.m_num_elements[i]) {
        err_out << "Error: Number of elements at position " << i
                << " do not match between meshes\n";
        all_ok = false;
      }
    }
    for (unsigned i = 0; i < C_NumElementTypes + 1; ++i) {
      if (m_element_offsets[i] != msh.m_element_offsets[i]) {
        err_out << "Error: Element offset at position " << i
                << " do not match between meshes\n";
        all_ok = false;
      }
    }
    if (m_boundary_info.size() != msh.m_boundary_info.size()) {
      err_out << "Error: Boundary dictionary local sizes do not agree\n";
      all_ok = false;
    }
    if (m_boundary_info != msh.m_boundary_info) {
      // Do a more detailed check
      for (std::size_t i = 0; i < m_boundary_info.size(); ++i) {
        if (m_boundary_info[i].size() != msh.m_boundary_info[i].size()) {
          err_out << "Error: # Patches not equal for node # "
                  << i + m_node_dht.begin() << " they are "
                  << m_boundary_info[i].size() << " and "
                  << msh.m_boundary_info[i].size() << "\n";
          all_ok = false;
        }
      }
    }
    return all_ok;
  }

  /**
   * @brief  Initialize the volume hash table
   *
   * @param mfile : File name to read the volume data from
   * @param mdset : Data set name to read the volume data from
   */
  void InitVolumeDHT(const std::string mfile, const std::string mdset) {
    if (m_volume_dht.CommunicatorHandle().IsMaster()) {
      std::cout << "Message: Populating volume DHT by reading from \n";
      std::cout << "Message: " << mfile << " using data set \"" << mdset
                << "\"\n";
    }
    m_volume_dht.Populate<double>(mfile, mdset);
  }

  /**
   * @brief The total number of elements in the mesh
   *
   * @return Returns the total number of elements in the mesh
   */
  my_size_t GetNumElements() { return m_element_offsets[C_NumElementTypes]; }

  /**
   * @brief The total number of nodes in the mesh
   *
   * @return Returns the total number of nodes in the mesh
   */
  my_size_t GetNumNodes() { return m_num_nodes; }

  /**
   * @brief Get the Patch Labels object
   *
   * @return std::string&
   */
  std::vector<char> &GetPatchLabels() { return m_patch_labels; }

  /**
   *
   * @return
   */
  const DistributedHashTable<id_size_t, Point3d> &NodeHashTable() {
    return m_node_dht;
  }

  /**
   *
   * @return
   */
  DistributedHashTable<id_size_t, Point3d> &GetNodeHashTable() {
    return m_node_dht;
  }

  /**
   *
   * @return
   */
  DistributedHashTable<id_size_t, float> &GetElementWeightsHashTable() {
    return m_element_weights_dht;
  }

  /**
   *
   * @return
   */
  DistributedHashTable<id_size_t, double> &GetVolumeHashTable() {
    return m_volume_dht;
  }

  /**
   *
   * @return
   */
  const DistributedHashTable<id_size_t, Point3d> &CentroidHashTable() {
    return m_centroid_dht;
  }

  /**
   *
   * @return
   */
  const DistributedHashTable<id_size_t, ElementConnectivity> &
  ElementConnectivityHashTable() {
    return m_element_connectivity_dht;
  }

  /**
   *
   * @return
   */
  const DistributedHashTable<id_size_t, float> &ElementWeightsHashTable() {
    return m_element_weights_dht;
  }

  /**
   *
   * @return
   */
  DistributedHashTable<id_size_t, ElementConnectivity> &
  GetElementConnectivityHashTable() {
    return m_element_connectivity_dht;
  }

  /**
   *
   * @return
   */
  int GetNumPatches() { return m_num_patch; }

  /**
   *
   * @return
   */
  int GetNumPeriodicPairs() { return m_num_periodic_patches; }

  /**
   *
   * @return
   */
  std::vector<int> &GetPeriodicPairs() { return m_periodic_patch_ids; }

  /**
   *
   * @return
   */
  std::vector<id_size_t> &GetPeriodicNodePairs() {
    return m_periodic_node_pairs;
  }

  /**
   *
   * @return
   */
  binfo_t &GetBinfo() { return m_boundary_info; }

  /**
   *
   * @return
   */
  bface_t &GetBFaceInfo() { return m_bfaces_info; }

  /**
   *
   * @return
   */
  const my_size_t *GetElementOffset() { return m_element_offsets; }

  /**
   *
   * @return
   */
  double GetMemUsage() {
    uint64_t mem_used = 0;
    mem_used += m_node_dht.size() * sizeof(Point3d) * sizeof(id_size_t);
    mem_used += m_element_connectivity_dht.size() *
                sizeof(ElementConnectivity) * sizeof(id_size_t);
    mem_used +=
        m_element_weights_dht.size() * sizeof(float) * sizeof(id_size_t);
    mem_used += m_centroid_dht.size() * sizeof(Point3d) * sizeof(id_size_t);
    mem_used += m_volume_dht.size() * sizeof(double) * sizeof(id_size_t);
    for (auto &item : m_boundary_info)
      mem_used += item.size() * sizeof(int);
    mem_used += m_periodic_patch_ids.size() * sizeof(int);
    return double(mem_used) / 1048576.0;
  }

  /**
   * Check if mesh is a 2D mesh
   * @return
   */
  bool Is2D() { return m_is_2d; }

  /**
   * @brief
   *
   * @return const std::pair<Point3d, Point3d>&
   */
  const std::pair<Point3d, Point3d> &GetAABB() { return m_AABB; }

  /**
   * @brief Get the Local Ambiguous Faces object
   *
   * @param global_ids
   * @param comm
   */
  void GetLocalBoundaryFaces(std::vector<id_size_t> &global_ids,
                             CommunicatorConcept &comm,
                             bface_t &local_bfaces) {
    // std::stringstream cat;
    // cat << "recv_bface" << comm.Rank() << ".dat";
    // std::ofstream fout_recv(cat.str());
    // cat.str("");
    // cat << "send_bface" << comm.Rank() << ".dat";
    // std::ofstream fout_send(cat.str());

    // Only execute if boundary faces are present in the mesh
    if (m_bfaces_info.empty())
      return;
    // Step 0: Create hash table of element_id<->proc_id
    //         using local partition gids and comm Rank
    DistributedHashTable<id_size_t, int> gid_to_pid(comm);
    gid_to_pid.SetSize(GetNumElements());
    std::vector<int> data(global_ids.size(), comm.Rank());
    gid_to_pid.Populate(global_ids, data);
    data.clear();
    // Step 1: Find using the hash table (step 0) what
    //         processors to send the ambiguous faces
    std::vector<id_size_t> assumed_gids(
        GetElementConnectivityHashTable().size(),
        GetElementConnectivityHashTable().begin());
    for (id_size_t i = 0; i < id_size_t(assumed_gids.size()); ++i)
      assumed_gids[i] += i;
    data.resize(assumed_gids.size());
    gid_to_pid.GetDataFromKeys(assumed_gids, data);
    // Step 2: Mark elements that contain ambiguous faces
    //         on assumed partiton
    std::map<int, std::vector<id_size_t>> items_to_send;
    for (id_size_t i = 0; i < id_size_t(assumed_gids.size()); ++i)
      if (!m_bfaces_info[i].empty())
        items_to_send[data[i]].push_back(i);
    // for (const auto &item : items_to_send)
    //   std::cout << "Sending to proc " << item.first
    //             << " of size = " << item.second.size() << "\n";
    // Step 3: Using the blind send/recv primitive notify
    //         the blind receiver how many proc to recv
    auto num_recvs = comm.InferNumReceive(items_to_send);
    auto num_sends = items_to_send.size();
    // std::cout << "num_recvs = " << num_recvs << " in rank = " << comm.Rank()
    //           << "\n";
    // Step 4: Pack the ambiguous face data and do blind
    //         send/recv
    std::map<int, std::vector<id_size_t>> send_pack_data;
    for (const auto &item : items_to_send) {
      auto &my_send_item = send_pack_data[item.first];
      for (const auto &ielm : item.second) {
        // Pack 1) the boundary element id
        my_send_item.push_back(ielm +
                               GetElementConnectivityHashTable().begin());
        // fout_send //<< comm.Rank() << " : "
        //     << ielm + GetElementConnectivityHashTable().begin() + 1 << " ";
        // std::cout << "elm id = " << my_send_item.back() << "\n";
        // 2) Number of boundary faces attached to element
        my_send_item.push_back(m_bfaces_info[ielm].size());
        // std::cout << "size id = " << my_send_item.back() << "\n";
        for (const auto &item_data : m_bfaces_info[ielm]) {
          // 3) the face number in the element of the boundary face
          my_send_item.push_back(item_data.face_id);
          // fout_send << item_data.face_id << " ";
          // 4) the patch id of the boundary face
          my_send_item.push_back(item_data.patch_id);
        }
        // fout_send << "\n";
      }
    }
    std::vector<MPI_Request> send_req(num_sends);
    std::vector<MPI_Status> send_stat(num_sends);
    MPI_Status recv_stat;
    int send_count = 0;
    for (const auto &item : send_pack_data)
      MPI_Isend(item.second.data(),
                item.second.size(),
                GetMpiType<id_size_t>(),
                item.first,
                comm.Rank(),
                comm.RawHandle(),
                &send_req[send_count++]);
    // Step 5: Create the local data-structure to store
    //         the ambiguous faces
    std::map<id_size_t, std::vector<BoundaryFace>> recv_bface_map;
    for (int irecv = 0; irecv < num_recvs; ++irecv) {
      int recv_size;
      MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
      MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
      std::vector<id_size_t> temp_buffer(recv_size);
      MPI_Recv(temp_buffer.data(),
               recv_size,
               GetMpiType<id_size_t>(),
               recv_stat.MPI_SOURCE,
               recv_stat.MPI_TAG,
               comm.RawHandle(),
               &recv_stat);
      // std::cout << "Recv size = " << recv_size << "\n";
      my_size_t my_loc = 0;
      for (;;) {
        // Unpack 1) the boundary element id
        auto my_elm_gid = temp_buffer[my_loc++];
        // fout_recv << my_elm_gid + 1 << " ";
        // std::cout << "eml id = " << my_elm_gid << "\n";
        // 2) Number of boundary faces attached to element
        auto num_attached_bfaces = temp_buffer[my_loc++];
        // std::cout << "num_attached_bfaces = " << num_attached_bfaces << "\n";
        for (decltype(num_attached_bfaces) i = 0; i < num_attached_bfaces;
             ++i) {
          BoundaryFace temp_bface_struct;
          // 3) the face number in the element of the boundary face
          temp_bface_struct.face_id = temp_buffer[my_loc++];
          // fout_recv << temp_bface_struct.face_id << " ";
          // 4) the patch id of the boundary face
          temp_bface_struct.patch_id = temp_buffer[my_loc++];
          recv_bface_map[my_elm_gid].push_back(temp_bface_struct);
        }
        // fout_recv << "\n";
        if (my_loc == temp_buffer.size())
          break;
      }
    }
    MPI_Waitall(num_sends, send_req.data(), send_stat.data());
    // Move from gid map to array
    local_bfaces.resize(global_ids.size());
    std::map<id_size_t, int> gid_to_lid;
    for (decltype(global_ids.size()) i = 0; i < global_ids.size(); ++i)
      gid_to_lid[global_ids[i]] = i;
    // Search in map to find if we received any element that
    // is not in the hash
    for (const auto &item_map : recv_bface_map)
      // for (const auto &face : item_map.second)
      if (gid_to_lid.find(item_map.first) == std::end(gid_to_lid))
        std::cout << "Error: Received a boundary element that does not "
                     "belong to this processor!\n";

    // std::cout << "Sizes match : " << global_ids.size() << " and "
    //           << recv_bface_map.size() << "\n";
    for (const auto &item_map : recv_bface_map)
      for (const auto &face : item_map.second)
        local_bfaces[gid_to_lid[item_map.first]].push_back(face);

#if 0
    cat.str("");
    cat << "check_bface" << comm.Rank() << ".dat";
    std::ofstream fout_check(cat.str());
    for (id_size_t i = 0; i < global_ids.size(); ++i) {
      if (!local_bfaces[i].empty()) {
        fout_check << global_ids[i] + 1 << " ";
        for (int j = 0; j < local_bfaces[i].size(); ++j) {
          fout_check << local_bfaces[i][j].face_id << " ";
        }
        fout_check << "\n";
      }
    }
#endif

    // size_t num_bfaces = 0;
    // for (const auto &item : local_bfaces)
    //   num_bfaces += item.size();
    // for (const auto &item_in_item : item)
    //   num_bfaces++;
    // std::cout << "MeshIO: Total boundary faces formed = " << num_bfaces << "
    // in rank =" << comm.Rank() << "\n";
  }

private:
  bool m_is_2d = false;
  my_size_t m_num_elements[C_NumElementTypes] = {0};        //!<
  my_size_t m_element_offsets[C_NumElementTypes + 1] = {0}; //!<
  my_size_t m_num_nodes = 0;                                //!<
  ElementDHT m_element_connectivity_dht;                    //!<
  PointDHT m_node_dht;                                      //!<
  PointDHT m_centroid_dht;                                  //!<
  ScalarDHT m_volume_dht;                                   //!<
  FloatDHT m_element_weights_dht;                           //!<
  binfo_t m_boundary_info;                                  //!<
  bface_t m_bfaces_info;                                    //!<
  int m_num_patch;                                          //!<
  int m_num_periodic_patches = 0;                           //!<
  std::vector<int> m_periodic_patch_ids;                    //!<
  std::vector<id_size_t> m_periodic_node_pairs;             //!<
  std::pair<Point3d, Point3d> m_AABB; //!< Axis Aligned Bounding-Box
  std::vector<char> m_patch_labels;   //!< Patch label information
}; // End of class Mesh

} // namespace taru
