/** @file el2part.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 17 June 2019
 *  @brief Documentation for el2part.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "hdf5.hpp"
#include <cassert>
#include <cstdio>

namespace taru {

static const int C_MD5CoordSize = 33; //!< The fixed length of MD5 string

/**
 * @brief Function returns the file name of the el2part file as a string
 * @param npart
 * @return std::string
 */
inline std::string GetEl2PartFileName(const int npart) {
  std::stringstream cat;
  cat << "el2part_" << npart << ".h5";
  return cat.str();
}

/**
 * @brief Creates the group name of the local part group
 * @param parent
 * @param part_idx
 * @return std::string
 */
inline std::string GetEl2PartGroupName(hid_t &parent, int part_idx) {
  char bufname[200];
  std::sprintf(bufname, "part%07i", part_idx);
  auto part_group = std::string("/") + std::string(bufname);
  hdf5::CreateGroup(parent, part_group.c_str());
  return part_group;
}

/**
 * @brief Creates the group name of the local part schedule
 * @param parent
 * @param part_idx
 * @return std::string
 */
inline std::string GetScheduleGroupName(hid_t &parent, int part_idx) {
  char bufname[200];
  std::sprintf(bufname, "neighbour%07i", part_idx);
  auto part_group = std::string("/") + std::string(bufname);
  hdf5::CreateGroup(parent, part_group.c_str());
  return part_group;
}

/**
 * @brief Writes the local partition information to the el2part HDF5 file
 * @param parent
 * @param part
 * @param lcl_el_cnt
 * @param el2part
 * @param md5coords
 */
inline void WriteEl2PartLocal(hid_t parent,
                              int part,
                              int lcl_el_cnt,
                              int *el2part,
                              char *md5coords) {
  // compute fortran 1 based partition index(to be
  // consistent with parmetis output)
  auto part_idx = part + 1;
  auto part_group_str = GetEl2PartGroupName(parent, part_idx);
  hdf5::Write(parent, part_group_str, "part_idx", &part_idx, 1);
  hdf5::Write(parent, part_group_str, "lcl_el_cnt", &lcl_el_cnt, 1);
  hdf5::Write(parent, part_group_str, "el_2_part", el2part, lcl_el_cnt);
  // hdf5::Write(parent, part_group_str, "md5coords", md5coords,
  // C_MD5CoordSize);
}

/**
 * @brief Writes the element2partition cache file.
 * @param comm_c C communicator object
 * @param my_rank current process rank in the communicator
 * @param npart number of partitions
 * @param ndim number of space dimensions (2 or 3)
 * @param glb_el_cnt global element count (on all processes)
 * @param el2part pointer to the array of element->partition as returned by
 * parmetis
 * @param el_coords (not used but retained for API compatability)
 */
inline void WriteEl2Part(MPI_Comm comm_c,
                         int my_rank,
                         int npart,
                         int ndim,
                         int glb_el_cnt,
                         std::vector<int> &el2part,
                         double *el_coords) {
  char md5coords[C_MD5CoordSize] = {'*'};
  CommunicatorConcept comm(comm_c, "el2part");
  auto file_name = GetEl2PartFileName(npart);
  hid_t hfile;
  if (comm.Rank() == 0) {
    hfile =
        H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    assert(hfile >= 0);
    auto param_group_str = std::string("/") + std::string("Parameters");
    hdf5::CreateGroup(hfile, param_group_str.c_str());
    hdf5::Write(hfile, param_group_str, "npart", &npart, 1);
    hdf5::Write(hfile, param_group_str, "glb_el_cnt", &glb_el_cnt, 1);
    H5Fclose(hfile);
    comm.Barrier();
  } else {
    comm.Barrier();
  }
  for (rank_t i = 0; i < comm.size(); ++i) {
    if (comm.Rank() == i) {
      hfile = H5Fopen(file_name.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
      WriteEl2PartLocal(hfile, i, el2part.size(), &el2part[0], md5coords);
      assert(hfile >= 0);
      H5Fclose(hfile);
      comm.Barrier();
    } else
      comm.Barrier();
  }
}

/**
 * @brief Write the schedule obtained in the el2part file
 * @param level
 * @param comm
 * @param my_schedule
 * @param filename
 */
inline void WriteSchedule(int level,
                          CommunicatorConcept &comm,
                          const std::vector<rank_t> &my_ranks,
                          const std::vector<rank_t> &my_schedule,
                          const std::string &filename) {
  hid_t hfile;
  auto comm_group_str =
      std::string("/") + std::string("Level_") + std::to_string(level);
  std::vector<rank_t> schedule(my_schedule);
  std::vector<rank_t> branch_ranks(my_ranks);
  /**
   *  Schedule should be FORTRAN indexed
   */
  for (auto &item : schedule)
    item++;
  for (auto &item : branch_ranks)
    item++;
  /**
   * This is needed to prevent dead-lock but
   * I do not know why?
   */
  if (comm.IsValid()) {
    /**
     * Write the master data first (some parameters)
     */
    if (comm.IsMaster()) {
      hfile = H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
      assert(hfile >= 0);
      hdf5::CreateGroup(hfile, comm_group_str.c_str());
    }
    /**
     *  Register all variables in the tuple
     */
    GatherMasterTupleMap my_tuple;
    AddGatherMasterTuple(my_tuple, schedule);
    AddGatherMasterTuple(my_tuple, branch_ranks);

    /**
     * Do the write on the master rank using
     * DoGatherMaster design pattern
     */
    comm.DoGatherMaster(
        my_tuple,
        /**
         * Post receive lambda write the info to file
         * @param part
         * @param my_tuple
         * @note Capture the hfile and group string
         */
        [&hfile, &comm_group_str](int part, GatherMasterTupleMap &my_tuple) {
          part = part + 1;
          auto dset_name =
              comm_group_str + "/NeighboursOfBranch" + std::to_string(part);
          int schedule_size = SizeFunction(my_tuple, schedule);
          auto schedule_ptr = reinterpret_cast<rank_t *>(
              RawAccessFunction(my_tuple, schedule, 0));
          hdf5::Write(hfile, dset_name.c_str(), schedule_ptr, schedule_size);
          dset_name = comm_group_str + "/LeafsOfBranch" + std::to_string(part);
          int branch_ranks_size = SizeFunction(my_tuple, branch_ranks);
          auto branch_ranks_ptr = reinterpret_cast<rank_t *>(
              RawAccessFunction(my_tuple, branch_ranks, 0));
          hdf5::Write(
              hfile, dset_name.c_str(), branch_ranks_ptr, branch_ranks_size);
        },
        /**
         * Empty post send
         * @param my_tuple
         */
        [](GatherMasterTupleMap &my_tuple) {});
    /**
     *  Close the hdf5 file in master rank
     */
    if (comm.IsMaster())
      H5Fclose(hfile);
  }
}

} // namespace taru
