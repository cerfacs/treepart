/**
 * @file libtreeadapt.cpp
 * @author Pavanakumar Mohanamuraly (mkumar@cerfacs.fr)
 * @brief Documentation for libtreeadapt.cpp
 * @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#include "libtreeadapt.h"
#include "adapt_helper.hpp"
#include "colouring.hpp"
#include "el2part.hpp"
#include "mesh.hpp"
#include "mesh_reader_pointer.hpp"
#include "solution_field.hpp"
#include <ostream>
#include <sys/resource.h>

using namespace taru;

/* Some typedef short-cuts */
using HybridTreeComm = TreeCommunicator<HybridPayload>;
using MeshReader = PointerMeshReader<int, CommunicatorConcept>;
using HybridTreeCommPtr = std::unique_ptr<HybridTreeComm>;
using MeshPtr = std::unique_ptr<Mesh>;
using MeshReaderPtr = std::unique_ptr<MeshReader>;
using CommunicatorConceptPtr = std::unique_ptr<CommunicatorConcept>;
using HipMeshReaderPtr = std::unique_ptr<HipMeshReader>;
using SolutionFieldPtr = std::unique_ptr<SolutionField>;

/* Global variables of the interface */
HybridTreeCommPtr libtp_tree_world;
AdaptOptions libtp_options;
std::vector<SolutionFieldInterface> libtp_external_solution;
SolutionFieldPtr libtp_solution;
MeshReaderPtr libtp_avbp_mesh;
MeshPtr libtp_mesh;
CommunicatorConceptPtr libtp_world;
AdaptMetaData libtp_adapt_meta;

/**
 * Initializes the Zoltan library with the given arguments and communicator.
 *
 * @param nargs The number of command-line arguments.
 * @param args An array of command-line arguments.
 * @param communicator The communicator object used for communication.
 */
void InitZoltan(int nargs, char *args[], CommunicatorConcept &communicator) {
  float zoltan_version;
  int rc = Zoltan_Initialize(nargs, args, &zoltan_version);
  if (rc != ZOLTAN_OK) {
    if (communicator.IsMaster())
      std::cerr << "Failed to initialize Zoltan -- sorry...\n";
    MPI_Finalize();
    std::exit(0);
  }
  if (communicator.IsMaster()) {
    std::cout << "Message: Zoltan initialised with ZOLTAN_OK "
              << "and found {version: " << zoltan_version << "}\n";
    std::cout << "Message: sizeof(id_size_t) = " << sizeof(id_size_t) << "\n";
    std::cout << "Message: sizeof(t_index) = " << sizeof(t_index) << "\n";
    std::cout << "Message: sizeof(t_scalar) = " << sizeof(t_scalar) << "\n";
    std::cout << "Message: sizeof TreePart real = " << sizeof(double) << "\n";
    // std::cout << "Message: sizeof ParMetis real_t = " << sizeof(real_t) <<
    // "\n"; std::cout << "Message: sizeof ParMetis idx_t = " << sizeof(idx_t)
    // << "\n";
  }
}

/**
 * @brief Copy the AVBP mesh into our TreePart payload structure
 *
 * @param numdims   : Mesh dimension to partition (2D/3D)
 * @param lcl_cnt   : Local element count
 * @param vstart    : The starting offset of element distribution
 * @param el_coords : The nodal coordinates
 * @param eptr      : Is the element row pointer in FORTRAN offset
 * @param eind      : Is the element column pointer in FORTRAN offset
 * @param payload
 */
void CopyToPayload(t_index numdims,
                   t_index lcl_cnt,
                   t_index vstart,
                   t_scalar *el_coords,
                   t_index *eptr,
                   t_index *eind,
                   HybridPayload &payload) {
  /**
   * Copy global indices to the payload
   */
  payload.global_ids.clear();
  payload.global_ids.shrink_to_fit();
  payload.global_ids.resize(lcl_cnt);
  for (t_index i = 0; i < lcl_cnt; ++i)
    payload.global_ids[i] = i + vstart;
  /**
   *  Copy coordinates x,y
   *  Copy z if 3D otherwise zero init
   */
  payload.coordinates.clear();
  payload.coordinates.shrink_to_fit();
  payload.coordinates.resize(lcl_cnt);
  for (t_index i = 0; i < lcl_cnt; ++i)
    payload.coordinates[i].x = el_coords[numdims * i];
  for (t_index i = 0; i < lcl_cnt; ++i)
    payload.coordinates[i].y = el_coords[numdims * i + 1];
  if (numdims == 3) {
    for (t_index i = 0; i < lcl_cnt; ++i)
      payload.coordinates[i].z = el_coords[numdims * i + 2];
  } else {
    for (t_index i = 0; i < lcl_cnt; ++i)
      payload.coordinates[i].z = 0.0;
  }
  /**
   * @todo Implement copying of element connectivity info
   * and weights from api call
   */
  payload.connectivity.clear();
  payload.connectivity.shrink_to_fit();
  payload.connectivity.resize(lcl_cnt);
  for (t_index i = 0; i < lcl_cnt; ++i) {
    t_index j = eptr[i] - 1;
    t_index k = 0;
    t_index j_end = eptr[i + 1] - 1;
    for (; j < j_end; ++j, ++k)
      payload.connectivity[i].nodes[k] = eind[j];
  }
  payload.weights.clear();
  payload.weights.shrink_to_fit();
  payload.weights.resize(lcl_cnt, 1.0);
}

/**
 * @brief Create the local permutation data
 * @details The local permutation array to renumber
 *          and reorder the nodes such that the unique
 *          nodes owned by the rank come first and the
 *          rest are non-owned shared nodes.
 */
void CreateLocalPermutations() {
  auto num_nodes = libtp_adapt_meta.owned_map.size();
  num_nodes += libtp_adapt_meta.spill_over_map.size();
  libtp_adapt_meta.local_perm.resize(num_nodes);
  libtp_adapt_meta.local_perm.shrink_to_fit();
  libtp_adapt_meta.local_iperm.resize(num_nodes);
  libtp_adapt_meta.local_iperm.shrink_to_fit();

  std::map<id_size_t, id_size_t> inv_owned_map;
  std::map<id_size_t, id_size_t> inv_owned_map1;
  unsigned count = 0;
  for (const auto &item : libtp_adapt_meta.owned_map) {
    inv_owned_map[item.second] = item.first;
    inv_owned_map1[item.second] = count;
    count++;
  }
  for (const auto &item : libtp_adapt_meta.spill_over_map) {
    inv_owned_map[item.second] = item.first;
    inv_owned_map1[item.second] = count;
    count++;
  }
  // Loop over local ordering that is passed to avbp
  for (const auto &item : inv_owned_map)
    libtp_adapt_meta.local_perm[inv_owned_map1[item.first]] = item.second;
  // Form inverse permutation for renumbering
  for (unsigned i = 0; i < num_nodes; ++i)
    libtp_adapt_meta.local_iperm[libtp_adapt_meta.local_perm[i]] = i;
}

void libtreepart_avbp(t_index *elmdist,
                      t_index *eptr,
                      t_index *eind,
                      t_index *part,
                      t_index numflag,
                      t_scalar *el_coords,
                      int lcl_cnt,
                      int numdims,
                      t_index num_parts,
                      MPI_Comm comm,
                      t_index *dg_ptr,
                      t_index *dg_graph) {
  Time(global_timer::TIME_OVERALL, global_timer::C_TimerStart);
  /**
   *  Create the root communicator
   */
  CommunicatorConcept world(comm, "tree_root");
  if (world.IsMaster())
    std::cout << C_GitVersionString;
  InitZoltan(0, NULL, world);
  TreeCommunicator<HybridPayload> input_tree(world);
  /*if (input_tree.Root().IsMaster()) {
    std::cout << "Message: First element of eptr : [" << eptr[0] << "]\n";
    std::cout << "Message: First element of eind : [" << eind[0] << "]\n";
    std::cout << "Message: Element Size Distribution : ";
    for (decltype(num_parts) i = 0; i < num_parts; ++i)
      std::cout << "         Rank " << i
                << " : size = " << elmdist[i + 1] - elmdist[i] << "\n";
  }*/

  std::vector<int> topology_info;
  /**
   *  Detect topology using HWLOC, print HWLOC info
   *  and create levels (disable hardware detection)
   */
  // CommunicatorConcept::GetTopologyInfo(topology_info);
  // if (input_tree.Root().IsMaster()) {
  //   std::cout << "Message: Detected Topology : [  ";
  //   for (auto level : topology_info)
  //     std::cout << level << "  ";
  //   std::cout << "]\n";
  // }
  topology_info.push_back(1);
  for (auto level : topology_info)
    input_tree.AddLevel(level, partition_method::METIS);
  for (auto &item : input_tree)
    item.Payload().SetDimension(numdims);

  /**
   *  Bootstrap lambda function to supply to the
   *  Run() method of tree-partitioner
   */
  auto bootstrap_lambda =
      [&numdims, &lcl_cnt, &elmdist, &el_coords, &eptr, &eind](
          TreeCommunicator<HybridPayload> &input_tree, int begin_from) {
        Time(global_timer::TIME_BOOTSTRAP, global_timer::C_TimerStart);
        if (input_tree.Root().IsMaster())
          std::cout << "Message: Copying payload ... ";
        CopyToPayload(numdims,
                      lcl_cnt,
                      elmdist[input_tree.Root().Rank()],
                      el_coords,
                      eptr,
                      eind,
                      input_tree[begin_from].Payload());
        if (input_tree.Root().IsMaster())
          std::cout << "Done!\n";
        /**
         *  Aggregate to the top most level for bootstrap (only if hierarchical)
         *  for flat topology no need to aggregate
         */
        if (input_tree.size() > 1) {
          input_tree.Aggregate(begin_from,
                               input_tree[begin_from].Payload().coordinates);
          input_tree.Aggregate(begin_from,
                               input_tree[begin_from].Payload().global_ids);
          input_tree.Aggregate(begin_from,
                               input_tree[begin_from].Payload().weights);
          input_tree.Aggregate(begin_from,
                               input_tree[begin_from].Payload().connectivity);
        }
        if (input_tree.Root().IsMaster())
          std::cout << "Message: Boostrap complete!\n";
        Time(global_timer::TIME_BOOTSTRAP, global_timer::C_TimerStop);
      };

  /**
   * Now create a distributed hash table of part index in order to
   * obtain the part number (all reduce is necessary for sync)
   * @todo investigate need for this sync
   */
  DistributedHashTable<id_size_t, rank_t> part_dht(input_tree.Root());
  decltype(lcl_cnt) total_entities = lcl_cnt;
  MPI_Allreduce(&lcl_cnt,
                &total_entities,
                1,
                GetMpiType<decltype(lcl_cnt)>(),
                MPI_SUM,
                input_tree.Root().RawHandle());
  part_dht.SetSize(total_entities);

  PartitionRunner<HybridPayload> partition_runner;
  partition_runner.Run(input_tree, bootstrap_lambda);

  /**
   *  Create the keys and update the hash
   */
  auto myrank = input_tree.Root().Rank();
  std::vector<rank_t> new_parts(input_tree[0].Payload().global_ids.size());
  std::fill(new_parts.begin(), new_parts.end(), myrank);
  part_dht.Populate(input_tree[0].Payload().global_ids, new_parts);

  /**
   *  Use the part hash table and find the part id of keys
   *  Note: AVBP expects (1 <= part <= num_parts)
   */
  std::vector<id_size_t> keys(lcl_cnt);
  std::vector<rank_t> find_parts(lcl_cnt);

  for (decltype(lcl_cnt) i = 0; i < lcl_cnt; ++i)
    keys[i] = i + elmdist[myrank];
  part_dht.GetDataFromKeys(keys, find_parts);
  for (auto &item : find_parts)
    item++;
  std::copy(find_parts.begin(), find_parts.end(), part);

  /**
   *  Write the El2Part HDF5 file
   */
  taru::WriteEl2Part(input_tree.Root().RawHandle(),
                     input_tree.Root().Rank(),
                     input_tree.Root().size(),
                     numdims,
                     elmdist[num_parts] - elmdist[0],
                     find_parts,
                     nullptr);
  find_parts.clear();
  find_parts.shrink_to_fit();

  /**
   *  Print timing information to user on screen
   */
  Time(global_timer::TIME_OVERALL, global_timer::C_TimerStop);
  input_tree.Root().AllReduceMax(global_timer::C_GlobalTimerValues,
                                 global_timer::TIME_ARRAY_SIZE);
  global_timer::PrintTimingStats(input_tree.Root().Rank());
  global_timer::ClearTimers();
}

void libtreeadapt_copy_comm(MPI_Fint *fcomm, int *comm_size, int *my_rank) {
  *fcomm = MPI_Comm_c2f(libtp_world->RawHandle());
  *comm_size = libtp_world->size();
  *my_rank = libtp_world->Rank();
}

void init_libtreeadapt(int *num_dims, MPI_Fint *fcomm) {
  Time(global_timer::TIME_OVERALL, global_timer::C_TimerStart);
  MPI_Comm comm;
  comm = MPI_Comm_f2c(*fcomm);
  /**
   *  Create the root communicator
   */
  libtp_world =
      CommunicatorConceptPtr(new CommunicatorConcept(comm, "tree_root"));
  auto &world = *libtp_world;
  if (world.IsMaster())
    std::cout << C_GitVersionString;
  InitZoltan(0, NULL, world);
  libtp_tree_world = HybridTreeCommPtr(new HybridTreeComm(world));
  libtp_avbp_mesh = MeshReaderPtr(new MeshReader(world));
  libtp_solution = SolutionFieldPtr(new SolutionField());

  auto &input_tree = *libtp_tree_world;
  std::vector<int> topology_info;
  /**
   *  Detect topology using HWLOC, print HWLOC info
   *  and create levels (disable hardware detection)
   */
  topology_info.push_back(1);
  for (auto level : topology_info)
    input_tree.AddLevel(level, partition_method::METIS);
  for (auto &item : input_tree)
    item.Payload().SetDimension(*num_dims);
  libtp_world->Barrier();
}

void close_libtreeadapt() {
  auto &input_tree = *libtp_tree_world;
  /**
   *  Print timing information to user on screen
   */
  Time(global_timer::TIME_OVERALL, global_timer::C_TimerStop);
  input_tree.Root().AllReduceMax(global_timer::C_GlobalTimerValues,
                                 global_timer::TIME_ARRAY_SIZE);
  // global_timer::PrintTimingStats(input_tree.Root().Rank());
  global_timer::ClearTimers();
  if (libtp_avbp_mesh != nullptr) {
    auto ptr = libtp_avbp_mesh.release();
    delete ptr;
  }
  if (libtp_mesh != nullptr) {
    auto ptr = libtp_mesh.release();
    delete ptr;
  }
  libtp_external_solution.clear();
  libtp_external_solution.shrink_to_fit();
  if (libtp_solution != nullptr) {
    auto ptr = libtp_solution.release();
    delete ptr;
  }
  if (libtp_tree_world != nullptr) {
    auto ptr = libtp_tree_world.release();
    delete ptr;
  }
  if (libtp_world != nullptr) {
    auto ptr = libtp_world.release();
    delete ptr;
  }
}

void libtreeadapt_set_nodes(int *nnodes,
                            int *mynode,
                            int *nodelist,
                            double *x) {
  libtp_avbp_mesh->m_nnode = *nnodes;
  libtp_avbp_mesh->m_mynode = *mynode;
  libtp_avbp_mesh->m_nodelist = nodelist;
  libtp_avbp_mesh->m_node_x = x;
  libtp_world->Barrier();
}

void libtreeadapt_allocate_bfaces(int *npatches) {
  libtp_avbp_mesh->m_nbfaces.resize(*npatches);
  libtp_avbp_mesh->m_bface_elm.resize(*npatches);
  libtp_avbp_mesh->m_bface_no.resize(*npatches);
}

void libtreeadapt_set_bfaces(int *patch_no,
                             int *nbfaces,
                             int *bface_elm,
                             int *bface_no) {
  if (*patch_no <= libtp_avbp_mesh->m_npatch && *patch_no > 0) {
    libtp_avbp_mesh->m_nbfaces[*patch_no - 1] = *nbfaces;
    libtp_avbp_mesh->m_bface_elm[*patch_no - 1] = bface_elm;
    libtp_avbp_mesh->m_bface_no[*patch_no - 1] = bface_no;
  }
}

void libtreeadapt_set_triangles(int *ntrielm, int *trielm) {
  libtp_avbp_mesh->m_ntrielm = *ntrielm;
  libtp_avbp_mesh->m_trielm = trielm;
  libtp_world->Barrier();
}

void libtreeadapt_set_tets(int *ntetelm, int *tetelm) {
  libtp_avbp_mesh->m_ntetelm = *ntetelm;
  libtp_avbp_mesh->m_tetelm = tetelm;
  libtp_world->Barrier();
}

void libtreeadapt_set_bnodes(int *npatch,
                             int *nbnodes,
                             int *bnode_begin,
                             int *bnode_size,
                             int *bnode_to_node) {
  libtp_avbp_mesh->m_npatch = *npatch;
  libtp_avbp_mesh->m_nbnodes = *nbnodes;
  libtp_avbp_mesh->m_bnode_begin = bnode_begin;
  libtp_avbp_mesh->m_bnode_size = bnode_size;
  libtp_avbp_mesh->m_bnode_to_node = bnode_to_node;
  libtp_world->Barrier();
}

void libtreeadapt_option_mmg_verbosity(int *mmg_verbosity) {
  libtp_options.m_verbosity = *mmg_verbosity;
}

void libtreeadapt_option_dowrite(int *fbool) {
  libtp_options.m_write_final = true;
  if (*fbool == 0) {
    libtp_options.m_write_intermediate = false;
  } else {
    libtp_options.m_write_intermediate = true;
  }
  libtp_world->Barrier();
}

void libtreeadapt_option_hGrad(double *hGrad) {
  libtp_options.m_mmg_hGrad = *hGrad;
  libtp_world->Barrier();
}

void libtreeadapt_option_hausdorff(double *hausdorff) {
  libtp_options.m_mmg_hausdorff = *hausdorff;
  libtp_world->Barrier();
}

void libtreeadapt_option_hmin(double *hmin) {
  libtp_options.m_mmg_hmin = *hmin;
  libtp_world->Barrier();
}

void libtreeadapt_option_hmax(double *hmax) {
  libtp_options.m_mmg_hmax = *hmax;
  libtp_world->Barrier();
}

void libtreeadapt_option_maxIter(int *maxIter) {
  libtp_options.m_mmg_max_iter = *maxIter;
  libtp_world->Barrier();
}

void libtreeadapt_option_maxAdaptThreshold(double *adapt_threshold_max) {
  libtp_options.m_adapt_threshold_max = *adapt_threshold_max;
  libtp_world->Barrier();
}

void libtreeadapt_option_minAdaptThreshold(double *adapt_threshold_min) {
  libtp_options.m_adapt_threshold_min = *adapt_threshold_min;
  libtp_world->Barrier();
}

void libtreeadapt_set_ndim(int *num_dim) {
  auto &input_tree = *libtp_tree_world;
  for (auto &item : input_tree)
    item.Payload().SetDimension(*num_dim);
  if (*num_dim == 2)
    libtp_avbp_mesh->m_is2d = true;
  else
    libtp_avbp_mesh->m_is2d = false;
  libtp_world->Barrier();
}

void libtreeadapt_option_interpCheck(int *do_check) {
  if (*do_check == 0)
    libtp_options.do_interpolation_check = false;
  else
    libtp_options.do_interpolation_check = true;
  libtp_world->Barrier();
}

void libtreeadapt_option_interpType(int *interp_type) {
  switch (*interp_type) {
  case least_squares_interpol:
    libtp_options.m_adapt_interp_type = least_squares_interpol;
    break;

  default:
  case barycentric_interpol:
    libtp_options.do_interpolation_check = true;
    break;
  }
  libtp_world->Barrier();
}

void libtreeadapt_option_deviation_weights(int *fbool) {
  if (*fbool == 0)
    libtp_options.use_deviation_weights = false;
  else
    libtp_options.use_deviation_weights = true;
  libtp_world->Barrier();
}

void libtreeadapt_get_nnode(int *node_count) {
  *node_count = libtp_adapt_meta.owned_map.size() +
                libtp_adapt_meta.spill_over_map.size();
  libtp_world->Barrier();
}

void libtreeadapt_get_nodes(double *node_coords, int *node_global_indices) {
  // inverse node map
  std::map<id_size_t, id_size_t> inv_owned_map;
  std::map<id_size_t, id_size_t> inv_owned_map1;
  unsigned count = 0;
  for (const auto &item : libtp_adapt_meta.owned_map) {
    inv_owned_map[item.second] = item.first;
    inv_owned_map1[item.second] = count;
    count++;
  }
  for (const auto &item : libtp_adapt_meta.spill_over_map) {
    inv_owned_map[item.second] = item.first;
    inv_owned_map1[item.second] = count;
    count++;
  }
  int ndim = (libtp_avbp_mesh->m_is2d == true) ? 2 : 3;
  // Copy coordinates and global node ids
  for (const auto &item : inv_owned_map) {
    node_coords[inv_owned_map1[item.first] * ndim] =
        libtp_adapt_meta.local_xyz[item.second].x;
    node_coords[inv_owned_map1[item.first] * ndim + 1] =
        libtp_adapt_meta.local_xyz[item.second].y;
    if (ndim == 3)
      node_coords[inv_owned_map1[item.first] * ndim + 2] =
          libtp_adapt_meta.local_xyz[item.second].z;
    // Note the index are Fortran indexed 1 is the starting index
    node_global_indices[inv_owned_map1[item.first]] = item.first;
  }
  libtp_adapt_meta.local_xyz.clear();
  libtp_adapt_meta.local_xyz.shrink_to_fit();
  libtp_world->Barrier();
}

void libtreeadapt_num_elements(int *nelm) {
  [[maybe_unused]] const int ELEMENT_TYPE_TRI = 0;     // 1
  [[maybe_unused]] const int ELEMENT_TYPE_QUAD = 1;    // 2
  [[maybe_unused]] const int ELEMENT_TYPE_TETRA = 2;   // 3
  [[maybe_unused]] const int ELEMENT_TYPE_PYRAMID = 3; // 4
  [[maybe_unused]] const int ELEMENT_TYPE_PRISM = 4;   // 5
  [[maybe_unused]] const int ELEMENT_TYPE_HEXA = 5;    // 6
  for (int i = 0; i < ELEMENT_TYPE_HEXA + 1; ++i)
    nelm[i] = 0;
  int mytype =
      (libtp_avbp_mesh->m_is2d == true) ? ELEMENT_TYPE_TRI : ELEMENT_TYPE_TETRA;
  int nelmnod = (libtp_avbp_mesh->m_is2d == true) ? 3 : 4;
  nelm[mytype] = libtp_adapt_meta.local_elmconn.size() / nelmnod;
  libtp_world->Barrier();
}

void libtreeadapt_get_element_node_conn(int *elmnodptr) {
  const auto &conn = libtp_adapt_meta.local_elmconn;
  for (unsigned i = 0; i < conn.size(); ++i)
    elmnodptr[i] = libtp_adapt_meta.local_iperm[conn[i] - 1] + 1; // FtoCtoF
  libtp_adapt_meta.local_elmconn.clear();
  libtp_adapt_meta.local_elmconn.shrink_to_fit();
  libtp_world->Barrier();
}

void libtreeadapt_get_num_unique_nodes(int *num_owned) {
  *num_owned = libtp_adapt_meta.owned_map.size();
  libtp_world->Barrier();
}

void libtreeadapt_get_global_nnodes(int *num_nodes) {
  *num_nodes = libtp_mesh->GetNumNodes();
  libtp_world->Barrier();
}

void libtreeadapt_get_num_ngbr_procs(int *num_shared_rank) {
  std::set<int> unique_shared_ranks;
  for (const auto &bnode : libtp_adapt_meta.adapt_binfo) {
    for (const auto &ibc : bnode) {
      if (ibc < 0) {
        unique_shared_ranks.insert(ibc);
      }
    }
  }
  *num_shared_rank = unique_shared_ranks.size();
  libtp_world->Barrier();
}

void libtreeadapt_get_shared_node_count(int *shared_node_count_ptr) {
  // int &shared_node_count = *shared_node_count_ptr;
  // shared_node_count = 0;
  // for (const auto &bnode : libtp_adapt_meta.adapt_binfo) {
  //   // Check if any of the boundary id is shared
  //   bool is_shared = false;
  //   for (const auto &ibc : bnode)
  //     if (ibc < 0)
  //       is_shared = true;
  //   // only count if a shared boundary id was found
  //   if (is_shared == true)
  //     shared_node_count++;
  // }
  std::set<int> unique_shared_ids;
  int i = 0;
  for (const auto &binfo : libtp_adapt_meta.adapt_binfo) {
    for (const auto &ibc : binfo) {
      if (ibc < 0) {
        unique_shared_ids.insert(i + 1);
      }
    }
    i++;
  }
  *shared_node_count_ptr = unique_shared_ids.size();
  libtp_world->Barrier();
}

void libtreeadapt_get_shared_nodes(int *shared_lids) {
  std::set<int> shared_node_lids;
  for (decltype(libtp_adapt_meta.adapt_binfo.size()) i = 0;
       i < libtp_adapt_meta.adapt_binfo.size();
       ++i)
    for (const auto &ibc : libtp_adapt_meta.adapt_binfo[i])
      if (ibc < 0)
        shared_node_lids.insert(libtp_adapt_meta.local_iperm[i] + 1);

  std::copy(shared_node_lids.begin(), shared_node_lids.end(), shared_lids);
  libtp_world->Barrier();
}

void libtreeadapt_get_patch_count(int *patch_cnt, int *bnode_cnt) {
  // get the global patch count
  (*patch_cnt) = libtp_mesh->GetNumPatches();
  // get bnode count of patches
  (*bnode_cnt) = 0;
  for (const auto &item : libtp_adapt_meta.adapt_binfo) {
    for (const auto &ibc : item) {
      if (ibc > 0) {
        (*bnode_cnt)++;
      }
    }
  }
  libtp_world->Barrier();
}

void libtreeadapt_get_bnode_data(int *patch_cnt, int *bnodes_lidx) {
  // Form the location offset to put the lidx at the right location
  std::vector<int> bnode_loc(libtp_mesh->GetNumPatches() + 1);
  for (const auto &item : libtp_adapt_meta.adapt_binfo) {
    for (const auto &ibc : item) {
      if (ibc > 0) {
        bnode_loc[ibc]++;
      }
    }
  }

  // Convert count to offset
  for (int i = 1; i < libtp_mesh->GetNumPatches() + 1; ++i) {
    bnode_loc[i] += bnode_loc[i - 1];
    patch_cnt[i - 1] = 0;
  }

  // Loop over all nodes and correctly place the bnode in the right location
  int count = 0;
  for (const auto &bnode : libtp_adapt_meta.adapt_binfo) {
    // Loop over all boundary patch information
    for (const auto &pid : bnode) {
      // Only look for physical patches (not processor patches)
      if (pid > 0) {
        // In the offset array start from 0 to know the exact location
        auto &myloc = bnode_loc[pid - 1];
        bnodes_lidx[myloc++] = libtp_adapt_meta.local_iperm[count] + 1;
        patch_cnt[pid - 1]++;
      }
    }
    count++;
  }
  libtp_world->Barrier();
}

void libtreeadapt_get_ndim(int *num_dim) {
  *num_dim = 3;
  if (libtp_avbp_mesh->m_is2d == true)
    *num_dim = 2;
  libtp_world->Barrier();
}

void libtreeadapt_get_deviation(int *nnodes, double *deviation) {
  for (int i = 0; i < *nnodes; ++i)
    deviation[i] = libtp_adapt_meta.deviation[libtp_adapt_meta.local_perm[i]];
  libtp_world->Barrier();
}

void libtreeadapt_print_opt() {
  if (libtp_world->IsMaster()) {
    std::cout << "==============   KalpaTARU ADAPT OPTIONS ===============\n";
    std::cout << "  Write Intermediate mesh  : "
              << libtp_options.m_write_intermediate << "\n";
    std::cout << "  Minimum edge length      : " << libtp_options.m_mmg_hmin
              << "\n";
    std::cout << "  Maximum edge length      : " << libtp_options.m_mmg_hmax
              << "\n";
    std::cout << "  Maximum adapt iterations : " << libtp_options.m_mmg_max_iter
              << "\n";
    std::cout << "  Minimum adapt threshold  : "
              << libtp_options.m_adapt_threshold_min << "% \n";
    std::cout << "  Maximum adapt threshold  : "
              << libtp_options.m_adapt_threshold_max << "% \n";
    std::cout << "  Graduation               : " << libtp_options.m_mmg_hGrad
              << "\n";
    std::cout << "  Hausdorff                : "
              << libtp_options.m_mmg_hausdorff << "\n";
    std::cout << "==============================================\n";
  }
  libtp_world->Barrier();
}

void libtreeadapt_reset_solution() {
  libtp_external_solution.clear();
  libtp_external_solution.shrink_to_fit();
  // First element in the solution field is always the target metric field
  libtp_external_solution.push_back(SolutionFieldInterface(0, 1, nullptr));
  libtp_world->Barrier();
}

void libtreeadapt_add_solution(int *offset, int *stride, double *var) {
  libtp_external_solution.push_back(
      SolutionFieldInterface(*offset, *stride, var));
  libtp_world->Barrier();
}

void libtreeadapt_get_solution(int *n,
                               int *nsol,
                               int *offset,
                               int *stride,
                               int *gids,
                               double *var) {
  std::vector<double> t_var(*n);
  std::vector<id_size_t> keys(*n);
  for (int i = 0; i < *n; ++i)
    keys[i] = gids[i] - 1;
  libtp_solution->m_solution_dht[*nsol].GetDataFromKeys(keys, t_var);
  for (int i = 0; i < *n; ++i)
    var[i * (*stride) + (*offset)] = t_var[i];
  libtp_world->Barrier();
}

void libtreeadapt_get_iso_metric(int *n, int *gids, double *metric) {
  std::vector<double> t_var(*n);
  std::vector<id_size_t> keys(*n);
  for (int i = 0; i < *n; ++i)
    keys[i] = gids[i] - 1;
  libtp_solution->m_metric_dht[0].GetDataFromKeys(keys, t_var);
  for (int i = 0; i < *n; ++i)
    metric[i] = t_var[i];
  libtp_world->Barrier();
}

void libtreeadapt_iso_metric(double *metric) {
  libtp_external_solution[0] = (SolutionFieldInterface(0, 1, metric));
  libtp_world->Barrier();
}

void libtreeadapt_mesh_from_file() {
  HipMeshReader reader(libtp_avbp_mesh->m_mesh_filename,
                       libtp_tree_world->Root());
  libtp_mesh = MeshPtr(new Mesh(reader, libtp_tree_world->Root()));
  libtp_world->Barrier();
}

void libtreeadapt_mesh_from_ptr() {
  libtp_mesh = MeshPtr(new Mesh(*libtp_avbp_mesh, libtp_tree_world->Root()));
  libtp_world->Barrier();
}

void libtreeadapt_mesh_filename(int *name_len, char *name) {
  std::vector<char> temp_name(*name_len + 1);
  for (int i = 0; i < *name_len; ++i)
    temp_name[i] = name[i];
  auto t_str = std::string(temp_name.data(), 0, *name_len);
  libtp_avbp_mesh->m_mesh_filename = t_str;
  libtp_world->Barrier();
}

void libtreeadapt_allocate_solution() {
  auto num_solution_vars = libtp_external_solution.size() - 1;
  libtp_solution->m_solution_dset.resize(num_solution_vars);
  libtp_solution->m_solution_groups.resize(1);
  libtp_solution->m_solution_groups[0] = "Solution";
  libtp_solution->m_solution_dht.resize(num_solution_vars,
                                        ScalarDHT(*libtp_world));
  // Allocate metric hash table
  libtp_solution->m_metric_dht.resize(1, ScalarDHT(*libtp_world));
  for (decltype(num_solution_vars) i = 0; i < num_solution_vars; ++i) {
    libtp_solution->m_solution_dset[i] = "Var_" + std::to_string(i);
  }
  libtp_world->Barrier();
}

void libtreeadapt_copy_external_solution() {
  auto &num_nodes = libtp_avbp_mesh->m_nnode;
  auto &node_list = libtp_avbp_mesh->m_nodelist;
  std::vector<id_size_t> keys(num_nodes);
  std::vector<double> data(num_nodes);
  for (decltype(libtp_avbp_mesh->m_nnode) i = 0; i < num_nodes; ++i)
    keys[i] = node_list[i] - 1;

  for (int i = 1; i < int(libtp_external_solution.size()); ++i) {
    // Make sure dictionary is of correct size
    if (libtp_solution->m_solution_dht[i - 1].size() == 0)
      libtp_solution->m_solution_dht[i - 1].SetSize(libtp_mesh->GetNumNodes());
    // Copy solution data
    auto &data_ptr = libtp_external_solution[i].m_data_ptr;
    auto &offset = libtp_external_solution[i].m_offset;
    auto &stride = libtp_external_solution[i].m_stride;
    for (int j = 0; j < num_nodes; ++j)
      data[j] = data_ptr[offset + stride * j];
    libtp_solution->m_solution_dht[i - 1].Populate(keys, data);
  }
  libtp_world->Barrier();

  // Metric field init
  auto i = 0;
  if (libtp_solution->m_metric_dht[i].size() == 0)
    libtp_solution->m_metric_dht[i].SetSize(libtp_mesh->GetNumNodes());
  auto &offset = libtp_external_solution[i].m_offset;
  auto &stride = libtp_external_solution[i].m_stride;
  for (int j = 0; j < num_nodes; ++j)
    data[j] = libtp_external_solution[i].m_data_ptr[offset + stride * j];
  libtp_solution->m_metric_dht[i].Populate(keys, data);
  libtp_world->Barrier();
}

void libtreeadapt_copy_node_coordinates() {
  auto &num_nodes = libtp_avbp_mesh->m_nnode;
  auto &node_list = libtp_avbp_mesh->m_nodelist;
  auto &data_ptr = libtp_avbp_mesh->m_node_x;
  std::vector<id_size_t> keys(num_nodes);
  std::vector<Point3d> data(num_nodes);
  const auto stride = (libtp_avbp_mesh->m_is2d == true) ? 2 : 3;
  for (decltype(libtp_avbp_mesh->m_nnode) i = 0; i < num_nodes; ++i) {
    keys[i] = node_list[i] - 1;
    data[i].x = data_ptr[stride * i];
    data[i].y = data_ptr[stride * i + 1];
    if (libtp_avbp_mesh->m_is2d != true)
      data[i].z = data_ptr[stride * i + 2];
  }
  libtp_mesh->GetNodeHashTable().Populate(keys, data);
  libtp_world->Barrier();
}

void libtreeadapt_copy_external_iso_metric() {
  auto &num_nodes = libtp_avbp_mesh->m_nnode;
  auto &node_list = libtp_avbp_mesh->m_nodelist;
  std::vector<id_size_t> keys(num_nodes);
  std::vector<double> data(num_nodes);
  for (int i = 0; i < num_nodes; ++i)
    keys[i] = node_list[i] - 1;

  // Metric field init
  auto i = 0;
  auto &offset = libtp_external_solution[i].m_offset;
  auto &stride = libtp_external_solution[i].m_stride;
  for (int j = 0; j < num_nodes; ++j)
    data[j] = libtp_external_solution[i].m_data_ptr[offset + stride * j];
  libtp_solution->m_metric_dht[i].Populate(keys, data);
  libtp_world->Barrier();
}

void libtreeadapt_adapt_loop() {
  libtp_options.adapt_step_counter++;
  libtp_world->Barrier();

  // The partitioner cycling
  std::vector<partition_method> topology_partition_method;

  bool do_bootstrap = true;
  libtp_options.use_interface_weights = true;
  libtp_options.use_interface_edge_weights = true;

  if (libtp_options.use_interface_edge_weights) {
    // Edge weights only compatible with GRAPH method
    topology_partition_method.emplace_back(partition_method::METIS);
    topology_partition_method.emplace_back(partition_method::METIS);
    // Make sure interface weights are enabled
    libtp_options.use_interface_weights = true;
  } else {
    topology_partition_method.emplace_back(partition_method::RIB);
    topology_partition_method.emplace_back(partition_method::METIS);
  }

  auto &i = libtp_options.m_mmg_cur_iter;
  for (i = 0; i < libtp_options.m_mmg_max_iter; ++i) {
    if (libtp_world->IsMaster())
      std::cout << "Message: Treeadapt step #"
                << libtp_options.adapt_step_counter + 1 << " loop #" << i + 1
                << "\n";
    // Cycle b/w GRAPH and RIB
    libtp_tree_world->SetPartitionMethod(
        topology_partition_method[(i + 1) % 2]);

    if (libtp_avbp_mesh->m_is2d) {
      libtp_options.m_threshold_count = AdaptLoop2D(libtp_mesh,
                                                    *libtp_tree_world,
                                                    libtp_options,
                                                    libtp_adapt_meta,
                                                    *libtp_solution,
                                                    do_bootstrap);
    } else {
      libtp_options.m_threshold_count = AdaptLoop3D(libtp_mesh,
                                                    *libtp_tree_world,
                                                    libtp_options,
                                                    libtp_adapt_meta,
                                                    *libtp_solution,
                                                    do_bootstrap);
    }
    /* Check maximum use memory. */
    rusage use;
    if (getrusage(RUSAGE_SELF, &use) == 0) {
      /* On linux Kb, on OSX in bytes for now just handle linux */
      /* Need to divide by 1024 again on OSX! */
      double mem = use.ru_maxrss;
      std::vector<double> min_max_mem = {-mem, mem};
      std::vector<double> sum_mem = {mem, mem};
      libtp_world->AllReduceMax(min_max_mem.data(), 2);
      libtp_world->AllReduceSum(sum_mem.data(), 2);
      sum_mem[0] = sum_mem[0] / libtp_world->size();
      min_max_mem[0] = -min_max_mem[0]; // Trick avoid dual reduce
      if (libtp_world->IsMaster())
        // Rescale to get MB ( depends on platform )
        std::cout << "Message: getrusage predicted heap usage [MB]: \n"
                  << " [ Min  : " << min_max_mem[0] / RUSAGE_MEM_TO_MBYTES
                  << " - "
                  << " Max  : " << min_max_mem[1] / RUSAGE_MEM_TO_MBYTES
                  << " - "
                  << " Mean : " << sum_mem[0] / RUSAGE_MEM_TO_MBYTES << " - "
                  << " Sum  : " << sum_mem[1] / RUSAGE_MEM_TO_MBYTES << " ] \n";
    }

    if (i == libtp_options.m_mmg_max_iter - 1 ||
        libtp_options.m_threshold_count == 0) {
      // Keep the adapt meta data alive for transfer to the interface
      // but clear the payload data because we can read from dictionary
      auto &input_tree = *libtp_tree_world;
      auto last_level = 0;
      auto &item = input_tree[last_level];
      // Default is 3D mesh (tetra elements)
      int nelmnod = 4;
      if (libtp_avbp_mesh->m_is2d == true)
        nelmnod = 3;
      // Allocate the element node connectivity array
      libtp_adapt_meta.local_elmconn.clear();
      libtp_adapt_meta.local_elmconn.shrink_to_fit();
      libtp_adapt_meta.local_elmconn.reserve(
          nelmnod * item.Payload().connectivity.size());
      // Create the global to local index map
      std::map<id_size_t, id_size_t> node_global_to_local;
      for (const auto &item_in_owned : libtp_adapt_meta.owned_map)
        node_global_to_local[item_in_owned.second] = item_in_owned.first;
      for (const auto &item_in_spill : libtp_adapt_meta.spill_over_map)
        node_global_to_local[item_in_spill.second] = item_in_spill.first;
      // Copy local connectivity info
      for (const auto &elem : item.Payload().connectivity) {
        for (const auto &elm_node : elem.nodes) {
          if (elm_node > 0) {
            libtp_adapt_meta.local_elmconn.push_back(
                node_global_to_local[elm_node] + 1);
          }
        }
      }
      // clear the local LB payload (we do not need it)
      item.Payload().clear();
      item.Payload().shrink_to_fit();

      // Make sure we exit the loop (break was not working)
      i = libtp_options.m_mmg_max_iter;
      break;
    } else {
      libtp_adapt_meta.clear();
    } // End of checking for convergence or max iteration reached
    libtp_world->Barrier();
    do_bootstrap = false;
  } // Loop over adapt iterations
  // Create the local node permutations for re-ordering
  // and re-numbering
  CreateLocalPermutations();
  libtp_world->Barrier();
}

void libtreeadapt_clear_adapt_meta() {
  // clear the adapt meta data
  libtp_adapt_meta.clear();
}

void libtreeadapt_compute_edge_length_sum(int *nnodes,
                                          int *nelm,
                                          int *elmcon,
                                          int *shared_proc_cnt,
                                          int *shared_proc_ranks,
                                          int *shared_nodes_cnt,
                                          int *shared_nodes_lid,
                                          double *xyz,
                                          double *sumlen,
                                          double *edgcnt) {
  std::vector<int> my_lowest_shared_rank(*nnodes, -1);
  // Create the shared node data-structure
  int offset = 0;
  for (int i = 0; i < *shared_proc_cnt; ++i) {
    const auto &ng_rank = shared_proc_ranks[i];
    for (int j = 0; j < shared_nodes_cnt[i]; ++j) {
      const auto my_lid = shared_nodes_lid[offset] - 1;
      // Make sure you get the lowest rank in the list of shared ranks at node
      if (my_lowest_shared_rank[my_lid] >= 0) {
        my_lowest_shared_rank[my_lid] =
            std::min(my_lowest_shared_rank[my_lid], ng_rank);
      }
      // If nothing was assigned then assign the first neighbour
      else {
        my_lowest_shared_rank[my_lid] = ng_rank;
      }
      // Increment the offset pointer
      ++offset;
    }
  }
  int nelmnod = 4; // Default is tet in 3D
  int ndim = 3;    // Dimension of coordinate array
  // Check if this is a 2D mesh
  if (libtp_avbp_mesh->m_is2d == true) {
    nelmnod = 3; // Change to triangles in 2D
    ndim = 2;    // Stride of 2 in coordinate array
  }
  // Collect all unique edges in the mesh (needs element node ordering)
  std::set<std::pair<int, int>> edge_set;
  if (nelmnod == 4) {
    const auto tet_nodes = TetEdgeNodes();
    for (int ielm = 0; ielm < *nelm; ++ielm) {
      // Loop over the edge list of the tetra
      for (const auto &edge_node : tet_nodes) {
        // Note using C indexing
        const auto i_left = std::min(elmcon[ielm * nelmnod + edge_node[0]],
                                     elmcon[ielm * nelmnod + edge_node[1]]);
        const auto i_right = std::max(elmcon[ielm * nelmnod + edge_node[0]],
                                      elmcon[ielm * nelmnod + edge_node[1]]);
        const auto left_rank = my_lowest_shared_rank[i_left - 1];
        const auto right_rank = my_lowest_shared_rank[i_right - 1];
        // Check if both left/right nodes are shared nodes
        if (left_rank >= 0 && right_rank >= 0) {
          const auto my_rank = libtp_world->Rank();
          // If left/right rank are larger than my_rank then include this edge
          if (left_rank > my_rank && right_rank > my_rank)
            // Fortran to C index
            edge_set.insert(std::make_pair(i_left - 1, i_right - 1));
        } else {
          // Fortran to C index
          edge_set.insert(std::make_pair(i_left - 1, i_right - 1));
        }
      }
    }
  } else {
    const auto tri_nodes = TriEdgeNodes();
    for (int ielm = 0; ielm < *nelm; ++ielm) {
      // Loop over the edge list of the tetra
      for (const auto &edge_node : tri_nodes) {
        // Note using C indexing
        auto i_left = std::min(elmcon[ielm * nelmnod + edge_node[0]],
                               elmcon[ielm * nelmnod + edge_node[1]]);
        auto i_right = std::max(elmcon[ielm * nelmnod + edge_node[0]],
                                elmcon[ielm * nelmnod + edge_node[1]]);
        const auto left_rank = my_lowest_shared_rank[i_left - 1];
        const auto right_rank = my_lowest_shared_rank[i_right - 1];
        // Check if both left/right nodes are shared nodes
        if (left_rank >= 0 && right_rank >= 0) {
          const auto my_rank = libtp_world->Rank();
          // If left/right rank are larger than my_rank then include this edge
          if (left_rank >= my_rank && right_rank >= my_rank)
            // Fortran to C index
            edge_set.insert(std::make_pair(i_left - 1, i_right - 1));
        } else {
          // Fortran to C index
          edge_set.insert(std::make_pair(i_left - 1, i_right - 1));
        }
      }
    }
  }

  // Zero out the input data
  for (int i = 0; i < *nnodes; ++i) {
    sumlen[i] = 0;
    edgcnt[i] = 0;
  }

  // Loop over edge list and sum the edge length
  for (const auto &ipair : edge_set) {
    double dx = 0, dy = 0, dz = 0;
    const auto &i1 = ipair.first;
    const auto &i2 = ipair.second;
    dx = xyz[i1 * ndim + 0] - xyz[i2 * ndim + 0];
    dy = xyz[i1 * ndim + 1] - xyz[i2 * ndim + 1];
    if (ndim == 3)
      dz = xyz[i1 * ndim + 2] - xyz[i2 * ndim + 2];
    auto dist = sqrt(dx * dx + dy * dy + dz * dz);
    sumlen[i1] += dist;
    sumlen[i2] += dist;
    edgcnt[i1]++;
    edgcnt[i2]++;
  }
  libtp_world->Barrier();
}

void libtreeadapt_debug_tecplot(int *ndim,
                                int *nnode,
                                int *nelm,
                                int *npatch,
                                int *elmnod,
                                int *bnode_cnt,
                                int *bnode,
                                double *xyz) {
  std::stringstream cat;
  cat << "debug_file_" << libtp_world->Rank() << ".dat";
  std::ofstream fout(cat.str());
  int num_elm_nod = 3;
  if (*ndim == 3) {
    fout << "VARIABLES=\"X\",\"Y\",\"Z\"\n";
    fout << "ZONE DATAPACKING=BLOCK, NODES=" << *nnode << ", ELEMENTS=" << *nelm
         << ", ZONETYPE=FETETRAHEDRON\n";
    num_elm_nod = 4;
  } else {
    fout << "VARIABLES=\"X\",\"Y\"\n";
    fout << "ZONE DATAPACKING=BLOCK, NODES=" << *nnode << ", ELEMENTS=" << *nelm
         << ", ZONETYPE=FETRIANGLE\n";
  }
  for (int i = 0; i < *nnode; ++i) {
    fout << xyz[i * (*ndim)] << "\n";
  }
  for (int i = 0; i < *nnode; ++i) {
    fout << xyz[i * (*ndim) + 1] << "\n";
  }
  if (*ndim == 3) {
    for (int i = 0; i < *nnode; ++i) {
      fout << xyz[i * (*ndim) + 2] << "\n";
    }
  }
  // Write internal data
  for (int i = 0; i < *nelm; ++i) {
    for (int j = 0; j < num_elm_nod; ++j) {
      fout << elmnod[i * num_elm_nod + j] << "  ";
    }
    fout << "\n";
  }
  fout.close();
  cat.str("");
  cat << "debug_bnd_file_" << libtp_world->Rank() << ".dat";
  fout.open(cat.str());
  if (*ndim == 3)
    fout << "VARIABLES=\"X\",\"Y\",\"Z\"\n";
  else
    fout << "VARIABLES=\"X\",\"Y\"\n";
  int count = 0;
  for (int ipatch = 0; ipatch < *npatch; ++ipatch) {
    fout << "ZONE I=" << bnode_cnt[ipatch] << "\n";
    for (int i = 0; i < bnode_cnt[ipatch]; ++i) {
      auto id = bnode[count] - 1;
      fout << xyz[id * (*ndim)] << " " << xyz[id * (*ndim) + 1];
      if (*ndim == 3)
        fout << " " << xyz[id * (*ndim) + 2];
      fout << "\n";
      count++;
    }
  }
  libtp_world->Barrier();
}

void libtreeadapt_get_owned_marker(int *nnodes, int *marker) {
  // First fill marker with zeros
  for (int i = 0; i < *nnodes; ++i)
    marker[i] = 0;
  // Owned map contains the list of locally owned nodes
  // first(local-node-id) -> second(global-node-id) map
  // Also note that local-node-id is zero indexed (C-style)
  for (size_t i = 0; i < libtp_adapt_meta.owned_map.size(); ++i)
    marker[i] = 1;
  libtp_world->Barrier();
}

void libtreeadapt_option_mesh_filename(int *len, char *filename) {
  libtp_solution->SetPrefix(filename);
}

void colour_api_call(t_index *nn,
                     t_index *ne,
                     t_index *mcg,
                     t_index *ncommon,
                     t_index *eptr,
                     t_index *eind,
                     t_index *gofs,
                     t_index *gcolour,
                     t_index *epart,
                     t_index *perm,
                     t_index *max_gsize) {
  *max_gsize = colour_api_call1(
      *nn, *ne, *mcg, *ncommon, eptr, eind, gofs, gcolour, epart, perm);
}

void write_colour_triangle(t_index *nn,
                           t_index *ne,
                           t_index *mcg,
                           double *x,
                           double *y,
                           double *z,
                           t_index *eptr,
                           t_index *eind,
                           t_index *gofs,
                           t_index *gcolour,
                           t_index *perm) {
  write_colour_triangle1(
      *nn, *ne, *mcg, x, y, z, eptr, eind, gofs, gcolour, perm);
}
