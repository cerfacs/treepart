find_package(Git 1.8.0)

if (NOT TARU_VERSION_CONFIGURE_FILE_PATH OR TARU_VERSION_CONFIGURE_FILE_PATH STREQUAL "")
  MESSAGE (FATAL_ERROR "Please set TARU_VERSION_CONFIGURE_FILE_PATH to generate the git version number")
endif()
if (NOT TARU_VERSION_CONFIGURE_FILE_PREFIX OR TARU_VERSION_CONFIGURE_FILE_PREFIX STREQUAL "")
  MESSAGE (FATAL_ERROR "Please set TARU_VERSION_CONFIGURE_FILE_PREFIX to generate the git version number")
endif()

# Checks if CURRENT_STRING matches string in file FILENAME
# And sets a BOOLEAN GENERATE_VERSION to TRUE if they do not match
function(CHECK_VERSION_CHANGE CURRENT_STRING FILE_PATH FILE_PREFIX)

  # Input/Output files using config file extension and prefix
  set(FILE_EXTENSION "h")
  SET(FILENAME ${CMAKE_BINARY_DIR}/${FILE_PATH}/${FILE_PREFIX})
  SET(IN_FILENAME ${CMAKE_SOURCE_DIR}/${FILE_PATH}/${FILE_PREFIX}.${FILE_EXTENSION}.in)
  SET(OUT_FILENAME ${FILENAME}.${FILE_EXTENSION})

  # Check if the file version exists in FILENAME
  IF (EXISTS ${FILENAME} AND EXISTS ${OUT_FILENAME})
    FILE (READ ${FILENAME} CHECK_VER)
    IF ("${CHECK_VER}" STREQUAL "${CURRENT_STRING}")
      SET (GENERATE_VERSION FALSE)
      MESSAGE (STATUS "Version not changed ${CURRENT_STRING} ")
    ELSE ()
      SET (GENERATE_VERSION TRUE)
      STRING (LENGTH "${TARU_VERSION_STRING}" GIT_VERSION_LENGTH)
      MESSAGE (STATUS "Git version string ${CURRENT_STRING} successfully created")
    ENDIF ()
  ELSE ()
    SET (GENERATE_VERSION TRUE)
    MESSAGE (STATUS "No version string found generating ${CURRENT_STRING}")
  ENDIF ()

  # Write the version file and generated configure version headers
  IF(GENERATE_VERSION)
    FILE (WRITE ${FILENAME} ${CURRENT_STRING})
    # MESSAGE (STATUS "In file ${IN_FILENAME} out file ${OUT_FILENAME}")
    CONFIGURE_FILE (${IN_FILENAME} ${OUT_FILENAME} @ONLY)
  ENDIF()
endfunction()

# Sets the version string using GIT if found or set it to default value
function (GENERATE_TARU_VERSION_STRING)
  SET(TARU_VERSION_STRING "NO-GIT-VERSION")
  IF(GIT_FOUND)
    # git short sha1
    EXECUTE_PROCESS (COMMAND
                    "${GIT_EXECUTABLE}" describe --tags --abbrev=0 --dirty=-modified
                    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
                    OUTPUT_VARIABLE TARU_GIT_HASH
                    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
    # Generate the version file
    SET (TARU_VERSION_STRING ${TARU_GIT_HASH} PARENT_SCOPE)
  ENDIF ()
endfunction()

## Generate version string and check against existing one
GENERATE_TARU_VERSION_STRING()
CHECK_VERSION_CHANGE("${TARU_VERSION_STRING}" "${TARU_VERSION_CONFIGURE_FILE_PATH}" "${TARU_VERSION_CONFIGURE_FILE_PREFIX}")
