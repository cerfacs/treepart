if(TARU_ENABLE_DOWNLOAD)
  set(MMG3D_GIT_TAG v5.7.3)
  ## Use the unsupported corner fix MMG3D branch
  ## This is not planned to be merged into the main MMG3D branch
  ## so don't be surprised if this branch is not working anymore
  if(TARU_USE_MMG3D_CORNER_FIX)
    set(MMG3D_GIT_TAG 6884c82e43ffdc433cf2ed1ccb91df98552bbe86)
  endif()
  CPMAddPackage(
    NAME mmg
    GIT_TAG ${MMG3D_GIT_TAG}
    GITHUB_REPOSITORY MmgTools/mmg
    OPTIONS "USE_SCOTCH OFF" "USE_ELAS OFF" "CMAKE_BUILD_TYPE Release" "BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS}"
    EXCLUDE_FROM_ALL ${TARU_ENABLE_BUNDLEDLIB}
  )
else()
  CPMAddPackage(
    NAME mmg
    URL ${CMAKE_SOURCE_DIR}/thirdparty/mmg-5.7.3.tar.gz
    URL_HASH SHA256=b0a9c5ad6789df369a68f94295df5b324b6348020b73bcc395d32fdd44abe706
    OPTIONS "USE_SCOTCH OFF" "USE_ELAS OFF" "CMAKE_BUILD_TYPE Release" "BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS}"
    EXCLUDE_FROM_ALL ${TARU_ENABLE_BUNDLEDLIB}
  )
endif()
set(TARU_ADAPT_LIBS ${TARU_ADAPT_LIBS} Mmg::libmmg_a Mmg::libmmg3d_a Mmg::libmmg2d_a Mmg::libmmgs_a)
