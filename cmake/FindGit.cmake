# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#.rst:
# FindGit
# -------
#
# The module defines the following variables:
#
# ``GIT_EXECUTABLE``
#   Path to Git command-line client.
# ``Git_FOUND``, ``GIT_FOUND``
#   True if the Git command-line client was found.
# ``GIT_VERSION_STRING``
#   The version of Git found.
#
# Example usage:
#
# .. code-block:: cmake
#
#    find_package(Git)
#    if(Git_FOUND)
#      message("Git found: ${GIT_EXECUTABLE}")
#    endif()

# Look for 'git' or 'eg' (easy git)
#
SET (git_names git eg)

# Prefer .cmd variants on Windows unless running in a Makefile
# in the MSYS shell.
#
IF (CMAKE_HOST_WIN32)
  IF (NOT CMAKE_GENERATOR MATCHES "MSYS")
    SET (git_names git.cmd git eg.cmd eg)
    # GitHub search path for Windows
    FILE (GLOB github_path
          "$ENV{LOCALAPPDATA}/Github/PortableGit*/cmd"
          "$ENV{LOCALAPPDATA}/Github/PortableGit*/bin"
          )
    # SourceTree search path for Windows
    SET (_git_sourcetree_path "$ENV{LOCALAPPDATA}/Atlassian/SourceTree/git_local/bin")
  ENDIF ()
ENDIF ()

# First search the PATH and specific locations.
FIND_PROGRAM (GIT_EXECUTABLE
              NAMES ${git_names}
              HINTS
              $ENV{GIT_HOME}
              /opt/homebrew/bin
              /usr/bin
              DOC "Git executable"
              NO_DEFAULT_PATH
              NO_CMAKE_PATH
              NO_CMAKE_ENVIRONMENT_PATH
              NO_SYSTEM_ENVIRONMENT_PATH
              NO_CMAKE_SYSTEM_PATH
              NO_PACKAGE_ROOT_PATH )

IF (CMAKE_HOST_WIN32)
  # Now look for installations in Git/ directories under typical installation
  # prefixes on Windows.  Exclude PATH from this search because VS 2017's
  # command prompt happens to have a PATH entry with a Git/ subdirectory
  # containing a minimal git not meant for general use.
  FIND_PROGRAM (GIT_EXECUTABLE
                NAMES ${git_names}
                PATH_SUFFIXES Git/cmd Git/bin
                NO_SYSTEM_ENVIRONMENT_PATH
                DOC "Git command line client"
                )
ENDIF ()

MARK_AS_ADVANCED (GIT_EXECUTABLE)

UNSET (git_names)
UNSET (_git_sourcetree_path)

IF (GIT_EXECUTABLE)
  EXECUTE_PROCESS (COMMAND ${GIT_EXECUTABLE} --version
                   OUTPUT_VARIABLE git_version
                   ERROR_QUIET
                   OUTPUT_STRIP_TRAILING_WHITESPACE)
  IF (git_version MATCHES "^git version [0-9]")
    STRING (REPLACE "git version " "" GIT_VERSION_STRING "${git_version}")
  ENDIF ()
  UNSET (git_version)
ENDIF ()

INCLUDE (FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (Git
                                   REQUIRED_VARS GIT_EXECUTABLE
                                   VERSION_VAR GIT_VERSION_STRING)
