###############################################################
# Generate Doxygen file in build dir to generate doxygen docs
CPMAddPackage("gh:mosra/m.css#master")

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile.in
        ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/doc/figures DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/doc/html/doc)
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile-mcss DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

find_package(Doxygen REQUIRED dot)
add_custom_target(doc ALL
        COMMAND "${m.css_SOURCE_DIR}/documentation/doxygen.py" "${CMAKE_CURRENT_BINARY_DIR}/Doxyfile-mcss"
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API documentation with Doxygen"
        DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile.in
        VERBATIM)
