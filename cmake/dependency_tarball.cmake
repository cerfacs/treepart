# # fmt library
# CPMAddPackage(
#   NAME fmt
#   URL ${CMAKE_SOURCE_DIR}/thirdparty/fmt-10.2.1.tar.gz
#   URL_HASH SHA256=1250e4cc58bf06ee631567523f48848dc4596133e163f02615c97f78bab6c811
#   EXCLUDE_FROM_ALL ON
# )

# # spdlog library
# CPMAddPackage(
#   NAME spdlog
#   URL ${CMAKE_SOURCE_DIR}/thirdparty/spdlog-1.14.1.tar.gz
#   URL_HASH SHA256=1586508029a7d0670dfcb2d97575dcdc242d3868a259742b69f100801ab4e16b
#   EXCLUDE_FROM_ALL ON
# )

# # Catch2 library
# CPMAddPackage(
#   NAME Catch2
#   URL ${CMAKE_SOURCE_DIR}/thirdparty/Catch2-3.5.4.tar.gz
#   URL_HASH SHA256=b7754b711242c167d8f60b890695347f90a1ebc95949a045385114165d606dbb
#   EXCLUDE_FROM_ALL ON
# )

# nanoflann Nearest neighbour library
CPMAddPackage(
  NAME nanoflann
  URL ${CMAKE_SOURCE_DIR}/thirdparty/nanoflann-1.5.5.tar.gz
  URL_HASH SHA256=fd28045eabaf0e7f12236092f80905a1750e0e6b580bb40eadd64dc4f75d641d
  EXCLUDE_FROM_ALL ON
)

# CLI11 command line parse
CPMAddPackage(
  NAME CLI11
  URL ${CMAKE_SOURCE_DIR}/thirdparty/CLI11-v2.4.2.tar.gz
  URL_HASH SHA256=f2d893a65c3b1324c50d4e682c0cdc021dd0477ae2c048544f39eed6654b699a
  EXCLUDE_FROM_ALL ON
)
