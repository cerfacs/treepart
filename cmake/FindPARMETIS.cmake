######################################################
## Adapted from STAMPS CMake ParMETIS configuration ##
######################################################
if (DEFINED ENV{PARMETIS_DIR})
  set(PARMETIS_SEARCH_PATH "$ENV{PARMETIS_DIR}")
  message(STATUS "Using PARMETIS_DIR environment variable: ${PARMETIS_SEARCH_PATH}")
endif ()

FIND_PATH (PARMETIS_INCLUDE_DIR
  NAMES parmetis.h
  HINTS ${PARMETIS_SEARCH_PATH}
  PATH_SUFFIXES "include"
  DOC "Directory where the PARMETIS header files are located"
  NO_DEFAULT_PATH
  NO_CMAKE_PATH
  NO_CMAKE_ENVIRONMENT_PATH
  NO_SYSTEM_ENVIRONMENT_PATH
  NO_CMAKE_SYSTEM_PATH
  NO_PACKAGE_ROOT_PATH )

FIND_LIBRARY (PARMETIS_LIBRARY
  NAMES libparmetis.a parmetis parmetis${PARMETIS_LIB_SUFFIX}
  HINTS ${PARMETIS_SEARCH_PATH}
  PATH_SUFFIXES "lib"
  DOC "Directory where the PARMETIS library is located"
  NO_DEFAULT_PATH
  NO_CMAKE_PATH
  NO_CMAKE_ENVIRONMENT_PATH
  NO_SYSTEM_ENVIRONMENT_PATH
  NO_CMAKE_SYSTEM_PATH
  NO_PACKAGE_ROOT_PATH)

IF (PARMETIS_INCLUDE_DIR AND EXISTS "${PARMETIS_INCLUDE_DIR}/metis.h")
  SET (idx_t_pattern "^#define[\t ]+IDXTYPEWIDTH[\t ]+([0-9]+)$")
  SET (real_t_pattern "^#define[\t ]+REALTYPEWIDTH[\t ]+([0-9]+)$")

  FILE (STRINGS "${PARMETIS_INCLUDE_DIR}/metis.h" parmetis_idx_t_line REGEX ${idx_t_pattern})
  STRING(REGEX REPLACE "^#define[ \t]+IDXTYPEWIDTH[ \t]+([0-9]+).*" "\\1" PARMETIS_IDX_T "${parmetis_idx_t_line}")

  FILE (STRINGS "${PARMETIS_INCLUDE_DIR}/metis.h" parmetis_real_t_line REGEX ${real_t_pattern})
  STRING(REGEX REPLACE "^#define[ \t]+REALTYPEWIDTH[ \t]+([0-9]+).*" "\\1" PARMETIS_REAL_T "${parmetis_real_t_line}")
ENDIF()

# Standard package handling
INCLUDE (FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (PARMETIS
                                    REQUIRED_VARS PARMETIS_IDX_T PARMETIS_REAL_T
                                    PARMETIS_LIBRARY PARMETIS_INCLUDE_DIR)

IF (PARMETIS_FOUND)
    message(STATUS "Found PARMETIS: ${PARMETIS_LIBRARY}")
    message(STATUS "PARMETIS include directory: ${PARMETIS_INCLUDE_DIR}")
    add_library(PARMETIS::PARMETIS UNKNOWN IMPORTED)
    set_target_properties(PARMETIS::PARMETIS PROPERTIES
        IMPORTED_LOCATION "${PARMETIS_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PARMETIS_INCLUDE_DIR}"
    )
ENDIF ()
