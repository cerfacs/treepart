# CPMAddPackage(
#   NAME fmt
#   GIT_TAG 10.2.1
#   GITHUB_REPOSITORY fmtlib/fmt
#   EXCLUDE_FROM_ALL ON
# )

# CPMAddPackage(
#   NAME spdlog
#   GIT_TAG v1.x
#   GITHUB_REPOSITORY gabime/spdlog
#   EXCLUDE_FROM_ALL ON
# )

# CPMAddPackage(
#   NAME Catch2
#   GIT_TAG v3.5.4
#   GITHUB_REPOSITORY catchorg/Catch2
#   EXCLUDE_FROM_ALL ON
# )

CPMAddPackage(
  NAME nanoflann
  GIT_TAG v1.5.5
  GITHUB_REPOSITORY jlblancoc/nanoflann
  EXCLUDE_FROM_ALL ON
)

CPMAddPackage(
  NAME CLI11
  GIT_TAG v2.0.0
  GITHUB_REPOSITORY CLIUtils/CLI11
  EXCLUDE_FROM_ALL ON
)
