###################################################
## Adapted from STAMPS CMake METIS configuration ##
###################################################
if (DEFINED ENV{METIS_DIR})
    set(METIS_SEARCH_PATH "$ENV{METIS_DIR}")
    message(STATUS "Using METIS_DIR environment variable: ${METIS_SEARCH_PATH}")
endif ()

FIND_PATH (METIS_INCLUDE_DIR
  NAMES metis.h
  HINTS ${METIS_SEARCH_PATH}
  PATH_SUFFIXES "include"
  DOC "Directory where the METIS header files are located"
  NO_DEFAULT_PATH
  NO_CMAKE_PATH
  NO_CMAKE_ENVIRONMENT_PATH
  NO_SYSTEM_ENVIRONMENT_PATH
  NO_CMAKE_SYSTEM_PATH
  NO_PACKAGE_ROOT_PATH)

FIND_LIBRARY (METIS_LIBRARY
  NAMES libmetis.a metis metis${METIS_LIB_SUFFIX}
  HINTS ${METIS_SEARCH_PATH}
  PATH_SUFFIXES "lib"
  DOC "Directory where the METIS library is located"
  NO_DEFAULT_PATH
  NO_CMAKE_PATH
  NO_CMAKE_ENVIRONMENT_PATH
  NO_SYSTEM_ENVIRONMENT_PATH
  NO_CMAKE_SYSTEM_PATH
  NO_PACKAGE_ROOT_PATH)

# Get METIS version
IF (NOT METIS_VERSION_STRING AND METIS_INCLUDE_DIR AND EXISTS "${METIS_INCLUDE_DIR}/metis.h")
  SET (version_pattern "^#define[\t ]+METIS_(MAJOR|MINOR)_VERSION[\t ]+([0-9\\.]+)$")
  SET (idx_t_pattern "^#define[\t ]+IDXTYPEWIDTH[\t ]+([0-9]+)$")
  SET (real_t_pattern "^#define[\t ]+REALTYPEWIDTH[\t ]+([0-9]+)$")

  FILE (STRINGS "${METIS_INCLUDE_DIR}/metis.h" metis_version REGEX ${version_pattern})

  FILE (STRINGS "${METIS_INCLUDE_DIR}/metis.h" metis_idx_t_line REGEX ${idx_t_pattern})
  STRING(REGEX REPLACE "^#define[ \t]+IDXTYPEWIDTH[ \t]+([0-9]+).*" "\\1" METIS_IDX_T "${metis_idx_t_line}")

  FILE (STRINGS "${METIS_INCLUDE_DIR}/metis.h" metis_real_t_line REGEX ${real_t_pattern})
  STRING(REGEX REPLACE "^#define[ \t]+REALTYPEWIDTH[ \t]+([0-9]+).*" "\\1" METIS_REAL_T "${metis_real_t_line}")

  FOREACH (match ${metis_version})
    IF (METIS_VERSION_STRING)
      SET (METIS_VERSION_STRING "${METIS_VERSION_STRING}.")
    ENDIF ()
    STRING (REGEX REPLACE ${version_pattern} "${METIS_VERSION_STRING}\\2" METIS_VERSION_STRING ${match})
    SET (METIS_VERSION_${CMAKE_MATCH_1} ${CMAKE_MATCH_2})
  ENDFOREACH ()
  UNSET (metis_version)
  UNSET (version_pattern)
ENDIF ()

# Standard package handling
INCLUDE (FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (METIS
                                  REQUIRED_VARS METIS_LIBRARY
                                  METIS_INCLUDE_DIR METIS_IDX_T
                                  METIS_REAL_T)

IF (METIS_FOUND)
    message(STATUS "Found METIS: ${METIS_LIBRARY}")
    message(STATUS "METIS include directory: ${METIS_INCLUDE_DIR}")
    add_library(METIS::METIS UNKNOWN IMPORTED)
    set_target_properties(METIS::METIS PROPERTIES
        IMPORTED_LOCATION "${METIS_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${METIS_INCLUDE_DIR}"
    )
ENDIF ()
