# Mesh adaptation (do_treeadapt)

<p style='text-align: justify;'>
`do_treeadapt` is an unstructured mesh adaptation CLI tool using MMG2D/3D library. It exploits the hierarchical structure of KalpaTARU and tries to minimise the number of iterations necessary to attain convergence of the parallel adaptation process. `do_treeadapt` does not help the user with the metric formulation and is agnostic to the metric calculation. It provides the ability to read arbitrary scalar metric values from HDF5 files (file name + dataset name) at mesh nodes and imposes that as target edge value at the given node. Then it iteratively runs a distributed parallel adaptation till the target metric is reached. The tool handles periodic boundaries by freezing the nodes at these boundaries from adapting to ensure conformity.
</p>

## do_treeadapt CLI inputs

The following are the major inputs necessary for running `do_treeadapt` in command line mode. The overall structure of the command line input is as follows:

```tcsh
mpiexec -np ${NUM_PROCS} do_treeadapt \
    -l ${LEAFS_PER_LEVEL} -p ${PARTITIONER_TYPE} \
    -i ${INPUT_PREFIX} \
    --metric-file ${ADAPT_METRIC_HDF5_FILE} \
    --metric-dset ${ADAPT_METRIC_HDF5_DATASET_NAME} \
    --iter-max ${MAX_ADAPT_ITERATIONS} \
    --hmax ${LIMIT_MAX_EDGE_LENGTH} \
    --hmin ${LIMIT_MIN_EDGE_LENGTH} \
    --hGrad ${EDGE_LENGTH_GRAD} \
    --hausdorff ${HAUSDROFF_DISTANCE} \
    --write-intermediate
```

1. `-l` : Number of leaves per node required in the partition hierarchy
    - for flat partition give a value `1`
    - for a 2 socket and 4 core topology use `4 2`

2. `-p` : The partitioner algorithm
   - `RCB`     (Recursive Coordinate Bisection)
   - `RIB`     (Recursive Inertial Bisection)
   - `HFSC`    (Hilbert Space-filling curve)
   -  `Scotch` (PT-Scotch/Scotch combination)
   -  `METIS`  (ParMETIS/METIS combination)
   -  `KaHIP`  under development (Karlsruhe HIGH Quality Partitioning)
   -  `GRAPH`  is deprecated (ParMetis/Metis combination)

   *Note:* You can give different partitioner for different partition for example, `-l 4 2 -p RCB GRAPH`

3. `-i` : The input mesh file prefix i.e. the file name after removing the `.mesh.h5` suffix

4. `--iter-max`: The maximum number of iterations to use to obtain the final adapted mesh

5. `--hmax` : The maximum edge length allowed in the adapted mesh. This is not an optional argument. If the metric exceeds this value then it is clipped to `hmax`

6. `--hmin` : The minimum edge length allowed in the adapted mesh and if metric is lower than this value then the metric is clipped to hmin

7. `--hausdorff` : Set the Hausdorff distance for the adaptation (boundary conformity)

8. `--hGrad` : Set the edge length gradient parameter

9. `--write-intermediate` : Saves the intermediate adapted mesh for debugging purpose

*Note:* The intermediate adapted meshes are output using the prefix `-i` as: `prefix_#.mesh.h5`.

## Frequently asked questions

**Q** *What is this Hausdorff distance?*

<p style='text-align: justify;'>
The Hausdorff distance between two meshes is the maximum value between the two one-sided Hausdorff distances (technically not they are not a distance): These two measures are not symmetric (e.g. the results depends on what mesh you set as \f$X\f$ or \f$Y\f$ : see figure below).
</p>

![Hausdorff Visu](doc/figures/husdroff_example.png "Hausdorff distance of two curves X  and Y")

![Hausdorff distance](doc/figures/husdroff.png "Hausdorff distance formula")

<p style='text-align: justify;'>
 Therefore, MMG takes this hausdorff distance to enforce how close topologically should the adapted mesh be to the original un-adapted mesh. This is also very useful in mesh simplification or coarsening to maintain closeness to the original surface mesh.

For a nice introduction see:
```
[1] P. Cignoni, C. Rocchini, R. Scopigno, Metro: measuring error on simplified surfaces, Computer Graphics Forum 17 (2), 167-174, 1998.
```
</p>

**Q** *What is hGrad?*

<p style='text-align: justify;'>
The `hGrad` option allows to set the gradation value. It controls the ratio between two adjacent edges. With a gradation of \f$h\f$, two adjacent edges \f$e_1\f$ and \f$e_2\f$ must respect the following condition:
</p>

\f$ \frac{1}{h} \le \frac{e_1}{e_2} \le h \f$

By default, the gradation value is \f$1.3\f$.

**Q:** *How do I improve the quality or modify/fix certain properties of my existing mesh?*

**A:** In many instances one would like to improve the quality, fix edge length gradation or min/max edge length sizes. In such cases running `do_treeadapt` without specifying a metric HDF5 file will use all input parameters specified in `--hmin`, `--hmax`, `--hGrad` and `--hausdorff` to generate a new adapted mesh respecting these values. This feature is especially useful to perform a quality improvement after a mesh deformation step (FSI, Shape optimisation, etc.) based on spring analogy or linear elasticity.

Here is a short recipe for quality,

```bash
mpiexec -np ${NUM_PROCS} do_treeadapt -l 1 -p METIS METIS -i my_mesh --iter-max 100 --hmax 1.0 --hmin 0.001 --hGrad 1.1 --hausdorff 0.1
```

The mesh file `my_mesh.mesh.h5` will be adapted to respect the given minimum edge length of 0.1, maximum edge length of 1.0, a gradation in edge length of 1.1 using Hausdroff distance 0.1. Note that METIS partitioner is used in every cycle.

**Q:** *Can I use `do_treeadapt` as a mesh generation tool?*

**A:** Strict answer is no, you always need a mesh to begin adaptation. But we have had people use this tool to generate very fine meshes using coarse meshes or coarsen meshes from very fine ones. Although we do not recommend doing this (bump up or bring down your meshes sizes aggressively), since boundary node projection to CAD surface is still not supported. Boundary conformity is necessary for wall bounded flows and curved geometries under adverse pressure gradients.

**Q:** *Can I run this in serial mode?*

**A:** This tool is designed to work in a distributed parallel environment. For serial application we recommend using Hip (software package developed by Jens D. Muller @ Queen Mary University of London).

**Q:** *Can I run this on tiny meshes on large number of processor counts?*

**A:** We recommend a minimum of 10k elements per MPI rank (at any given iteration). So avoid using this tool on tiny meshes (<10k) since this is known to cause deadlocks in the graph partitioner (especially Scotch). This tool was designed to generate meshes in excess of 200M elements on exascale machines.

**Q:** *Do you support other mesh formats?*

**A:** No. We support only the Hip/AVBP HDF5 mesh. CGNS mesh reader/writer is planned for the next release.

**Q:** *Do you support anisotropic metrics?*

**A:** No. We support only isotropic metric at the moment. Anisotropic mesh adaptation using Tucanos is planned for the next release.

**Q:** *My adaptation failed or hanging what should I do?*

**A:** We recommend doing the following before reporting this as bug or asking for help:

1. First check the logs i.e., "Message: *****" output of `do_treeadapt`. Make sure the values shown make sense. For example `hmin` or `hmax` might be set to unrealistic values.
2. Check your metric values to see if they are not too low (as this might produce a very huge mesh).
3. Make sure you are not using unrealistic small mesh on large processor counts.
4. Make sure you do not aggressively coarsen or refine your mesh. Always go in incremental steps of refinement or coarsening.
5. Make sure for the size of the problem you have dedicated enough MPI ranks. It might be possible that you have less number of MPI ranks considering the size of the mesh. Note that it does take time to adapt very large meshes.
6. Check if you have a `Release` build with ASAN turned off. `Debug` builds especially with ASAN turned ON can be very slow.
