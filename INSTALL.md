# Installation instructions

Begin the installation of KalpaTARU by first cloning the source repository from GitLab.

```bash
> git clone git@nitrox.cerfacs.fr:open-source/treeadapt.git
> cd treeadapt
> mkdir build
> cd build
```

The project uses modern CMake (>=v3.20) we maintain three ways to install KalpaTARU.

  1. Automatic download of packages
  2. Install from sources in Thirdparty repository

Autodetection of system installed packages is only available for parallel HDF5. Rest of the libraries are installed by KalpaTARU to avoid problems with 32/64-bit integers, version/configuration conflicts with Zoltan, MMG and PT-Scotch libraries.

If any of these auto-install fails on your system (or) you found an edge case to handle in our CMake auto-install scripts, please submit a patch/pull-request or open a bug report in GitLab issues.

## CMake variable documentation

- **TARU_ENABLE_ASAN** : Enable address sanitizer for checking memory leaks and errors (default `OFF`)
- **TARU_USE_MMG3D_CORNER_FIX** : Use the corner fix branch of MMG3D (when download is enabled) (default `OFF`)
- **TARU_ENABLE_TEST** : Enable unit tests (compile and run) (default `OFF`)
- **TARU_ENABLE_DOC** : Compile and install source documentation using Doxygen (default `OFF`)
- **TARU_ENABLE_DOWNLOAD** : Download the sources instead of using thirdparty or using system installed external dependency (default is `OFF`)
- **TARU_USE_SYSTEM_PHDF5** : Use the system HDF5 instead of downloading or using thirdparty sources
- **TARU_USE_INT64_GIDS** : If set to `ON` it will enable 64bit integers for global indices (default is `OFF`)
- **TARU_THIRDPARTY** : Path to the thirdparty folder that contains the source archive of external depedencies (default value is `${PROJECT_SOURCE_DIR}/thridparty`)
- **TARU_ENABLE_BUNDLEDLIB** : Create a single bundled static library of treeadapt + all dependent libraries (only Gcc/Clang/Intel). Use with caution as this might break on certain OS where `ar` archiver does not support `-M` flag like MacOSX. A bundled library called `libtreeadapt_bundle.a` will be created in the `${CMAKE_INSTALL_PREFIX}/lib` folder. (default `OFF`)

## Configuring, compiling and installing KalpaTARU

Example using thirdparty folder

```bash
> cmake -S .. -B . -DTARU_THIRDPARTY=/home/user/thirdparty \
  -DTARU_ENABLE_ASAN=OFF -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../install
> CMAKE_BUILD_PARALLEL_LEVEL=4 cmake --build . --target install
```

Example using download method

```bash
> cmake -S .. -B . -DTARU_ENABLE_DOWNLOAD=ON -DTARU_ENABLE_ASAN=OFF \
  -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../install
> CMAKE_BUILD_PARALLEL_LEVEL=4 cmake --build . --target install
```

Example using system installed packages

```bash
> cmake -S .. -B . -DTARU_ENABLE_DOWNLOAD=ON -DTARU_ENABLE_ASAN=OFF \
  -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../install \
  -DTARU_USE_SYSTEM_PHDF5=ON
> CMAKE_BUILD_PARALLEL_LEVEL=4 cmake --build . --target install
```

*Note: MMG library, Zoltan and Scotch are installed automatically using the download or thirdparty since we have to ensure they respect the TARU_USE_INT64_GIDS variable. We cannot ensure if the system installed library will respect this flag or have the right version. `CMAKE_BUILD_PARALLEL_LEVEL` is useful to set parallel build using modern CMake scripts. `CMAKE_INSTALL_PREFIX` is the installation folder.*

## Doxygen documentation

KalpaTARU uses m.css style for beautiful Doxygen documentation of sources and markdown documentation files. For installing documentation ensure Doxygen is installed with graphviz support and enable the `TARU_ENABLE_DOC` while configuring i.e., `-DTARU_ENABLE_DOC=ON`. Additionally, it requires installing python packages `jinga2` and `Pygments` and latex for rendering maths equations. The m.css package is automatically downloaded from the internet and currently we do not support offline documentation generation. You can use the generated `Doxyfile` in the build directory to manually build it yourself.

```bash
pip3 install jinja2 Pygments
```

*Note: On MacOSX machine `m.css` fails due to the absence of the `libgs.dylib` in the environment. So add the environment variable `export LIBGS=/opt/homebrew/lib/libgs.dylib` (assuming you used Homebrew to install Ghostscript `brew install ghostscript`). See the VSCode development documentation for example using VSCode workspace.*

## How to make my own thirdparty source folder

The source package dependency of KalpaTARU is available as a public git repo [KalpaTARU_thirdparty.git](https://github.com/pavanakumar/KalpaTARU_thirdparty.git). You can create your own third party folder using the following the commands. We assume that you have a working internet connection and have `git` installed on the machine with LFS support. For example, `$MY_THIRDPARTY_FOLDER` is the environment variable pointing to the folder where you want to clone all the third party packages.

```bash
> git clone https://github.com/pavanakumar/KalpaTARU_thirdparty.git $MY_THIRDPARTY_FOLDER
> cd $MY_THIRDPARTY_FOLDER
> git lfs install
> git lfs pull
```

Then provide this folder as input during the CMake configure of KalpaTARU as shown below. Note that we configure HDF5 library along with the sources of KalpaTARU as a CMake subproject. If you wish to use system HDF5 then feel free to do that by setting `-DTARU_USE_SYSTEM_PHDF5=ON`. The package source versions are tested in our CI/CD and should be support.

```bash
> cmake -S .. -B . -DTARU_ENABLE_DOWNLOAD=OFF -DTARU_ENABLE_ASAN=OFF \
  -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../install \
  -DTARU_USE_SYSTEM_PHDF5=OFF -DTARU_THIRDPARTY=$MY_THIRDPARTY_FOLDER
> CMAKE_BUILD_PARALLEL_LEVEL=4 cmake --build . --target install
```

## METIS/ParMETIS support

KalpaTARU supports METIS which is an optional dependency (due to strict licence issues).
The following has be ensured when you install ParMETIS/METIS library,
  - Correct integer IDXTYPEWIDTH in metis.h file (64 if TARU_USE_INT64_GIDS is set to ON)
  - Correct REALTYPEWIDTH to double in metis.h file (64 always)
  - Install both METIS and ParMETIS libraries (installing ParMETIS does not install METIS)

We recommend using our `install_metis.sh` script in `KalpaTARU_thirdparty` git repository to install METIS library to ensure you are using the correct configurations. The ParMETIS version in this repo is patched for generating deterministic partitioning in METIS. This is essential for reproducibility of AMR meshes for a given number of partitions.


You can use the ENV var `${METIS_DIR}` and `${PARMETIS_DIR}` to specify the installed folder of METIS and ParMETIS library. The script will check the sizes and only proceed if everything matches. For reproducible AMR runs use the ParMETIS version from COOP team. Build scripts are also available to instal the library from sources.
