# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).

## Release v0.8.2 (2024-12-16)

### Changed

- Complete revamp of CMake scripts
- Optional dependency ParMETIS/METIS
- Improved CI/CD testing using OpenMPI
- Fixed Kumar Dual mesh construction

### Added

- Using experimental PT-Scotch/Scotch graph partitioner (default)
- New method to install external packages using CPM/CMake
- Merged RCM ordering back from stale branch
- Better Doxygen documentation using CMake
- Started markdown documentation pages
- MUST Trace in CI/CD docker for verification of MPI codes

### Deprecated

- GRAPH defaults to METIS partitioner in CLI (remove in future release)

### Known issues

- Scotch partitioner deadlocks for tiny graphs on many MPI ranks (avoid aggressive coarsening)

## Release v0.8.1 (2023-10-31)

### Added

- Adapt mesh filename option in lib and CLI app
- Reproducible external parmetis version.
- Working parmetis dual graph alternative

### Changed

- Switch to docker.gitrunner for CI
- Set and use default prefix when meshname is speficied
- Fix mesh/sol/metric HDF5 in API mode
- Fix API for mesh prefix change
- Fix for mesh write via API
- Fix element count no div by 8
- Fixed/improved adapt LB edge weights
- Fix for ParMETIS allocator bug in DualGraph
- By default use Parmetis dualgraph (for now)
- Print imbalance after adapt
- Re-enabled interpol type in CLI app
- Correction of weights for reproducibility

### Deprecated

[ - ]

## Release v0.7.0 (2023-04-14)

### Added

- Added estimator (rule of thumb) for target mesh size deviation to improve bootstrap loadbalancing
- Added experimental PTSCOTCH support for partitioning.
- Added test cases

### Changed

- Add locale dependent mesh size output for easier view.
- Updated Dockerfile to use mpich avoiding rdma issues due to opempi and enable CI with ASAN support.
- Updated external hdf5 to 1.10.8 and use cmake hdf5 install
- Added parmetis petsc fixes for reproducibility.
- Added options and ubvec parmetis options to improve loadbalancing
- Improved deviation metric loadbalance at boostrap
- Improved cmake workflow
- Cleanup of warnings (miscelaneous)
- Correction of out of bounds arrays

### Deprecated

[ - ]

## Release v0.5.1 (2022-10-06)

### Added

- New API function for node coord copy

## Release v0.5.0 (2022-08-25)

### Added

- barycentric interpolation algorithm
- linear interpolation error check
- API : added option to switch interpolation algo and enable/disable linear interpolation check

### Changed

- removed kdtree lib from build. Switched to nannoflam only

## [0.4.0] 2021-12-4

### Added

[ - ]

### Changed

- default build type to Realase
- git find correction.

### Fixed

- mynodes fix
- edge weights

### Deprecated

[ - ]
